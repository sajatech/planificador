$(document).ready(function() {
    $.get("../services/getservices", { order: "ser_date desc" }, function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panServices").removeClass("hide").show();

        // Tabla de los servicios
        oTable = $("#services").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],
            "responsive": true,

            "aoColumns": [
                { "mData": "cliCode", "visible": false },
                { "mData": "conCode", "visible": false },
                { "mData": "endFormat", "visible": false },
                { "mData": "serCode", "visible": false },
                { "mData": "docType", "visible": false },
                { "mData": "client" },
                { "mData": "serDate" },
                { "mData": "serName" },
                { "mData": "amount" },
                { "mData": "paid" },
                { "mData": "balance" },
                { "mData": "status" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_edit", "sButtonText": "Update/View",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData();
                            seeService(node[0].cliCode, node[0].conCode, node[0].endFormat);
                        }

                    }
                 ]
            },

            "language": {
                "lengthMenu": "_MENU_"
            },

            "iDisplayLength": 50
        });

        $("#services_filter input").addClass("form-control").attr("placeholder", "Find Service...");
        $("#services_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_services_0").prepend("<i class='fa fa-pencil'/></i> ");
        $("#ToolTables_services_1").prepend("<i class='fa fa-file-text-o'/></i> ");
        $("#ToolTables_services_2").prepend("<i class='fa fa-envelope-o'/></i> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#services_length').addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#services_filter").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left"));

        $(".panel-footer").append($("#services+.row")).addClass("p10");
        $("#services_paginate>ul.pagination").addClass("pull-right m0");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });
});