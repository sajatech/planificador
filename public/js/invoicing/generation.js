$(document).ready(function() {
    win = null;

    $.get("../services/getservices?status='EXECUTED'&order='id desc'", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panServices").removeClass("hide").show();

        // Tabla de los servicios
        oTable = $("#services").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],
            "responsive": true,

            "aoColumns": [
                { "mData": "conCode", "visible": false },
                { "mData": "client" },
                { "mData": "conDate" },
                { "mData": "amount" },
                { "mData": "paid" },
                { "mData": "balance" },
                { "mData": "genDocuments" },
                { "mData": "serInvoicing" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    /*{ "sExtends": "editor_edit", "sButtonText": "Update/View",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData();
                            seeService(node[0].cliCode, node[0].conCode, node[0].endFormat);
                        }

                    },*/

                    { "sExtends": "editor_remove", "sButtonText": "Generate Document",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }*/
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();

                            if(node[0].serInvoicing == 0) {
                                notif({
                                    msg: "Client " + node[0].client + " does not have services for invoicing.",
                                    type: "error",
                                    position: "left",
                                    multiline: true
                                });                
                            } else {
                                $("#result").empty().load("../services/getinvoicing?contract=" + node[0].conCode, function(response, status) {
                                    // Checkboxes y radio buttons personalizados
                                    $(".icheck").iCheck({
                                        checkboxClass: "icheckbox_minimal-red"
                                    });

                                    $("#generate").attr({ "data-invoicing-contract": node[0].conCode, "data-invoicing-type": (node[0].docType).toLowerCase() });
                                    $("#modInvoicing").modal("show");
                                });
                            }
                        }
                      
                    }
                 ]
            },

            "language": {
                "lengthMenu": "_MENU_"
            },

            "iDisplayLength": 50
        });

        $("#services_filter input").addClass("form-control").attr("placeholder", "Find Client...");
        $("#services_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_services_0").prepend("<i class='fa fa-pencil'/></i> ");
        $("#ToolTables_services_1").prepend("<i class='fa fa-file-text-o'/></i> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#services_length').addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#services_filter").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left"));

        $(".panel-footer").append($("#services+.row")).addClass("p10");
        $("#services_paginate>ul.pagination").addClass("pull-right m0");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //

        $("#generate").on("click", function() {
            var loading = $("#loaInvoicing");
            // Se guarda la referencia al formulario
            var $f = $("#forInvoicing");

            //if($f.parsley().validate()) { 
                loading.show().removeClass("hide");
                var str = $f.serialize();

                /*$("input:checkbox:checked").each(function() {
                    alert($(this).val());
                });*/
                // Bloqueo de peticiones repetidas
                if($f.data("locked") == undefined || !$f.data("locked")) {
                    var $this = $(this);

                    $.ajax({  
                        type: "GET",
                        url: "../invoicing/documentbd?contract=" + $this.attr("data-invoicing-contract") + "&type=" + $this.attr("data-invoicing-type") + "&generation=1",
                        data: str,

                        beforeSend: function() {
                            $f.data("locked", true);
                        },

                        success: function(respuesta) {
                            function relTable() {
                                loading.hide();
                                $("#cerrar").click();
                                oTable.ajax.url("../services/getservices?status='EXECUTED'&order='id desc'").load();
                            }                          

                            if(win != null && !win.closed)
                                win.close();

                            win = (window.open("/" + (window.location.pathname).split("/")[1] + "/invoicing/document?file=" + respuesta, "Services Document", "width=240, height=600")).addEventListener("load", relTable, true);
                        },

                        error: function () {
                            loading.hide();

                            notif({
                                msg: "Sucedió un error, contacte con el administrador del sistema.",
                                type: "error",
                                multiline: true,
                                position: "left"
                            });
                        },

                        complete: function() { 
                            $f.data("locked", false);
                        }               
                    });

                    return false;
                }
            //}
        });
    });
});