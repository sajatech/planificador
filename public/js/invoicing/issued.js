$(document).ready(function() {
    win = null;

    $.get("../documents/getdocuments", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panIssued").removeClass("hide").show();

        // Tabla de los artículos
        oTable = $("#issued").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-12 col-xs-12 col-md-6'i><'col-sm-12 col-xs-12 col-md-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],
            "responsive": true,

            "aoColumns": [
                { "mData": "conCode", "visible": false },
                { "mData": "docId", "visible": false },
                { "mData": "client" },
                { "mData": "docType" },
                { "mData": "docNumber" },
                { "mData": "amount" },
                { "mData": "status" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    /*{ "sExtends": "editor_create", "sButtonText": "New", 

                        "fnInit": function(button) {
                            $(button).addClass("");
                            
                            /*if(data.privilegios.includes(1))
                                $(button).removeClass("hide");
                        },

                        "fnClick": function() { alert("no implementado");
                            //$.redirect("/" + (window.location.pathname).split("/")[1] + "/contracts/registration");
                        }

                    },*/
                    
                    { "sExtends": "editor_edit", "sButtonText": "View",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData();

                            if((node[0].status.replace(/<[^>]*>?/g, "") == "DISABLED")) {
                                notif({
                                    msg: "Document disabled.",
                                    type: "error",
                                    position: "left",
                                    multiline: true
                                });                
                            } else {
                                var button = $("#ToolTables_issued_0");
                                var prePdf = $("#preloader-pdf");
                                var body = $("body");

                                prePdf.removeClass("hide").show();

                                $.ajax({  
                                    type: "GET",
                                    url: "../invoicing/encurl",

                                    data: {
                                        contract: node[0].conCode,
                                        type: (node[0].docType).toLowerCase(),
                                        number: node[0].docNumber
                                    },

                                    beforeSend: function() {
                                        button.addClass("disabled");
                                    },

                                    success: function(respuesta) {
                                        function remDisabled() {
                                            button.removeClass("disabled");
                                            prePdf.fadeOut("slow");
                                            body.css({ "overflow": "visible" });
                                        }                          

                                        if(win != null && !win.closed)
                                            win.close();

                                        win = (window.open("/" + (window.location.pathname).split("/")[1] + "/invoicing/document?file=" + respuesta, "Services Document", "width=240, height=600")).addEventListener("load", remDisabled, true);
                                    },

                                    error: function () {
                                        button.removeClass("disabled");
                                        prePdf.fadeOut("slow");
                                        body.css({ "overflow": "visible" });

                                        notif({
                                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                                            type: "error",
                                            multiline: true,
                                            position: "left"
                                        });
                                    }
                                });
                            }
                        }

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Disable",

                        "fnInit": function(button) {
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }*/
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();  
                            disDocument(node);
                        }
                      
                    },

                    { "sExtends": "editor_remove", "sButtonText": "Send by Email",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }*/
                        },

                        "fnClick": function() {
                            var node = this.fnGetSelectedData();

                            if((node[0].status.replace(/<[^>]*>?/g, "") == "DISABLED")) {
                                notif({
                                    msg: "Document disabled.",
                                    type: "error",
                                    position: "left",
                                    multiline: true
                                });                
                            } else {
                                var button = $("#ToolTables_issued_2");
                                var prePdf = $("#preloader-pdf");
                                var body = $("body");

                                prePdf.removeClass("hide").show();

                                $.ajax({  
                                    type: "GET",
                                    url: "../invoicing/senemail",
                                    dataType: "json",

                                    data: {
                                        contract: node[0].conCode,
                                        type: (node[0].docType).toLowerCase(),
                                        number: node[0].docNumber,
                                    },

                                    beforeSend: function() {
                                        button.addClass("disabled");
                                    },

                                    success: function(respuesta) {
                                        button.removeClass("disabled");
                                        prePdf.fadeOut("slow");
                                        body.css({ "overflow": "visible" });

                                        notif({
                                            msg: respuesta.text,
                                            type: respuesta.type,
                                            multiline: true,
                                            position: "left"
                                        });
                                    },

                                    error: function () {
                                        button.removeClass("disabled");
                                        prePdf.fadeOut("slow");
                                        body.css({ "overflow": "visible" });

                                        notif({
                                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                                            type: "error",
                                            multiline: true,
                                            position: "left"
                                        });
                                    }
                                });
                            }
                        }
                      
                    }
                ]
            },

            "language": {
                "lengthMenu": "_MENU_"
            },

            "iDisplayLength": 50  
        });

        // Se añaden los íconos
        //$("#ToolTables_issued_0").prepend("<i class='fa fa-plus'/></i> ");
        $("#ToolTables_issued_0").prepend("<i class='fa fa-search'/></i> ");
        $("#ToolTables_issued_1").prepend("<i class='fa fa-exchange'/></i> ");
        $("#ToolTables_issued_2").prepend("<i class='fa fa-envelope-o'/></i> ");

        $("#issued_filter input").addClass("form-control").attr("placeholder", "Find Document...");
        $("#issued_length select").addClass("form-control");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#issued_length').addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#issued_filter").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left"));

        $(".panel-footer").append($("#issued+.row")).addClass("p10");
        $("#issued_paginate>ul.pagination").addClass("pull-right m0");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });

    // Desactivación de documento
    disDocument = function(node) {
        //var url = (node[0].status.replace(/<[^>]*>?/g, "") == "ENABLED") ? "../documents/disdocument" : "../documents/enadocument";
        var button = $("#ToolTables_issued_1");

        button.addClass("disabled");

        $.ajax({  
            type: "POST",
            url: "../documents/disdocument",
            dataType: "json",

            data: { 
                code: node[0].docNumber
            },

            success: function(respuesta) {
                button.removeClass("disabled");
                oTable.ajax.url("../documents/getdocuments").load();

                notif({
                    msg: respuesta.text,
                    type: respuesta.type,
                    position: "left",
                    multiline: true
                });
            },

            error: function () {
                button.removeClass("disabled");

                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "left",
                    multiline: true
                });
            }
        });                
        
        return false; 
    }
});