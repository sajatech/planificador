$(document).ready(function() {
    $.get("../expenses/getconcepts", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panExpenses").removeClass("hide").show();

        // Tabla de los conceptos de gastos
        oTable = $("#concepts").dataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [0, "desc"],
            "responsive": true,

            "aoColumns": [
                { "mData": "conCode" },
                { "mData": "creDate" },
                { "mData": "name" },
                { "mData": "regBy" },
                { "mData": "status" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Add", 

                        "fnInit": function(button) {
                            $(button).addClass("");
                            
                            /*if(data.privilegios.includes(1))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function() {
                            actionsConcept.registration();
                        }

                    },
                    
                    { "sExtends": "editor_edit", "sButtonText": "Update/View",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData(); 
                            actionsConcept.update(node);
                        }

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Enable/Disable",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }*/
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();  
                            actionsConcept.disable(node);
                        }
                      
                    }
                ]
            },

            "language": {
                "lengthMenu": "_MENU_"
            },

            "iDisplayLength": 50,    
        });

        $("#concepts_filter input").addClass("form-control").attr("placeholder", "Find Item...");
        $("#concepts_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_concepts_0").prepend("<i class='fa fa-plus'/></i> ");
        $("#ToolTables_concepts_1").prepend("<i class='fa fa-pencil'/></i> ");
        $("#ToolTables_concepts_2").prepend("<i class='fa fa-exchange'/></i> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#concepts_length").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#concepts_filter").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left"));

        $(".panel-footer").append($("#concepts+.row")).addClass("p10");
        $("#concepts_paginate>ul.pagination").addClass("pull-right m0");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });

    $(".action").on("click", function() {
        $(this).attr("data-concept-action") == "registration" ? regconcept() : updconcept();
    });

    var actionsConcept = {};

    actionsConcept.registration = function() {
        $("#modConcepts .modal-title").html("Expenditure item registration");
        $("input").val("");
        $(".action").attr("data-concept-action", "registration").text("Send");
        $("#modConcepts").modal("show");

        setTimeout(function() {
            $("input#name").focus();
        }, 500);
    }

    actionsConcept.update = function(node) {
        $("#modConcepts .modal-title").html("Expenditure item update");
        $("#name").val(node[0].name);
        $(".action").attr({ "data-concept-code": node[0].conCode, "data-group-action": "update" }).text("Update");
        $("#modConcepts").modal("show");
    }

    actionsConcept.disable = function(node) {
        var url = (node[0].status.replace(/<[^>]*>?/g, "") == "ENABLED") ? "../configuration/disexpense" : "../configuration/enaexpense";
        $("#ToolTables_concepts_2").addClass("disabled");

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

            data: { 
                code: node[0].conCode
            },

            success: function(respuesta) {
                oTable.api().ajax.url("../expenses/getconcepts").load();
                $("#ToolTables_concepts_1").addClass("disabled");

                notif({
                    msg: respuesta.text,
                    type: respuesta.type,
                    position: "left",
                    multiline: true
                });
            },

            error: function () {
                $("#ToolTables_concepts_2").removeClass("disabled");

                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "left",
                    multiline: true
                });
            }
        });                
        
        return false; 
    }

    regconcept = function() {
        // Se guarda la referencia al formulario
        var $f = $("#concept");

        if($f.parsley().validate()) {
            var str = $f.serialize();
            var loading = $("#loaConcept");

            loading.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuration/regexpense",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../expenses/getconcepts").load();
                            $("#ToolTables_concepts_1, #ToolTables_concepts_2").addClass("disabled");
                        }

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        }

        return false;
    }

    updconcept = function() {
        // Se guarda la referencia al formulario
        var $f = $("#concept");

        if($f.parsley().validate()) {
            var str = $f.serialize();
            var loading = $("#loaConcept");

            loading.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuration/updexpense?code=" + $(".action").attr("data-concept-code"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.api().ajax.url("../expenses/getconcepts").load();
                            $("#ToolTables_expenses_1, #ToolTables_expenses_2").addClass("disabled");
                        }

                        notif({
                          msg: respuesta.text,
                          type: respuesta.type,
                          position: "center"
                        });
                    },

                    error: function () {
                        loading.hide();

                        notif({
                          msg: "Sucedió un error, contacte con el administrador del sistema.",
                          type: "error",
                          position: "center"
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        }
    }
});