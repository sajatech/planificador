$(document).ready(function() {
    $.get("../groups/getgroups", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panGroups").removeClass("hide").show();

        // Tabla de los grupos de trabajo
        oTable = $("#groups").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [0, "desc"],
            "responsive": true,

            "aoColumns": [
                { "mData": "groCode" },
                { "mData": "name" },
                { "mData": "leader" },
                { "mData": "regBy" },
                { "mData": "status" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "Add", 

                        "fnInit": function(button) {
                            $(button).addClass("");
                            
                            /*if(data.privilegios.includes(1))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function() {
                            actionsGroup.registration();
                        }

                    },
                    
                    { "sExtends": "editor_edit", "sButtonText": "Update/View",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData(); 
                            actionsGroup.update(node);
                        }

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Enable/Disable",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }*/
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();  
                            actionsGroup.disable(node);
                        }
                      
                    }
                ]
            },

            "language": {
                "lengthMenu": "_MENU_"
            },

            "iDisplayLength": 50,    
        });

        $("#groups_filter input").addClass("form-control").attr("placeholder", "Find Group...");
        $("#groups_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_groups_0").prepend("<i class='fa fa-plus'/></i> ");
        $("#ToolTables_groups_1").prepend("<i class='fa fa-pencil'/></i> ");
        $("#ToolTables_groups_2").prepend("<i class='fa fa-exchange'/></i> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#groups_length').addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#groups_filter").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left"));

        $(".panel-footer").append($("#groups+.row")).addClass("p10");
        $("#groups_paginate>ul.pagination").addClass("pull-right m0");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });

    $(".action").on("click", function() {
        $(this).attr("data-group-action") == "registration" ? reggroup() : updgroup();
    });

    var actionsGroup = {};

    actionsGroup.registration = function() {
        $("#modGroups .modal-title").html("Work group registration");
        $("input").val("");
        $(".action").attr("data-group-action", "registration").text("Send");
        $("#modGroups").modal("show");

        setTimeout(function() {
            $("input#name").focus();
        }, 500);
    }

    actionsGroup.update = function(node) {
        $("#modGroups .modal-title").html("Work group update");
        $("#name").val(node[0].name);
        $("#leader").val(node[0].leader);
        $(".action").attr({ "data-group-code": node[0].groCode, "data-group-action": "update" }).text("Update");
        $("#modGroups").modal("show");
    }

    // Desactivación/Activación de grupo
    actionsGroup.disable = function(node) {
        var url = (node[0].status.replace(/<[^>]*>?/g, "") == "ENABLED") ? "../configuration/disgroup" : "../configuration/enagroup";

        $("#ToolTables_groups_2").addClass("disabled");

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

            data: { 
                code: node[0].groCode
            },

            success: function(respuesta) {
                oTable.ajax.url("../groups/getgroups").load();
                $("#ToolTables_groups_1, #ToolTables_groups_2").addClass("disabled");

                notif({
                    msg: respuesta.text,
                    type: respuesta.type,
                    position: "left",
                    multiline: true
                });
            },

            error: function () {
                $("#ToolTables_groups_2").removeClass("disabled");

                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "left",
                    multiline: true
                });
            }
        });                
        
        return false; 
    }

    reggroup = function() {
        // Se guarda la referencia al formulario
        var $f = $("#group");

        if($f.parsley().validate()) {
            var str = $f.serialize();
            var loading = $("#loaGroup");

            loading.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuration/reggroup",
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.ajax.url("../groups/getgroups").load();
                        }

                        notif({
                          msg: respuesta.text,
                          type: respuesta.type,
                          position: "center"
                        });
                    },

                    error: function () {
                        loading.hide();
                        $("#ToolTables_groups_0").removeClass("disabled");

                        notif({
                          msg: "Sucedió un error, contacte con el administrador del sistema.",
                          type: "error",
                          position: "center"
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
            }
        }

        return false;
    }

    updgroup = function() {
        // Se guarda la referencia al formulario
        var $f = $("#group");

        if($f.parsley().validate()) {
            var str = $f.serialize();
            var loading = $("#loaGroup");

            loading.show().removeClass("hide");

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../configuration/updgroup?code=" + $(".action").attr("data-group-code"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        if(respuesta.type != "error") {
                            $("#cerrar").click();
                            oTable.ajax.url("../groups/getgroups").load();
                            $("#ToolTables_groups_1, #ToolTables_groups_2").addClass("disabled");
                        }

                        notif({
                          msg: respuesta.text,
                          type: respuesta.type,
                          position: "center"
                        });
                    },

                    error: function () {
                        loading.hide();

                        notif({
                          msg: "Sucedió un error, contacte con el administrador del sistema.",
                          type: "error",
                          position: "center"
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });                
                
                return false;       
            }
        }
    }
});