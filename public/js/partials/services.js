$(document).ready(function() {
    // Inicialización de variables
    var flag = false;

    $("#payType").select2({width: "100%"});

    // Ejecución del servicio
    $(".status label").on("click", function() {
        var status = $(this).find("input").val();
        $("#modAdditional").modal("hide");

        exeSerCallback = function(choice) {
            $("#modAdditional").modal("show");

            if(choice) {
                var addService = $("#addService");
                var forService = $("#forService");
                var linPayments = $("#linPayments");
                var url = status == "EXECUTED" ? "/services/exeservice" : "/services/noeservice";

                $.ajax({  
                    type: "POST",
                    url: "/" + (window.location.pathname).split("/")[1] + url,
                    dataType: "json",

                    data: { 
                        contract: addService.attr("data-contract-id"),
                        serDate: addService.attr("data-service-date")
                    },

                    success: function(respuesta) {
                        if(respuesta.type != "error") {
                            if(status == "EXECUTED") {
                                forService.hide();
                                linPayments.attr({ "data-toggle": "tab", "href": "#payments" }).parent().removeClass("disabled");
                            } else {
                                $("#forService").show();
                                linPayments.attr({ "data-toggle": "tab", "href": "#payments" }).parent().addClass("disabled");
                            }

                            if(typeof oTable !== "undefined")
                                oTable.ajax.url("/" + (window.location.pathname).split("/")[1] + "/services/getservices?order=ser_date desc").load();
                        }

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });
                    },

                    error: function () {
                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "left",
                            multiline: true
                        });
                    }
                });
            } else
                $("#notExecuted").parent().addClass("active").prev().removeClass("active");
        }

        if(status == "EXECUTED" && !flag) {
            notif_confirm({
                "message": "You want to change the status of the service to executed?",
                "textaccept": "Yes, I want",
                "textcancel": "Cancel",
                "fullscreen": true,
                "callback": exeSerCallback
            });
        } else {
            notif_confirm({
                "message": "You want to change the status of the service to not executed?",
                "textaccept": "Yes, I want",
                "textcancel": "Cancel",
                "fullscreen": true,
                "callback": exeSerCallback
            });
        }
    });

    // Ubicar el foco en campo de servicio adicional
    $("#linAdditional").on("click", function() {
        setTimeout(function() {
            $("input#addiService").focus();
        }, 500);
    });

    // Tabla de los servicios adicionales
    tabServices = $("#tabServices").DataTable({
        "paging": true, 
        "lengthChange":false,
        "iDisplayLength": 5,
        "aaSorting": [],
        "searching": false,
        "ordering": false,
        "info": false,
        "data": [],
        "responsive": true,

        "fnDrawCallback": function(settings, json) {
            $(".tooltips").tooltip();
        },

        "aoColumns": [
            { "mData": "index" },
            { "mData": "addService" },
            { "mData": "amount" },
            { "mData": "creDate" },
            { "mData": "actions", "sClass": "text-center" }
        ],
    });

    // Tabla de los pagos realizados
    tabPayments = $("#tabPayments").DataTable({
        "paging": true, 
        "lengthChange":false,
        "iDisplayLength": 5,
        "aaSorting": [],
        "searching": false,
		"ordering": false,
		"info": false,
        "data": [],

        "fnDrawCallback": function(settings, json) {
            $(".tooltips").tooltip();
        },

        "aoColumns": [
            { "mData": "index" },
            { "mData": "payDate" },
            { "mData": "amount" },
            { "mData": "payType" },
            { "mData": "actions", "sClass": "text-center" }
        ],
    });

    // Formatear campos de monto
    $(".amount").priceFormat({
        prefix: "",
        centsSeparator: ".",
        thousandsSeparator: ","
    });

    // Agregación de nuevo servicio adicional
    var addService = $("#addService");
    addService.on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forService");
        var str = $f.serialize();

        if($f.parsley().validate()) {
            var $this = $(this);
            $this.addClass("disabled");

            $.ajax({  
                type: "POST",
                url: "/" + (window.location.pathname).split("/")[1] + "/additional/registrationbd?contract=" + $this.attr("data-contract-id") + "&serDate=" + $this.attr("data-service-date"),
                dataType: "json",
                data: str,

                beforeSend: function() { 
                    $f.data("locked", true);
                },

                success: function(respuesta) {
                    addService.removeClass("disabled");

                    if(respuesta.type != "error") {
                        var balance = Math.abs(respuesta.balance) == 0 ? "0.00" : respuesta.balance;

                        $("#addiService").focus();
                        $("#totAdditional, #tilAdditional").text(respuesta.totAdditional);
                        $("#tilAmount").text(respuesta.amount);
                        $("#balance").text(balance);
                        $("#forService input").val("");
                        tabServices.ajax.url("/" + (window.location.pathname).split("/")[1] + "/additional/getadditional?service=" + respuesta.service).load();

                        if(typeof oTable !== "undefined")
                            oTable.ajax.url("/" + (window.location.pathname).split("/")[1] + "/services/getservices?order=ser_date desc").load();
                    }

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "left",
                        multiline: true
                    });                        
                },

                error: function () {
                    addService.removeClass("disabled");

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "left",
                        multiline: true
                    });
                },

                complete: function() { 
                    $f.data("locked", false);
                }               
            });
        }

        return false;   
    });

    // Agregación de nuevo pago
    var addPayment = $("#addPayment");
    addPayment.on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forPayment");
        var str = $f.serialize();

        if($f.parsley().validate()) {
            var $this = $(this);
            $this.addClass("disabled");

            $.ajax({  
                type: "POST",
                url: "/" + (window.location.pathname).split("/")[1] + "/payments/registrationbd?contract=" + $this.attr("data-contract-id") + "&serDate=" + $this.attr("data-service-date"),
                dataType: "json",
                data: str,

                beforeSend: function() { 
                    $f.data("locked", true);
                },

                success: function(respuesta) {
                    addPayment.removeClass("disabled");

                    if(respuesta.type != "error") {
                        var status = respuesta.status == "true" ? "Paid" : "Unpaid";
                        var balance = Math.abs(respuesta.balance) == 0 ? "0.00" : respuesta.balance;

                        $("#payType").val(null).trigger("change").next().empty();
                        $("#inpAmount2").val("");
                        $("#balance").text(balance);
                        $("#totPayments").text(respuesta.totPayments);
                        $("#status").text(status + " Service");
                        tabPayments.ajax.url("/" + (window.location.pathname).split("/")[1] + "/payments/getpayments?service=" + respuesta.service).load();

                        if(typeof oTable !== "undefined")
                            oTable.ajax.url("/" + (window.location.pathname).split("/")[1] + "/services/getservices?order=ser_date desc").load();
                    }

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "left",
                        multiline: true
                    });                        
                },

                error: function () {
                    addPayment.removeClass("disabled");

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "left",
                        multiline: true
                    });
                },

                complete: function() { 
                    $f.data("locked", false);
                }               
            });
        }

        return false;   
    });

    // Validación de campos de monto
    window.ParsleyValidator.addValidator("amount", function(value) {
        var response = value == "0.00" ? false : true;
        return response;
    });

    // Preparación de modal para ver servicio del día
    seeService = function(cliCode, conCode, endFormat) {
        $.post("/" + (window.location.pathname).split("/")[1] + "/clients/getclient/", { client: cliCode, type: "code" }, function(data) {
            var client = $.parseJSON(data);

            $.post("/" + (window.location.pathname).split("/")[1] + "/contracts/getcontract/", { contract: conCode, serDate: endFormat }, function(data) {
                var contract = $.parseJSON(data);
                var status = contract.paid == "true" ? "Paid" : "Unpaid";                   

                // Preparación tab "contract"
                $("#serPayment, #service").text(contract.contract.con_service);
                $("#status").text(status + " Service");
                $("#conCode").text(PadLeft(contract.contract.id, 8));
                $("#cliCode").text(client.cli_code);
                $("#cliName").text(client.cli_name);
                $("#conDate").text(contract.contract.con_day + ", " + moment(contract.contract.con_date).format("MM/DD/YYYY"));
                $("#address").text(client.cli_address);
                $("#phone").text(client.cli_phone);
                $("#mobPhone").text(client.cli_mob_phone);
                $("#email").text(client.cli_email);
                $("#payment, #contracted").text(number_format(contract.contract.con_pay));
                $("#group").text(contract.group);
                $("#position").text(contract.contract.con_position);
                $("#frequency").text(contract.contract.con_frequency);
                flag = false;

                // Seleccionar el estatus del servicio
                var executed = $("#executed");
                var notExecuted = $("#notExecuted");
                var forService = $("#forService");
                var linPayments = $("#linPayments");
                var balance = Math.abs(contract.balance) == 0 ? "0.00" : contract.balance;

                if(contract.status == "EXECUTED") {
                    executed.parent().addClass("active");
                    notExecuted.parent().removeClass("active");
                    forService.hide();
                    linPayments.attr({ "data-toggle": "tab", "href": "#payments" }).parent().removeClass("disabled");
                    flag = true;
                } else {
                    notExecuted.parent().addClass("active");
                    executed.parent().removeClass("active");
                    forService.show();
                    linPayments.removeAttr("data-toggle href").parent().addClass("disabled");
                }

                // ******************************** //

                // Preparación tab "additional"
                $("#addiService, #inpAmount1, #inpAmount2").val("");
                $("#tabServices").next().find("div").eq(0).html("<h3 class='mt0 text-danger' style='font-weight: bold'>Total: $ <span id='totAdditional'>" + number_format(contract.additional) + "</span></h3>");

                // Preparación tab "payments"
                $("#tilAdditional").text(number_format(contract.additional));
                $("#tilAmount").text(number_format(contract.amount));
                $("#balance").text(number_format(balance));
                $("#payType").val(null).trigger("change");
                $("#tabPayments").next().find("div").eq(0).html("<h3 class='mt0 text-danger' style='font-weight: bold'>Total: $ <span id='totPayments'>" + number_format(contract.payments) + "</span></h3>");

                // Otros ajustes
                $("#addService, #addPayment").removeAttr("readonly").attr({ "data-contract-id": contract.contract.id, "data-service-date": endFormat });
                $(".help-block").find("span").empty();
                $("#linContract").click();
                
                // Refrescar tabla de servicios y pagos
                tabServices.clear().ajax.url("/" + (window.location.pathname).split("/")[1] + "/additional/getadditional?service=" + contract.service).load().draw();
                tabPayments.clear().ajax.url("/" + (window.location.pathname).split("/")[1] + "/payments/getpayments?service=" + contract.service).load().draw();

                    $("#modAdditional").on("shown.bs.modal", function () {
                        tabServices.columns.adjust().responsive.recalc();
                        //$("#ajaxSpinner").hide();
                        //$("#services_wrapper").css("visibility", "visible");
                    });

                // Mostrar ventana modal
                $("#modServices").modal("hide");
                $("#modAdditional").modal("show");
            });
        });
    }

    // Eliminación de pagos
    delPayment = function(codigo) {
        var modAdditional = $("#modAdditional");
            modAdditional.modal("hide");

        delPayCallback = function(choice) {
            if(choice) {
                $.ajax({  
                    type: "POST",
                    url: "/" + (window.location.pathname).split("/")[1] + "/payments/delpayment",
                    dataType: "json",

                    data: {
                        codigo: codigo,
                    },

                    success: function(respuesta) {
                        modAdditional.modal("show");

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });

                        if(respuesta.type != "error") {
                            var balance = Math.abs(respuesta.balance) == 0 ? "0.00" : respuesta.balance;
                            var status = respuesta.status == "true" ? "Paid" : "Unpaid";

                            tabPayments.clear().ajax.url("/" + (window.location.pathname).split("/")[1] + "/payments/getpayments?service=" + respuesta.service).load().draw();
                            $("#balance").text(balance);
                            $("#totPayments").text(respuesta.totPayments);
                            $("#status").text(status + " Service");
                        }
                    },

                    error: function () {
                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center"
                        });
                    }
                });

                return false;               
            } else
                modAdditional.modal("show");
        }

        notif_confirm({
            "message": "Are you sure? You won't be able to revert this!",
            "textaccept": "Yes, delete it!",
            "textcancel": "Cancel",
            "fullscreen": true,
            "callback": delPayCallback
        });
    }

    // Eliminación de servicios adicionales
    delAdditional = function(codigo) {
        var modAdditional = $("#modAdditional");
            modAdditional.modal("hide");

        delAddCallback = function(choice) {
            if(choice) {
                $.ajax({  
                    type: "POST",
                    url: "/" + (window.location.pathname).split("/")[1] + "/additional/deladditional",
                    dataType: "json",

                    data: {
                        codigo: codigo,
                    },

                    success: function(respuesta) {
                        modAdditional.modal("show");

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });

                        if(respuesta.type != "error") {
                            var balance = Math.abs(respuesta.balance) == 0 ? "0.00" : respuesta.balance;

                            tabServices.clear().ajax.url("/" + (window.location.pathname).split("/")[1] + "/additional/getadditional?service=" + respuesta.service).load().draw();
                            $("#totAdditional, #tilAdditional").text(respuesta.totAdditional);
                            $("#tilAmount").text(respuesta.amount);
                            $("#balance").text(balance);
                        }
                    },

                    error: function () {
                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center"
                        });
                    }
                });

                return false;               
            } else
                modAdditional.modal("show");
        }

        notif_confirm({
            "message": "Are you sure? You won't be able to revert this!",
            "textaccept": "Yes, delete it!",
            "textcancel": "Cancel",
            "fullscreen": true,
            "callback": delAddCallback
        });
    }
});