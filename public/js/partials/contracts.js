$(document).ready(function() {
	$("#group").select2({width: "100%"}).on("change", function(e) {
        var conDate = $("#conDate").val();

        if(conDate != "")
            getContract(conDate, $(this).val());
    });

    // Checkboxes y radio buttons personalizados
    $(".icheck input").iCheck({
        radioClass: "iradio_minimal-red"
    });

    // Formatear campos de monto
    $("#pay").priceFormat({
        prefix: "",
        centsSeparator: ".",
        thousandsSeparator: ""
    });

    window.ParsleyValidator.addValidator("pay", function(value) {
        var response = value == "0,00" ? false : true;
        return response;
    });

    getContract = function(conDate, group) {
        $.post("../contracts/getnumcontracts", { conDate: conDate, group: group }, function(data) {
            $("#position").val(Number(data) + Number(1));
        });
    }

    $("#conDate").datepicker({
        todayHighlight: true,
        startView: 1,
        format: "DD, mm/dd/yyyy",
        autoclose: true
    }).on("changeDate", function() {
        var group = $("#group").val();
        var $this = $(this);
        $this.parent().next().remove();

        if(group != "")
            getContract($this.val(), group);
    });
});