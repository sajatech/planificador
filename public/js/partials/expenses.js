$(document).ready(function() {
    $("#concept").select2({width: "100%"});

    $("#expDate").datepicker({
        todayHighlight: true,
        startView: 1,
        autoclose: true
    });

    // Formatear campo de costo
    $("#cost").priceFormat({
        prefix: "",
        centsSeparator: ".",
        thousandsSeparator: ","
    });

    window.ParsleyValidator.addValidator("cost", function(value) {
        var response = value == "0.00" ? false : true;
        return response;
    });

    $("#quantity, #cost").on("keyup", function() {
        $("#total").val(number_format(Number($("#quantity").val()) * Number(replace_number($("#cost").val()))));
    });

	// Registro de gastos
	$("#regExpense").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forExpense");
        var loading = $("#loaExpense");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../expenses/registrationbd?status=" + $(".status").find(".active").find(".estEnabled").val(),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "center",
                            multiline: true
                        });
                        
                        if(respuesta.type != "error")
                            $("#canExpense").click();
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "center",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Resetear formulario
    $("#canExpense").on("click", function () {
        $("#forExpense").each(function() {
            this.reset();
        });

        $("#concept").val(null).trigger("change");
        $("#description").focus();

        return false;
    });
});