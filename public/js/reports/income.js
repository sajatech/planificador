$(document).ready(function() {
	$("#client, #payment").select2({width: "100%"});

    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
    	$(".datepicker").hide();
    });

    // Consulta de resumen de ingresos
    $("#consult").on("click", function () {
    	$(this).blur();

        // Inicialización de variables
        var loading = $("#loaIncome");
        var result = $("#result");

        if($("#forIncome").parsley().validate()) { 
        	loading.show().removeClass("hide");

	     	result.empty().removeClass("p20").load("../reports/getincome", { "since": $("#since").val(), "until": $("#until").val(), "client": $("#client").val(), "payment": $("#payment").val() }, function(response, status) {
	     		var genincomepdf = $("#genincomepdf");

	            loading.hide();

	     		if(status == "error") {
	                notif({
	                    msg: "Sucedió un error, contacte con el administrador del sistema.",
	                    type: "error",
	                    position: "left",
	                    multiline: true
	                });
	     		}

	            if(response == "") {
	                genincomepdf.addClass("disabled");
	                result.addClass("p20").html("<div class='alert alert-info mb-n'><i class='fa fa-fw fa-info-circle'></i>&nbsp; No income was found for the specified parameters. Enter the data again.</div>");
	            } else {
					var str = $("#forIncome").serialize();
					var tabIncome = $("#tabIncome");
	                
	                genincomepdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reports/income_summary?" + str + "', 'Income Summary', 'width=240, height=600')");
					loading.hide();

				    // Tabla de los ingresos
				    tabIncomeDat = tabIncome.DataTable({
				        "searching": false,
				        "paging": true, 
				        "info": false,         
				        "lengthChange": false,
				        "iDisplayLength": 50,
				        "aaSorting": [],
            			"responsive": true
				    });

        			new $.fn.dataTable.FixedHeader(tabIncomeDat);

                	tabIncome.next().find("div").eq(0).html("<h3 class='mt0 text-danger total' style='font-weight: bold'>Total: $ " + $("#total").text() + "</h3>");
				}
	     	});

	    } else
	    	$(".help-block").remove();

        return false;
    });
});