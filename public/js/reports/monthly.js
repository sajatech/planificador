$(document).ready(function() {	
    $(".input-small").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: "mm/yyyy",
		viewMode: "months",
		minViewMode: "months"   
	});

    // Consulta de servicios mensuales
    $("#consult").on("click", function () {
    	$(this).blur();

        // Inicialización de variables
        var loading = $("#loaMonthly");
        var result = $("#result");

        if($("#forMonthly").parsley().validate()) { 
        	loading.show().removeClass("hide");

	     	result.empty().removeClass("p20").load("../reports/getmonthly", { "month": $("#month").val() }, function(response, status) {
	     		var genmonthlypdf = $("#genmonthlypdf");

	            loading.hide();

	     		if(status == "error") {
	                notif({
	                    msg: "Sucedió un error, contacte con el administrador del sistema.",
	                    type: "error",
	                    position: "left",
	                    multiline: true
	                });
	     		}

	            if(response == "") {
	                genmonthlypdf.addClass("disabled");
	                result.addClass("p20").html("<div class='alert alert-info mb-n'><i class='fa fa-fw fa-info-circle'></i>&nbsp; No services was found for the specified parameters. Enter the data again.</div>");
	            } else {
					var str = $("#forMonthly").serialize();
					var tabMonthly = $("#tabMonthly");

	                genmonthlypdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reports/monthly_report?" + str + "', 'Income Summary', 'width=240, height=600')");
					loading.hide();
				}
	     	});
	    } else
	    	$(".help-block").remove();

        return false;
    });
});