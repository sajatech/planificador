$(document).ready(function() {
    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
    	$(".datepicker").hide();
    });

    // Consulta de clientes con pagos pendientes
    $("#consult").on("click", function () {
    	$(this).blur();

        // Inicialización de variables
        var loading = $("#loaPending");
        var result = $("#result");
        var forPending = $("#forPending");

        if(forPending.parsley().validate()) { 
        	loading.show().removeClass("hide");

	     	result.empty().removeClass("p20").load("../reports/getpending", { "since": $("#since").val(), "until": $("#until").val() }, function(response, status) {
	     		var genpendingpdf = $("#genpendingpdf");

	            loading.hide();

	     		if(status == "error") {
	                notif({
	                    msg: "Sucedió un error, contacte con el administrador del sistema.",
	                    type: "error",
	                    position: "left",
	                    multiline: true
	                });
	     		}

	            if(response == "") {
	                genpendingpdf.addClass("disabled");
	                result.addClass("p20").html("<div class='alert alert-info mb-n'><i class='fa fa-fw fa-info-circle'></i>&nbsp; No payments was found for the specified parameters. Enter the data again.</div>");
	            } else {
					var str = forPending.serialize();
					var tabPending = $("#tabPending");
	                
	                genpendingpdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reports/pending_payments?" + str + "', 'Pending Payments', 'width=240, height=600')");
					loading.hide();

				    // Tabla de los ingresos
				    tabPendingDat = tabPending.DataTable({
				        "searching": false,
				        "paging": true, 
				        "info": false,         
				        "lengthChange": false,
				        "iDisplayLength": 50,
				        "aaSorting": [],
				        "responsive": true
				    });

        			new $.fn.dataTable.FixedHeader(tabPendingDat);

                	tabPending.next().find("div").eq(0).html("<h3 class='mt0 text-danger total' style='font-weight: bold'>Total: $ " + $("#total").text() + "</h3>");
				}
	     	});

	    } else
	    	$(".help-block").remove();

        return false;
    });
});