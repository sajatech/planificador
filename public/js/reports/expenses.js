$(document).ready(function() {
    $(".input-small").datepicker({
        todayHighlight: true,
        startView: 1
    }).change(function() {
    	$(".datepicker").hide();
    });

	$("#concept").select2({width: "100%"});

    // Consulta de gastos emitidos
    $("#consult").on("click", function () { 
    	$(this).blur();

        // Inicialización de variables
        var loading = $("#loaExpense");
        var result = $("#result");
        var forExpense = $("#forExpense");

        if(forExpense.parsley().validate()) {
        	loading.show().removeClass("hide");

	     	result.empty().removeClass("p20").load("../reports/getexpenses", { "since": $("#since").val(), "until": $("#until").val(), "concept": $("#concept").val() }, function(response, status) {
	     		var genexpensespdf = $("#genexpensespdf");

	            loading.hide();

	     		if(status == "error") {
	                notif({
	                    msg: "Sucedió un error, contacte con el administrador del sistema.",
	                    type: "error",
	                    position: "left",
	                    multiline: true
	                });
	     		}

	            if(response == "") {
	                genexpensespdf.addClass("disabled");
	                result.addClass("p20").html("<div class='alert alert-info mb-n'><i class='fa fa-fw fa-info-circle'></i>&nbsp; No expenses was found for the specified parameters. Enter the data again.</div>");
	            } else {
					var str = forExpense.serialize();
					var tabExpenses = $("#tabExpenses");
	                
	                genexpensespdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reports/issued_expenses?" + str + "', 'Issued Expenses', 'width=240, height=600')");
					loading.hide();

				    // Tabla de los gastos
				    tabExpensesDat = tabExpenses.DataTable({
				        "searching": false,
				        "paging": true, 
				        "info": false,         
				        "lengthChange": false,
				        "iDisplayLength": 50,
				        "aaSorting": [],
				        "responsive": true
				    });

        			new $.fn.dataTable.FixedHeader(tabExpensesDat);

                	tabExpenses.next().find("div").eq(0).html("<h3 class='mt0 text-danger total' style='font-weight: bold'>Total: $ " + $("#total").text() + "</h3>");
				}
	     	});

	    } else
	    	$(".help-block").remove();

        return false;
    });
});