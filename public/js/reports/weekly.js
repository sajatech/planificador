$(document).ready(function() {	
	var calculate_week_range = function(e) {
        var input = e.currentTarget;

        $("body").find(".datepicker-days table tbody tr").removeClass("week-active");

        var tr = $("body").find(".datepicker-days table tbody tr td.active.day").parent();
        
        tr.addClass("week-active");

        var date = e.date;

        if(date != undefined) {
	        var start_date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	        var end_date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
	       	var friendly_string = ((start_date.getMonth() + 1) < 10 ? "0" : "") + (start_date.getMonth() + 1) + "/" + (start_date.getDate() < 10 ? "0" : "") + start_date.getDate() + "/" + start_date.getFullYear()  + " to " + ((end_date.getMonth() + 1) < 10 ? "0" : "") + (end_date.getMonth() + 1) + "/" + (end_date.getDate() < 10 ? "0" : "") + end_date.getDate() + "/" + end_date.getFullYear();

	        $(input).val(friendly_string);
        }
    }

    $(".input-small").datepicker({
        todayHighlight: true,
        autoclose: true,
        calendarWeeks: true,
    }).on("show", function(e) {
        var tr = $("body").find(".datepicker-days table tbody tr");

        tr.mouseover(function() {
            $(this).addClass("week");
        });

        tr.mouseout(function() {
            $(this).removeClass("week");
        });

        calculate_week_range(e);

    }).on("hide", function(e) {
        calculate_week_range(e);
    });

    // Consulta de servicios semanales
    $("#consult").on("click", function () {
    	$(this).blur();

        // Inicialización de variables
        var loading = $("#loaWeekly");
        var result = $("#result");

        if($("#forWeekly").parsley().validate()) { 
        	loading.show().removeClass("hide");

	     	result.empty().removeClass("p20").load("../reports/getweekly", { "week": $("#week").val() }, function(response, status) {
	     		var genincomepdf = $("#genincomepdf");

	            loading.hide();

	     		if(status == "error") {
	                notif({
	                    msg: "Sucedió un error, contacte con el administrador del sistema.",
	                    type: "error",
	                    position: "left",
	                    multiline: true
	                });
	     		}

	            if(response == "") {
	                genincomepdf.addClass("disabled");
	                result.addClass("p20").html("<div class='alert alert-info mb-n'><i class='fa fa-fw fa-info-circle'></i>&nbsp; No services was found for the specified parameters. Enter the data again.</div>");
	            } else {
					var str = $("#forWeekly").serialize();
					var tabIncome = $("#tabIncome");
	                
	                genincomepdf.removeClass("disabled").attr("onclick", "window.open('/" + (window.location.pathname).split("/")[1] + "/reports/weekly_report?" + str + "', 'Income Summary', 'width=240, height=600')");
					loading.hide();

				    // Tabla de los ingresos
				    tabIncome = tabIncome.dataTable({
				        "searching": false,
				        "paging": true, 
				        "info": false,         
				        "lengthChange": false,
				        "iDisplayLength": 50,
				        "aaSorting": []
				    });

                	tabIncome.next().find("div").eq(0).html("<h3 class='mt0 text-danger' style='font-weight: bold'>Total: $ " + $("#total").text() + "</h3>");
				}
	     	});

	    } else
	    	$(".help-block").remove();

        return false;
    });
});