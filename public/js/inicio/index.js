$(document).ready(function() {
	setTimeout(function() {
		$("input#username").focus();
	}, 500);

	$("#login").on("click", function () {
        var loading = $("#loaLogin");
        // Se guarda la referencia al formulario
        var $f = $("#forLogin");

		if($f.parsley().validate()) { 
			loading.show().removeClass("hide");
            var str = $f.serialize();

		  	// Bloqueo de peticiones repetidas
		  	if($f.data("locked") == undefined || !$f.data("locked")) { 
		  		$.ajax({
		            type: "POST",
		            url: "inicio/login",
		            data: str,

		            beforeSend: function() {
		            	$f.data("locked", true);
		            },

		            success: function(respuesta) {
		            	if(respuesta == 0)
							$.redirect("/" + (window.location.pathname).split("/")[1] + "/desktop");
						else {
							loading.hide();

		                    notif({
		                     	msg: respuesta,
		                    	type: "error",
		                     	position: "left",
                            	multiline: true
		                    });
						}
					},

					complete: function() {
						$f.data("locked", false);
					}
	        	});

	        	return false;
		  	}
		}
	});
});