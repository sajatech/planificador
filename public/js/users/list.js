$(document).ready(function() {
    $.get("../users/getusers", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panUsers").removeClass("hide").show();

        // Tabla de los usuarios
        oTable = $("#users").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],
            "responsive": true,

            "aoColumns": [
                { "mData": "useCode" },
                { "mData": "name" },
                { "mData": "email" },
                { "mData": "useRole" },
                { "mData": "username" },
                { "mData": "status" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "New", 

                        "fnInit": function(button) {
                            $(button).addClass("");
                            
                            /*if(data.privilegios.includes(1))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/users/registration");
                        }

                    },
                    
                    { "sExtends": "editor_edit", "sButtonText": "Update/View",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData(); 
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/users/update", { "code": node[0].useCode });
                        }

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Enable/Disable",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }*/
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();  
                            disUser(node);
                        }
                      
                    }
                ]
            },

            "language": {
                "lengthMenu": "_MENU_"
            },

            "iDisplayLength": 50  
        });

        $("#users_filter input").addClass("form-control").attr("placeholder", "Find User...");
        $("#users_length select").addClass("form-control");

        // Se añaden los íconos
        $("#ToolTables_users_0").prepend("<i class='fa fa-plus'/></i> ");
        $("#ToolTables_users_1").prepend("<i class='fa fa-pencil'/></i> ");
        $("#ToolTables_users_2").prepend("<i class='fa fa-exchange'/></i> ");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#users_length').addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#users_filter").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left"));

        $(".panel-footer").append($("#users+.row")).addClass("p10");
        $("#users_paginate>ul.pagination").addClass("pull-right m0");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });

    // Desactivación/Activación de usuario
    disUser = function(node) {
        $("#ToolTables_users_2").addClass("disabled");

        var url = (node[0].status.replace(/<[^>]*>?/g, "") == "ENABLED") ? "../users/disuser" : "../users/enauser";

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

            data: { 
                code: node[0].useCode
            },

            success: function(respuesta) {
                oTable.ajax.url("../users/getusers").load();
                $("#ToolTables_users_1, #ToolTables_users_2").addClass("disabled");

                notif({
                    msg: respuesta.text,
                    type: respuesta.type,
                    position: "left",
                    multiline: true
                });
            },

            error: function () {
                $("#ToolTables_users_2").removeClass("disabled");

                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "left",
                    multiline: true
                });
            }
        });                
        
        return false; 
    }
});