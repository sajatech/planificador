$(document).ready(function() {
    setTimeout(function() {
        $("input#useCode").focus();
    }, 500);

    $("#parsley-id-multiple-status").remove();

	// Registro de usuarios
	$("#regUser").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forUser");
        var loading = $("#loaUser");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../users/registrationbd?status=" + $(".status").find(".active").find(".estEnabled").val(),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });

                        if(respuesta.type != "error")
                            $("#canUser").click();
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "left",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Resetear formulario
    $("#canUser").on("click", function () {
        $("#forUser").each(function() {
            this.reset();
        });

        $("#useRole").val(null).trigger("change");
        $("#forUser").parsley().destroy();
        $("#useCode").focus();

        return false;
    });
});