$(document).ready(function() {
    $("#parsley-id-multiple-status").remove();

    // Desactivación/Activación de usuario
    disUser = function(action, code) {
        if(code) {
            var disabled = action == "enauser" ? "enabled" : "disabled";
            
            $("#" + disabled).parent().addClass("disabled");

            $.ajax({  
                type: "POST",
                url: "../users/" + action,
                dataType: "json",

                data: { 
                    code: code
                },

                success: function(respuesta) {
                    $("#" + disabled).parent().removeClass("disabled");

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "left",
                        multiline: true
                    });
                },

                error: function () {
                    $("#" + disabled).parent().removeClass("disabled");

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "left",
                        multiline: true
                    });
                }
            });          
            
            return false;
        }
    }

	// Actualización de usuario
	$("#updUser").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forUser");
        var loading = $("#loaUser");

        if($("#password").val() == "") {
            $f.parsley().destroy();
            $("#password, #pasConfirmation").removeAttr("required");
        }

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../users/updatebd?id=" + $(this).attr("data-user-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });

                        if(respuesta.type != "error") {
                            $("#enabled").parent().attr("onclick", "disUser('enauser', '" + respuesta.code + "')");
                            $("#disabled").parent().attr("onclick", "disUser('disuser', '" + respuesta.code + "')");
                            $("#password, #pasConfirmation").attr("required", "required")
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "left",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});
});