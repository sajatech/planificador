$(document).ready(function() {
    $("#parsley-id-multiple-status").remove();

    // Desactivación/Activación de gasto
    disExpense = function(action, code) {
        if(code) {
            var disabled = action == "enaexpense" ? "enabled" : "disabled";
            
            $("#" + disabled).parent().addClass("disabled");

            $.ajax({  
                type: "POST",
                url: "../expenses/" + action,
                dataType: "json",

                data: { 
                    code: code
                },

                success: function(respuesta) {
                    $("#" + disabled).parent().removeClass("disabled");

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "left",
                        multiline: true
                    });
                },

                error: function () {
                    $("#" + disabled).parent().removeClass("disabled");

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "left",
                        multiline: true
                    });
                }
            });          
            
            return false;
        }
    }

	// Actualización de gasto
	$("#updExpense").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forExpense");
        var loading = $("#loaExpense");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../expenses/updatebd?id=" + $(this).attr("data-expense-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() { 
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "left",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});
});