$(document).ready(function() {
	// Inicialización de variables
    var arrEvent = [];
    var calendar = false;
    var dataSet;
    
    var html = '<div class="dd" id="nestable_list_1">';
		html += '<ol class="dd-list">';
    
    var i = 1;
    var list, ordering;
    var regList = $("#regList");
    var reoList = $("#reoList");
    var $return = $("#return");
    var tabServices = $("#services-content").html();
    var tabSerFill;

	$("#payType").select2({width: "100%"});

	// Planificador mensual de servicios
	$("#calendar").fullCalendar({
	    defaultDate: moment(),
	    
	    header: {
	        left: "prevYear,nextYear",
	        center: "title",
	        right: "prev,next today"
	    },

	    defaultView: "month",
	    height: 426,
		selectable: true,
		displayEventTime: false,

        buttonText: {
            today: " ",
        },

        viewRender: function() {
            !calendar ? $(".fc-right").prepend("<span id='loaCalendar' class='hide'><img src='/" + (window.location.pathname).split("/")[1] + "/img/ajax.svg'/></span>") : "";
            calendar = true;
        },

	    eventRender: function(event) { 
	    	// Función que verifica si ya existe un servicio en el día
			function isCalendar(service) { 
				return service.date == event.end.format("YYYY-MM-DD");
			}

			// Función que devuelve la posición del servicio en el arreglo
			function findPosition(service) {
			  	return service.date == event.end.format("YYYY-MM-DD");
			}

            var position = arrEvent.findIndex(findPosition);

			if(event.ranges.filter(function(range) {
				if(arrEvent.find(isCalendar) == undefined && event.end.isAfter(moment(range.start)) && (event.end.isBefore(moment(range.end)) || event.end.format("YYYY-MM-DD") == moment(range.end).format("YYYY-MM-DD")) && event.end.diff(moment(range.start), "days") >= 0 && event.end.diff(moment(range.start), "days") % event.frequency == 0) {
					event.default = i;
					arrEvent.push({ date: event.end.format("YYYY-MM-DD"), quantity: 1, events: [event], backgroundColor: event.backgroundColor });
					i++;
					
					return true;
				} else {
					if(event.end.isAfter(moment(range.start)) && event.end.isBefore(moment(range.end)) && event.end.diff(moment(range.start), "days") >= 0 && event.end.diff(moment(range.start), "days") % event.frequency == 0) {
						if(arrEvent[position] != undefined) {
                            event.end.isAfter(moment(range.end)) || event.end.format("YYYY-MM-DD") == moment(range.end).format("YYYY-MM-DD") ? arrEvent[position].quantity-- : arrEvent[position].quantity++;
                            event.default = i;
                            arrEvent[position].backgroundColor = event.backgroundColor;
							arrEvent[position].events.push(event);
							i++;
						}
					}
					
					return false;
				}
			}).length > 0) {
				return $("<div data-service-date='" + event.end.format("YYYY-MM-DD") + "' data-service-index=" + arrEvent.findIndex(findPosition) + "></div>");
			} else
				return false;
	    },

	    events: function(start, end, timezone, callback) {
		    $.get("/" + (window.location.pathname).split("/")[1] + "/services/getevents/", function(data) {
	        	callback($.parseJSON(data));
		    });
	    },

		loading: function(bool) {
            var loaCalendar = $("#loaCalendar");
			bool ? loaCalendar.removeClass("hide") : loaCalendar.addClass("hide");
		},
		
     	eventAfterAllRender: function() { 
     		$("div.fc-week .fc-content-skeleton table tbody tr td").each(function() {
     			var $this = $(this);

     			if($this.attr("class") == "fc-event-container") {
     				var date = $this.find("div").attr("data-service-date");

					function findPosition(service) {
					  	return service.date == date;
					}

					var position = arrEvent.findIndex(findPosition);
					var word = arrEvent[position].quantity > 1 ? " Services" : " Service";

 					$this.find("div").html("<a class='fc-day-grid-event fc-h-event fc-event fc-start fc-end' style='font-size: 15px !important; background-color: " + arrEvent[position].backgroundColor + "'><div class='fc-content'><span class='fc-title'>" + arrEvent[position].quantity + "<span class='hidden-xs hidden-sm'>" + word + "</span><span class='visible-sm'> Ser.</span></span></div></a>");
     			}
     		});

     		$(".fc-prevYear-button, .fc-nextYear-button").addClass("hidden-xs");
 		},

		eventClick: function(calEvent) {
			var modServices = $("#modServices");

            $this = $(this);
			getpositions();
		}
	});

	// Vaciar arreglo de eventos al cambiar vista de calendario
	$(".fc-prevYear-button, .fc-nextYear-button, .fc-prev-button, .fc-next-button, .fc-today-button").on("click", function() {
		arrEvent = []
	});

    reoList.on("click", function() {
		html += '</ol>';
		html += '</div>';
    	$(this).hide();
    	regList.removeClass("hide").show();
    	$return.removeClass("hide").show();
    	$("#services-content").html(html);

		var height = Number(($(".modal-content").css("height")).replace("px", ""));

		if(height > 639)
			$(".modal-backdrop").css("height", (height + 60) + "px");
		
	    $("#nestable_list_1").nestable({
        	group: 1
    	}).on("change", updateOutput);

		function updateOutput(e) {
		    list = e.length ? e : $(e.target),
   		   	output = list.data("output");		    
		}
    });

    regList.on("click", function() { console.log(list.nestable("serialize"));
		var loading = $("#loaService");
		var list2 = list.nestable("serialize");

		loading.show().removeClass("hide");

		if(regList.data("locked") == undefined || !regList.data("locked")) {
	        $.ajax({  
	            type: "POST",
	            url: "/" + (window.location.pathname).split("/")[1] + "/contracts/updpositions",
	            dataType: "json",

	            data: {
	            	serList: list2
	            },

	            beforeSend: function() {
	                regList.data("locked", true);
	            },

	            success: function(respuesta) {
	                loading.hide();

	                notif({
	                    msg: respuesta.text,
	                    type: respuesta.type,
	                    position: "left",
	                    multiline: true
	                });                
	            },

	            error: function () {
	                loading.hide();

	                notif({
	                    msg: "Sucedió un error, contacte con el administrador del sistema.",
	                    type: "error",
	                    position: "left",
	                    multiline: true
	                });
	            },

	            complete: function() { 
	                regList.data("locked", false);
	            }               
	        });
	    }
    });

    $return.on("click", function() {
    	$(this).hide();
		$("#services-content").empty();
    	getpositions();
	});

    getpositions = function() {
        $("#services-content").html(tabServices);
        dataSet = [];
        html = "<div class='dd' id='nestable_list_1'>";
        html += '<ol class="dd-list">';

		var y = 1;
		(arrEvent[$this.attr("data-service-index")].events).forEach(function(service, index) {	
			delete service["start"];
			delete service["source"];

    		$.post("/" + (window.location.pathname).split("/")[1] + "/services/getstatus/", { date: arrEvent[$this.attr("data-service-index")].date, contract: service.conCode }, function(data) {
				var row = [
					null,
					service.conDate,
					service.conCode,
					service.groCode,
					service.cliCode,
					service.client,
					service.service,
					"<span class='label " + (data.status == "EXECUTED" ? "label-success" : "label-danger") + "'>" + data.status + "</span>",
					service.group,
					"<a class='btn btn-md btn-inverse btn-label tooltips pr8' onclick='seeService(\"" + service.cliCode + "\", \"" + service.conCode + "\", \"" + service.end.format("YYYY-MM-DD") + "\")' data-trigger='hover' data-original-title='See Service'><i class='fa fa-search btn-label-custom'></i></a>"
				];

				data.hasOwnProperty("position") ? row.push(Number(data.position)) : row.push(service.default);
				dataSet.push(row);

				if(y == 1) {
				    services = $("#services").DataTable({
				        "paging": true, 
				        "info": false,
				        "lengthChange":false,
				        "iDisplayLength": 10,
				        "searching": false,
				        "ordering": false,
				        "bAutoWidth": false,
				        "responsive": true,

				        "aoColumns": [
				        	null,
				        	{ "visible": false },
				        	{ "visible": false },
				        	{ "visible": false },
				        	{ "visible": false },
				            null,
				            null,
				            null,
				            null,
				            { "sClass": "text-center" },
				        	{ "visible": false }
				        ],

				        "fnDrawCallback": function(settings, json) {
				            $(".tooltips").tooltip();
				        }
				    });
				}

				if((arrEvent[$this.attr("data-service-index")].events).length == y) {
					dataSet.sort(function(a, b) {
						if(a[10] > b[10])
							return 1;
						
						if(a[10] < b[10])
							return -1;
						
						return 0;
					});

					dataSet.forEach(function(item, index) {
						item[0] = Number(index) + 1;
						html += "<li class='dd-item dd3-item' data-date='" + arrEvent[$this.attr("data-service-index")].date + "' data-contract='" + item[2] + "'>";
						html += "<div class='dd-handle dd3-handle'></div>";
						html += "<div class='dd3-content'>" + item[5] + "&nbsp;&nbsp;&nbsp;&nbsp; <span class='label label-success'>" + item[6] + "</span><span class='badge badge-primary pull-right'>" + item[8] + "</span></div>";
						html += '</li>';
					});

		            services.clear().rows.add(dataSet).draw();
		            $("#services_wrapper").css("visibility", "collapse");

					$("#modServices").on("shown.bs.modal", function () {
		            	services.columns.adjust().responsive.recalc();
		            	$("#ajaxSpinner").hide();
		            	$("#services_wrapper").css("visibility", "visible");
					});

		            $("#serDate").text(service.day + ", " + service.end.format("MM/DD/YYYY"));
				    $("#modServices").modal("show");
				}
				
				y++;
    		}, "json");

			/*function getData() {
			    var ajaxSpinner = $("#ajaxSpinner");
			    var services2 = $("#services");

			    return $.ajax({  
			    	type: "POST",
			        url: "/" + (window.location.pathname).split("/")[1] + "/services/getstatus",
			        dataType: "json",

			        data: {
			        	date: arrEvent[$this.attr("data-service-index")].date,
			        	contract: service.conCode
			        },

			        beforeSend: function() {
			            ajaxSpinner.show();
			            services2.hide();
			        },

			        complete: function() {
			            ajaxSpinner.hide();
			            services2.show();
			        }
			    });    
				//return $.post("/" + (window.location.pathname).split("/")[1] + "/services/getstatus/", { date: arrEvent[$this.attr("data-service-index")].date, contract: service.conCode }, null, "json");
			}
			getData().done(function(data) {
				var row = [
					null,
					service.conDate,
					service.conCode,
					service.groCode,
					service.cliCode,
					service.client,
					service.service,
					"<span class='label " + (data.status == "EXECUTED" ? "label-success" : "label-danger") + "'>" + data.status + "</span>",
					service.group,
					"<a class='btn btn-md btn-inverse btn-label tooltips pr8' onclick='seeService(\"" + service.cliCode + "\", \"" + service.conCode + "\", \"" + service.end.format("YYYY-MM-DD") + "\")' data-trigger='hover' data-original-title='See Service'><i class='fa fa-search btn-label-custom'></i></a>"
				];

				data.hasOwnProperty("position") ? row.push(Number(data.position)) : row.push(service.default);
				dataSet.push(row);

				if(y == 1) {
				    services = $("#services").DataTable({
				        "paging": true, 
				        "info": false,
				        "lengthChange":false,
				        "iDisplayLength": 10,
				        "searching": false,
				        "ordering": false,
				        "bAutoWidth": false,
				        "responsive": true,

				        "aoColumns": [
				        	null,
				        	{ "visible": false },
				        	{ "visible": false },
				        	{ "visible": false },
				        	{ "visible": false },
				            null,
				            null,
				            null,
				            null,
				            { "sClass": "text-center" },
				        	{ "visible": false }
				        ],

				        "fnDrawCallback": function(settings, json) {
				            $(".tooltips").tooltip();
				        },

			            "language": {
			                "sEmptyTable": "Processing..."
			            }
				    });
				}

				if((arrEvent[$this.attr("data-service-index")].events).length == y) {
					dataSet.sort(function(a, b) {
						if(a[10] > b[10])
							return 1;
						
						if(a[10] < b[10])
							return -1;
						
						return 0;
					});

					dataSet.forEach(function(item, index) {
						item[0] = Number(index) + 1;
						html += "<li class='dd-item dd3-item' data-date='" + arrEvent[$this.attr("data-service-index")].date + "' data-contract='" + item[2] + "'>";
						html += "<div class='dd-handle dd3-handle'></div>";
						html += "<div class='dd3-content'>" + item[5] + "&nbsp;&nbsp;&nbsp;&nbsp; <span class='label label-success'>" + item[6] + "</span><span class='badge badge-primary pull-right'>" + item[8] + "</span></div>";
						html += '</li>';
					});

		            services.clear().rows.add(dataSet).draw();
				}
				
				y++;
			});*/
		});

		regList.hide();
		$return.hide();
		reoList.show();
    }
});