$(document).ready(function() {
    $.get("../contracts/getcontracts", function(data) {
        var data = JSON.parse(data);

        $("#ajaxSpinner").hide();
        $("#panContracts").removeClass("hide").show();

        // Tabla de los artículos
        oTable = $("#contracts").DataTable({
            "sDom": "<'row'<'col-sm-3'l><'col-sm-6'T><'col-sm-3'f>r>t<'row'<'col-sm-12 col-xs-12 col-md-6'i><'col-sm-12 col-xs-12 col-md-6'p>>",
            "aaData": data.aaData,
            "bServerSide": false,
            "bAutoWidth": false,
            "bDestroy": true,
            "aaSorting": [],
            "responsive": true,

            "aoColumns": [
                { "mData": "conCode", "visible": false },
                { "mData": "client" },
                { "mData": "conDate" },
                { "mData": "service" },
                { "mData": "pay" },
                { "mData": "group" },
                { "mData": "status" }
            ],

            "oTableTools": {
                "sRowSelect": "single",

                "aButtons": [
                    { "sExtends": "editor_create", "sButtonText": "New", 

                        "fnInit": function(button) {
                            $(button).addClass("");
                            
                            /*if(data.privilegios.includes(1))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function() {
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/contracts/registration");
                        }

                    },
                    
                    { "sExtends": "editor_edit", "sButtonText": "Update/View",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(3) || data.privilegios.includes(4))
                                $(button).removeClass("hide");*/
                        },

                        "fnClick": function(button) {
                            var node = this.fnGetSelectedData(); 
                            $.redirect("/" + (window.location.pathname).split("/")[1] + "/contracts/update", { "code": node[0].conCode });
                        }

                    },

                    { "sExtends": "editor_remove", "sButtonText": "Enable/Disable",

                        "fnInit": function(button) { 
                            $(button).addClass("disabled");

                            /*if(data.privilegios.includes(5)) {
                                $(button).removeClass("hide");
                            }*/
                        },

                        "fnClick": function() { 
                            var node = this.fnGetSelectedData();  
                            disContract(node);
                        }
                      
                    }
                ]
            },

            "language": {
                "lengthMenu": "_MENU_"
            },

            "iDisplayLength": 50  
        });

        // Se añaden los íconos
        $("#ToolTables_contracts_0").prepend("<i class='fa fa-plus'/></i> ");
        $("#ToolTables_contracts_1").prepend("<i class='fa fa-pencil'/></i> ");
        $("#ToolTables_contracts_2").prepend("<i class='fa fa-exchange'/></i> ");

        $("#contracts_filter input").addClass("form-control").attr("placeholder", "Find Client...");
        $("#contracts_length select").addClass("form-control");

        // Se añaden los elementos de la tabla al panel
        $(".panel-ctrls").append("<i class='separator'></i>");
        $('.panel-ctrls').append($('#contracts_length').addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append("<i class='separator'></i>");
        $(".panel-ctrls").append($("#contracts_filter").addClass("pull-right").find("label").addClass("panel-ctrls-center"));
        $(".panel-ctrls").append($(".DTTT.btn-group").addClass("pull-left"));

        $(".panel-footer").append($("#contracts+.row")).addClass("p10");
        $("#contracts_paginate>ul.pagination").addClass("pull-right m0");

        new $.fn.dataTable.FixedHeader(oTable);

        // ******************************** //
    });

    // Desactivación/Activación de contrato
    disContract = function(node) {
        $("#ToolTables_contracts_2").addClass("disabled");

        var url = (node[0].status.replace(/<[^>]*>?/g, "") == "ENABLED") ? "../contracts/discontract" : "../contracts/enacontract";

        $.ajax({  
            type: "POST",
            url: url,
            dataType: "json",

            data: { 
                code: node[0].conCode
            },

            success: function(respuesta) {
                oTable.ajax.url("../contracts/getcontracts").load();
                $("#ToolTables_contracts_1, #ToolTables_contracts_2, #ToolTables_contracts_3").addClass("disabled");

                notif({
                    msg: respuesta.text,
                    type: respuesta.type,
                    position: "left",
                    multiline: true
                });
            },

            error: function () {
                $("#ToolTables_contracts_2").removeClass("disabled");

                notif({
                    msg: "Sucedió un error, contacte con el administrador del sistema.",
                    type: "error",
                    position: "left",
                    multiline: true
                });
            }
        });                
        
        return false; 
    }
});