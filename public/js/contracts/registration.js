$(document).ready(function() {
    setTimeout(function() {
        $("input#cliCode").focus();
    }, 500);

    $("#parsley-id-multiple-frequency, #parsley-id-multiple-status, #parsley-id-multiple-docType").remove();

	// Registro de contratos
	$("#regContract").on("click", function() {
        var loading = $("#loaContract");
        // Se guarda la referencia al formulario
        var $f = $("#forContract");

        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../contracts/registrationbd?status=" + $(".status").find(".active").find(".estEnabled").val(),
                    data: str,
                    dataType: "json",

                    beforeSend: function() {
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });
                        
                        if(respuesta.type != "error") {
                            $f.parsley().destroy();
                            $("#canContract").click();
                        }
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "left",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});
});