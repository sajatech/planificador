$(document).ready(function() {
    $("#parsley-id-multiple-frequency, #parsley-id-multiple-status, #parsley-id-multiple-docType").remove();

	// Actualización de contrato
	$("#updContract").on("click", function() {
        // Se guarda la referencia al formulario
        var $f = $("#forContract");
        var loading = $("#loaContract");

        $f.parsley().destroy();
        
        if($f.parsley().validate()) { 
            loading.show().removeClass("hide");
            var str = $f.serialize();

            // Bloqueo de peticiones repetidas
            if($f.data("locked") == undefined || !$f.data("locked")) {
                $.ajax({  
                    type: "POST",
                    url: "../contracts/updatebd?client=" + $(this).attr("data-client-id") + "&contract=" + $(this).attr("data-contract-id"),
                    data: str,
                    dataType: "json",

                    beforeSend: function() {
                        $f.data("locked", true);
                    },

                    success: function(respuesta) {
                        loading.hide();

                        notif({
                            msg: respuesta.text,
                            type: respuesta.type,
                            position: "left",
                            multiline: true
                        });                        
                    },

                    error: function () {
                        loading.hide();

                        notif({
                            msg: "Sucedió un error, contacte con el administrador del sistema.",
                            type: "error",
                            position: "left",
                            multiline: true
                        });
                    },

                    complete: function() { 
                        $f.data("locked", false);
                    }               
                });

                return false;
            }
        }
	});

    // Resetear formulario
    $("#canContract").on("click", function () {
        $("#forContract").each(function() {
            this.reset();
        });

        $(".icheck input[type=checkbox]").attr("checked", false).iCheck({
            radioClass: "iradio_minimal-red"
        });
        
        $("#group").val(null).trigger("change");
        $("#cliCode").focus();

        return false;
    });

    // Desactivación/Activación de contrato
    disContract = function(action, code) {
        if(code) {
            var disabled = action == "enauser" ? "enabled" : "disabled";
            
            $("#" + disabled).parent().addClass("disabled");

            $.ajax({  
                type: "POST",
                url: "../contracts/" + action,
                dataType: "json",

                data: { 
                    code: code
                },

                success: function(respuesta) {
                    $("#" + disabled).parent().removeClass("disabled");

                    notif({
                        msg: respuesta.text,
                        type: respuesta.type,
                        position: "left",
                        multiline: true
                    });
                },

                error: function () {
                    $("#" + disabled).parent().removeClass("disabled");

                    notif({
                        msg: "Sucedió un error, contacte con el administrador del sistema.",
                        type: "error",
                        position: "left",
                        multiline: true
                    });
                }
            });                
            
            return false;
        }
    }
});