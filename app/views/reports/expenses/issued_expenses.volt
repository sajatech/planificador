<style>
    body {
        background-color: #fff;
        font-family: OpenSans;
        font-size: 10pt;
    }
    .c052 {
        color: #05291D;
    }
    div {
        z-index: 1;
    }
    .ff-dsc {
        font-family: dejavusanscondensed;
    }
    .items td {
        border-bottom: 0.1mm solid #000000;
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        padding: 8px;
        font-size: 8pt;
    }
    .myfooter {
        border-top: 1px solid #000000; 
        font-size: 9pt; 
        padding-top: 3mm;
        text-align: center; 
    }
    .myheader td.elements {
        border: 0.1mm solid #888888 !important; 
        padding: 8px !important;
        background-color: #BBFEB9;
    }

    table.items {
        border-collapse: collapse;
        font-size: 9pt; 
        padding: 10px;
    }
    table thead td { 
        background-color: #BBFEB9;
        border-top: 0.1mm solid #000000;
        font-variant: small-caps;
        text-align: center;
    }
    td { 
        vertical-align: top;
    }
    p { 
        margin: 0pt; 
    }
    .pb15 {
        padding-bottom: 15px;
    }
    strong {
        font-weight: bold;
    }
    .text-center {
        text-align: center;
    }
    .title {
        font-size: 24px;
        text-align: center;
		margin-bottom: 15px;
	}
    .total {
        text-align: right;
		margin-top: 15px;
	}
</style>
<div>
    <htmlpageheader name="myheader">
        <table width="100%" class="myheader">
            <tr>
                <td width="60%" class="text-center elements c052" style="border-right: 0">

                    {{ image("img/logo.wmf", "height": "58px", "class": "pb15") }} <br/>

                    <span class="ff-dsc">&#9755; </span><strong>Website:</strong> www.ignaciogonzalezlandscaping.com <br/>
                    <span class="ff-dsc">&#10224; </span><strong>Address:</strong> 21790 NW 7th Court. Pembroke Pines. FL 33029 <br/>
                    <span class="ff-dsc">&#9993; </span><strong>Email:</strong> info@ignaciogonzalezlanscaping.com <br/>
                    <span class="ff-dsc">&#9990; </span><strong>Phone number:</strong> 786 346 78 66
                </td>
                <td width="40%" class="text-center elements" style="border-left: 0">
                    <div>Date: {{ date("l, m/d/Y") }}</div>
                    <div>
                        
                        {{ image("img/income.wmf", "height": "116px", "style": "margin-top: 6px") }}

                    </div>
                </td>
            </tr>
        </table>
    </htmlpageheader>
</div>
<div>
    <htmlpagefooter name="myfooter">
        <div class="myfooter">
            Page {PAGENO} of {nb} | IGNACIO GONZALEZ LANDSCAPING, INC.
        </div>
    </htmlpagefooter>
</div>
<sethtmlpageheader name="myheader" value="on" show-this-page="1"/>
<sethtmlpagefooter name="myfooter" value="on"/>
<div>
	<div class="title">
	    <strong>REPORT OF ISSUED EXPENSES</strong> <br/>
	</div>
    <table class="items" width="100%" cellpadding="8">
        <thead>
            <tr>
                <td><strong>DESCRIPTION</strong></td>
                <td><strong>EXPENSE DATE</strong></td>
                <td><strong>TOTAL</strong></td>
                <td><strong>EXPENDITURE ITEM</strong></td>
            </tr>
        </thead>
        <tbody>

        {% set total = 0 %}
        {% for expense in expenses %}

    		<tr {% if loop.index % 2 == 0 %} style="background-color: #DAFFD8" {% endif %}>
                <td> {{ expense.exp_description }} </td>
    			<td> {{ funciones.cambiaf_a_normal(expense.exp_date) }} </td>
    			<td> {{ "$ " ~ funciones.number_format(expense.total) }} </td>
    			<td> {{ expense.con_name }} </td>
    		</tr>

            {% set total = total + expense.total %}
        {% endfor %}

        </tbody>
    </table>
	<div class="total">
	    <h3 class="text-danger"><strong> {{ "Total: $ " ~ funciones.number_format(total) }} </strong></h3>
	</div>
</div>