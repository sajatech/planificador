<div class="row p-md">
	<div class="col-md-12">
		<table class="table table-striped table-bordered" cellspacing="0" widtd="100%" id="tabWeekly">
			<tr>
				<td><strong>Sunday</strong></td>
				<td> {{ weekly[6] }} </td>
			</tr>
			<tr>
				<td><strong>Monday</strong></td>
				<td> {{ weekly[0] }} </td>
			</tr>
			<tr>
				<td><strong>Tuesday</strong></td>
				<td> {{ weekly[1] }} </td>
			</tr>
			<tr>
				<td><strong>Wednesday</strong></td>
				<td> {{ weekly[2] }} </td>
			</tr>
			<tr>
				<td><strong>Thursday</strong></td>
				<td> {{ weekly[3] }} </td>
			</tr>
			<tr>
				<td><strong>Friday</strong></td>
				<td> {{ weekly[4] }} </td>
			</tr>
			<tr>
				<td><strong>Saturday</strong></td>
				<td> {{ weekly[5] }} </td>
			</tr>
		</table>
	</div>
</div>