{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
			<li>

				{{ link_to("desktop", "Calendar") }}

			</li>
			<li>

				{{ link_to("reports/weekly", "Reports") }}

			</li>
			<li class="active">

				{{ link_to("reports/weekly", "Weekly Report") }}

			</li>
        </ol>
        <div class="container-fluid">
			<div data-widget-group="group1">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2>Weekly Service Report<h2>
					</div>
					<div class="panel-body">

						{{ form("autocomplete": "off", "id": "forWeekly", "class": "form-horizontal row-border", "data-parsley-validate": "data-parsley-validate") }}

							<div class="form-group">
								<label for="weeYear" class="col-sm-2 control-label">Week of the Year</label>
								<div class="col-sm-8">
									<div class="input-group date" id="datepicker-startview">
										<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</span>

										{{ text_field("week", "class": "input-small form-control cur-pointer", "required": "required", "data-parsley-errors-messages-disabled": "data-parsley-errors-messages-disabled", "placeholder": "Enter Week of the Year") }}

									</div>
								</div>
							</div>

						{{ end_form() }}

						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									{{ submit_button("Consult", "class": "btn btn-primary btn", "id": "consult") }}

	        						{#{% if funciones.in_array(46, priRol) %}#}

										{{ link_to("#", "<i class='ti ti-download'></i> Generate PDF", "class": "btn btn-inverse disabled", "id": "genincomepdf", false) }}

									{#{% endif %}#}

									<span id="loaWeekly" class="hide">

										{{ image("img/ajax.svg") }}

									</span>

								</div>
							</div>
						</div>
					</div>
					<div class="panel-body p-n" id="result"></div>
				</div>
			</div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
		errorsWrapper: "<span class='help-block'></span>", 
		errorTemplate: "<span></span>"
    };
</script>