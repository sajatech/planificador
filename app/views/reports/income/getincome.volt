<div class="row p-md">
	<div class="col-md-12">
        <table class="table table-striped table-bordered nowrap" id="tabIncome" style="width: 100%">
			<thead>
				<tr>
					<th>Payment Date</th>
					<th>Amount</th>
					<th>Client</th>
					<th>Service</th>
					<th>Payment Type</th>
				</tr>
			</thead>
			<tbody>

				{% set total = 0 %}
				{% for inc in income %}

					<tr>
						<td> {{ funciones.cambiaf_a_normal(funciones.getdate(inc.pay_date)) }} </td>
						<td> {{ "$ " ~ funciones.number_format(inc.pay_amount) }} </td>
						<td> {{ inc.cli_name ~ " (" ~ inc.cli_code ~ ")" }} </td>
						<td> {{ inc.con_service }} </td>
						<td> {{ inc.typ_name }} </td>
					</tr>

					{% set total = total + inc.pay_amount %}
				{% endfor %}

			</tbody>
		</table>
		<span id="total" class="hide"> {{ funciones.number_format(total) }} </span>
	</div>
</div>