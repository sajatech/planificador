{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
			<li>

				{{ link_to("desktop", "Calendar") }}

			</li>
			<li>

				{{ link_to("reports/weekly", "Reports") }}

			</li>
			<li class="active">

				{{ link_to("reports/income", "Income Summary") }}

			</li>
        </ol>
        <div class="container-fluid">
			<div data-widget-group="group1">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2>Income Summary Report<h2>
					</div>
					<div class="panel-body">

						{{ form("autocomplete": "off", "id": "forIncome", "class": "form-horizontal row-border", "data-parsley-validate": "data-parsley-validate") }}

							<div class="form-group">
								<label for="datRange" class="col-sm-2 control-label">Date Range</label>
								<div class="col-sm-8">
									<div class="input-daterange input-group col-xs-12" id="datepicker-range">

										{{ text_field("since", "class": "input-small form-control cur-pointer", "required": "required", "data-parsley-errors-messages-disabled": "data-parsley-errors-messages-disabled", "placeholder": "Enter Start Date") }}

										<span class="input-group-addon">to</span>

										{{ text_field("until", "class": "input-small form-control cur-pointer", "required": "required", "data-parsley-errors-messages-disabled": "data-parsley-errors-messages-disabled", "placeholder": "Enter Ending Date") }}

									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="cliName" class="col-sm-2 control-label">Client Name</label>
								<div class="col-sm-8">
									<select name="client" id="client">
										<option value="">-- SELECT --</option>

										{% for client in clients %}

											<option value="{{ client.id }}">{{ client.cli_name ~ " (" ~ client.cli_code ~ ")" }}</option>

										{% endfor %}

									</select>
								</div>
							</div>							
							<div class="form-group">
								<label for="payType" class="col-sm-2 control-label">Payment Type</label>
								<div class="col-sm-8">
									<select name="payment" id="payment">
										<option value="">-- SELECT --</option>

										{% for type in types %}

											<option value="{{ type.id }}">{{ type.typ_name }}</option>

										{% endfor %}

									</select>
								</div>
							</div>

						{{ end_form() }}

						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									{{ submit_button("Consult", "class": "btn btn-primary btn", "id": "consult") }}

	        						{#{% if funciones.in_array(46, priRol) %}#}

										{{ link_to("#", "<i class='ti ti-download'></i> Generate PDF", "class": "btn btn-inverse disabled", "id": "genincomepdf", false) }}

									{#{% endif %}#}

									<span id="loaIncome" class="hide">

										{{ image("img/ajax.svg") }}

									</span>

								</div>
							</div>
						</div>
					</div>
					<div class="panel-body p-n" id="result"></div>
				</div>
			</div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
		errorsWrapper: "<span class='help-block'></span>", 
		errorTemplate: "<span></span>"
    };
</script>