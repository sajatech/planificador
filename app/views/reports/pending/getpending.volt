<div class="row p-md">
	<div class="col-md-12">
        <table class="table table-striped table-bordered nowrap" id="tabPending" style="width: 100%">
			<thead>
				<tr>
					<th>Client</th>
					<th>Service</th>
					<th>Mobile Phone</th>
					<th>Last Payment</th>
					<th>Balance</th>
				</tr>
			</thead>
			<tbody>

				{% set total = 0 %}
				{% for pen in pending %}
					{% if arrBalance[loop.index - 1] > 0 %}

					<tr>
						<td> {{ pen["cli_name"] ~ " (" ~ pen["cli_code"] ~ ")" }} </td>
						<td> {{ pen["con_service"] }} </td>
						<td> {{ pen["cli_mob_phone"] }} </td>
						<td> {{ funciones.cambiaf_a_normal(funciones.getdate(pen["pay_date"])) }} </td>
						<td> {{ "$ " ~ funciones.number_format(arrBalance[loop.index - 1]) }} </td>
					</tr>

					{% endif %}

					{% set total = total + arrBalance[loop.index - 1] %}
				{% endfor %}

			</tbody>
		</table>
		<span id="total" class="hide"> {{ funciones.number_format(total) }} </span>
	</div>
</div>