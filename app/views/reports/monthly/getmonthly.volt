<div class="row p-md">
	<div class="col-md-12">
		<div class="table-responsive">		
			<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="tabMonthly">
				<thead>
					<tr>
						<th width="14.29%">Sunday</th>
						<th width="14.29%">Monday</th>
						<th width="14.29%">Tuesday</th>
						<th width="14.29%">Wednesday</th>
						<th width="14.29%">Thursday</th>
						<th width="14.29%">Friday</th>
						<th width="14.29%">Saturday</th>
					</tr>
				</thead>
				<tbody>

					{{ weekly }}

				</tbody>
			</table>
		</div>
	</div>
</div>