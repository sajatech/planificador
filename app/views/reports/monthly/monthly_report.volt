<style>
    body {
        background-color: #fff;
        font-family: OpenSans;
        font-size: 10pt;
    }
    .c052 {
        color: #05291D;
    }
    div {
        z-index: 1;
    }
    .ff-dsc {
        font-family: dejavusanscondensed;
    }
    .fs8 {
        font-size: 8pt;
    }
    .fs14 {
        font-size: 14pt;
    }
    .fs16 {
        font-size: 16pt;
    }
    .invoice {
        font-family: serif; 
        margin-bottom: 3px;
        margin-top: 3px; 
    }
    .invoice td {
        border: 0.1mm solid #888888; 
        border-bottom: 0;
        padding: 5px;
    }
    .items td {
        border-bottom: 0.1mm solid #000000;
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        padding: 8px;
        font-size: 8pt;
    }
    .items td.services {
        font-size: 7pt;
    }
    .items td.blanktotal {
        text-align: center;
        font-style: italic;
        vertical-align: middle;   
    }
    .items td.cost {
        text-align: "." center;
    }
    .items td.totals {
        text-align: right;
    }
    .myfooter {
        border-top: 1px solid #000000; 
        font-size: 9pt; 
        padding-top: 3mm;
        text-align: center; 
    }
    .myheader td.elements {
        border: 0.1mm solid #888888 !important; 
        padding: 8px !important;
        background-color: #BBFEB9;
    }

    table.items {
        border-collapse: collapse;
        font-size: 9pt; 
        padding: 10px;
    }
    table thead td { 
        background-color: #BBFEB9;
        border-top: 0.1mm solid #000000;
        font-variant: small-caps;
        text-align: center;
    }
    td { 
        vertical-align: top;
    }
    td div {
        text-align: right !important;
    }
    p { 
        margin: 0pt; 
    }
    .pb15 {
        padding-bottom: 15px;
    }
    .status {
        color: #fff; 
        font-size: 16pt; 
        font-weight: bold; 
        padding: 3px;
    }
    strong {
        font-weight: bold;
    }
    .text-center {
        text-align: center;
    }
    .title {
        font-size: 24px;
        text-align: center;
		margin-bottom: 15px;
	}
    .total {
        text-align: right;
		margin-top: 15px;
	}
</style>
<div>
    <htmlpageheader name="myheader">
        <table width="100%" class="myheader">
            <tr>
                <td width="60%" class="text-center elements c052" style="border-right: 0">

                    {{ image("img/logo.wmf", "height": "58px", "class": "pb15") }} <br/>

                    <span class="ff-dsc">&#9755; </span><strong>Website:</strong> www.ignaciogonzalezlandscaping.com <br/>
                    <span class="ff-dsc">&#10224; </span><strong>Address:</strong> 21790 NW 7th Court. Pembroke Pines. FL 33029 <br/>
                    <span class="ff-dsc">&#9993; </span><strong>Email:</strong> info@ignaciogonzalezlanscaping.com <br/>
                    <span class="ff-dsc">&#9990; </span><strong>Phone number:</strong> 786 346 78 66
                </td>
                <td width="40%" class="text-center elements" style="border-left: 0">
                    <div>Date: {{ date("l, m/d/Y") }}</div>
                    <div>
                        
                        {{ image("img/income.wmf", "height": "116px", "style": "margin-top: 6px") }}

                    </div>
                </td>
            </tr>
        </table>
    </htmlpageheader>
</div>
<div>
    <htmlpagefooter name="myfooter">
        <div class="myfooter">
            Page {PAGENO} of {nb} | IGNACIO GONZALEZ LANDSCAPING, INC.
        </div>
    </htmlpagefooter>
</div>
<sethtmlpageheader name="myheader" value="on" show-this-page="1"/>
<sethtmlpagefooter name="myfooter" value="on"/>
<div>
	<div class="title">
	    <strong>MONTHLY REPORT: {{ month ~ "/" ~ year }}</strong> <br/>
	</div>
    <table class="items" width="100%" cellpadding="8">
        <thead>
            <tr>
                <td width="14.29%"><strong>SUNDAY</strong></td>
                <td width="14.29%"><strong>MONDAY</strong></td>
                <td width="14.29%"><strong>TUESDAY</strong></td>
                <td width="14.29%"><strong>WEDNESDAY</strong></td>
                <td width="14.29%"><strong>THURSDAY</strong></td>
                <td width="14.29%"><strong>FRIDAY</strong></td>
                <td width="14.29%"><strong>SATURDAY</strong></td>
            </tr>
        </thead>
        <tbody>

            {{ monthly }}

        </tbody>
    </table>
</div>