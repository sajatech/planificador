<!DOCTYPE html>
<html lang="en" class="coming-soon">
    <head>
        <meta charset="utf-8">
        <title>Planner of Services</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="">
        <meta name="author" content="Saja Tech">

    	{{ stylesheet_link("assets/fonts/font-awesome/css/font-awesome.min.css") }}
        {{ stylesheet_link("assets/css/styles.css") }}
        {{ stylesheet_link("assets/css/custom.css") }}
        {{ stylesheet_link("assets/plugins/notifIt/css/notifIt.css") }}

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
        <!--[if lt IE 9]>
            <link type="text/css" href="assets/css/ie8.css" rel="stylesheet">
            <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The following CSS are included as plugins and can be removed if unused-->
        
    </head>
    <body class="focused-form {{ router.getRewriteUri() == "/" or router.getRewriteUri() == "/login" or router.getRewriteUri() == "/password" ? "background" : "" }}">
        <div id="preloader">
            <div id="page-loader"></div>
        </div>

    	{{ content() }}

        <script type="text/javascript">
            $(window).load(function(e) {
                $("#preloader").fadeOut("slow");
                $("body").css({ "overflow": "visible" });
            });
        </script>
    </body>
</html>