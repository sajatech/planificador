{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
            <li>

                {{ link_to("desktop", "Calendar") }}

            </li>
            <li>

                {{ link_to("invoicing/generation", "Invoicing") }}

            </li>
            <li class="active">

                {{ link_to("invoicing/generation", "Generation of Documents") }}

            </li>
        </ol>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                            {{ image("img/ajax.svg") }}
                            {{ "Por favor, espere..." }}

                        </div>
                        <div class="panel panel-default hide" id="panServices">
                            <div class="panel-heading">
                                <h2 class="title c-w fz-24">Creation of Documents for Clients</h2>
                                <div class="panel-ctrls"></div>
                            </div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive mb0">                                
                                    <table class="table table-striped table-bordered nowrap tcur-pointer" id="services" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Contract Code</th>
                                                <th>Client</th>
                                                <th>Contract Date</th>
                                                <th>Amount</th>
                                                <th>Paid</th>
                                                <th>Balance</th>
                                                <th>Generated</th>
                                                <th>For Invoicing</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>                
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{# Modal de servicios por facturar #}
<div class="modal fade" id="modInvoicing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Preview of the payment document ítems</h2>
            </div>
            <div class="modal-body">

                {{ form("autocomplete": "off", "id": "forInvoicing", "data-parsley-validate": "data-parsley-validate") }}

                    <div id="result"></div>

                {{ end_form() }}

            </div>
            <div class="modal-footer">
                <span id="loaInvoicing" class="hide loading">

                    {{ image("img/ajax.svg") }}

                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
                <button type="button" class="btn btn-primary" id="generate">Generate</button>
            </div>
        </div>
    </div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsWrapper: "<span class='help-block'></span>", 
        errorTemplate: "<span></span>"
    };
</script>