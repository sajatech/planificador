<style>
    body {
        background-color: #fff;
        font-family: OpenSans;
        font-size: 10pt;
    }
    .c052 {
        color: #05291D;
    }
    div {
        z-index: 1;
    }
    .ff-dsc {
        font-family: dejavusanscondensed;
    }
    .fs14 {
        font-size: 14pt;
    }
    .fs16 {
        font-size: 16pt;
    }
    .receipt {
        font-family: serif; 
        margin-top: 12px; 
    }
    .receipt td {
        border: 0.1mm solid #888888; 
        border-bottom: 0;
        padding: 5px;
    }
    .items td {
        border-bottom: 0.1mm solid #000000;
        border-left: 0.1mm solid #000000;
        border-right: 0.1mm solid #000000;
        padding: 10px;
    }
    .items td.blanktotal {
        text-align: center;
        font-style: italic;
        vertical-align: middle;   
    }
    .items td.cost {
        text-align: "." center;
    }
    .items td.totals {
        text-align: right;
    }
    .myfooter {
        border-top: 1px solid #000000; 
        font-size: 9pt; 
        padding-top: 3mm;
        text-align: center; 
    }
    .myheader td.elements {
        border: 0.1mm solid #888888 !important; 
        padding: 8px !important;
        background-color: #BBFEB9;
    }

    table.items {
        border-collapse: collapse;
        font-size: 9pt; 
        padding: 10px;
    }
    table thead td { 
        background-color: #BBFEB9;
        border-top: 0.1mm solid #000000;
        font-variant: small-caps;
        text-align: center;
    }
    td { 
        vertical-align: top;
    }
    p { 
        margin: 0pt; 
    }
    .pb15 {
        padding-bottom: 15px;
    }
    .status {
        color: #fff; 
        font-size: 16pt; 
        font-weight: bold; 
        padding: 3px;
    }
    strong {
        font-weight: bold;
    }
    .text-center {
        text-align: center;
    }
</style>

{% if docStatus == "DISABLED" %}
    {% set docStatus = docStatus %}
    {% set bacColor = "red" %}
{% else %}
    {% if paid == "true" %}
        {% set docStatus = "PAID" %}
        {% set bacColor = "green" %}
    {% else %}
        {% set docStatus = "PENDING PAY" %}
        {% set bacColor = "red" %}
    {% endif %}
{% endif %}

<div>
    <htmlpageheader name="myheader">
        <table width="100%" class="myheader">
            <tr>
                <td width="60%" class="text-center elements c052" style="border-right: 0">

                    {{ image("img/logo.wmf", "height": "58px", "class": "pb15") }} <br/>

                    <span class="ff-dsc">&#9755; </span><strong>Website:</strong> www.ignaciogonzalezlandscaping.com <br/>
                    <span class="ff-dsc">&#10224; </span><strong>Address:</strong> 21790 NW 7th Court. Pembroke Pines. FL 33029 <br/>
                    <span class="ff-dsc">&#9993; </span><strong>Email:</strong> info@ignaciogonzalezlanscaping.com <br/>
                    <span class="ff-dsc">&#9990; </span><strong>Phone number:</strong> 786 346 78 66
                </td>
                <td width="40%" class="text-center elements" style="border-left: 0">
                    <div>Date: {{ date("l, m/d/Y") }}</div>
                    <div>
                        <table class="receipt" width="100%">
                            <tr>
                                <td width="45%">
                                    <span class="fs14">Receipt No.</span> <br/>
                                    <span class="fs16"><strong> {{ document }} </strong></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="status" style="border-top: 0; border-bottom: 0.1mm solid #888888; background-color: {{ bacColor }}"> {{ docStatus }} </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </htmlpageheader>
</div>
<div>
    <htmlpagefooter name="myfooter">
        <div class="myfooter">
            Page {PAGENO} of {nb} | IGNACIO GONZALEZ.
        </div>
    </htmlpagefooter>
</div>
<sethtmlpageheader name="myheader" value="on" show-this-page="1"/>
<sethtmlpagefooter name="myfooter" value="on"/> <br/>
<div>
    <table class="items" width="100%" cellpadding="8">
        <tr>
            <td style="border: 0.1mm solid #888888"><strong><span style="color: #555555">Name:</span></strong> {{ client.cli_name }} </td>
            <td style="border: 0.1mm solid #888888"><strong><span style="color: #555555">Phone Number:</span></strong> {{ client.cli_mob_phone }} </td>
        </tr>
        <tr>
            <td style="border: 0.1mm solid #888888"><strong><span style="color: #555555">Address:</span></strong> {{ client.cli_address }} </td>
            <td style="border: 0.1mm solid #888888"><strong><span style="color: #555555">Email:</span></strong> {{ client.cli_email }}</td>
        </tr>
    </table> <br/>
    <table class="items" width="100%" cellpadding="8">
        <thead>
            <tr>
                <td><strong>REF. NO.</strong></td>
                <td><strong>SERVICE DATE</strong></td>
                <td><strong>DESCRIPTION</strong></td>
                <td><strong>AMOUNT</strong></td>
            </tr>
        </thead>
        <tbody>

            {% set subtotal = 0 %}
            {% for service in services %}

                <tr>
                    <td align="center"> {{ "CS" ~ funciones.str_pad(service.id) }} </td>
                    <td align="center"> {{ service.con_day ~ ", " ~ funciones.cambiaf_a_normal(service.ser_date) }} </td>
                    <td> {{ contract.con_service }} </td>
                    <td class="cost"> {{ "$ " ~ funciones.number_format(contract.con_pay) }} </td>
                </tr>

                {% set subtotal = subtotal + contract.con_pay %}
            {% endfor %}

            {% for add in additional %}

                <tr>
                    <td align="center"> {{ "AS" ~ funciones.str_pad(add.id) }} </td>
                    <td align="center"> {{ funciones.cambiaf_a_normal(add.ser_date) }} </td>
                    <td> {{ add.add_name }} </td>
                    <td class="cost"> {{ "$ " ~ funciones.number_format(add.add_amount) }} </td>
                </tr>

                {% set subtotal = subtotal + add.add_amount %}
            {% endfor %}

            {% set amoDue = subtotal - payments %}

            <tr>
                <td class="blanktotal" colspan="2" rowspan="3">Payment Type(s): {{ payTypes|length == 0 ? "NONE" : payTypes }}</td>
                <td class="totals"><strong>TOTAL:</strong></td>
                <td class="totals cost"><strong> {{ "$ " ~ funciones.number_format(subtotal) }} </strong></td>
            </tr>
            <tr>
                <td class="totals">Paid:</td>
                <td class="totals cost"> {{ "$ " ~ funciones.number_format(payments) }} </td>
            </tr>
            <tr>
                <td class="totals"><strong>Amount due:</strong></td>
                <td class="totals cost"><strong>$ {{ funciones.number_format(amoDue) == "-0.00" ? "0.00" : funciones.number_format(amoDue) }}</strong></td>
            </tr>
        </tbody>
    </table>
</div>