{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
            <li>

                {{ link_to("desktop", "Calendar") }}

            </li>
            <li>

                {{ link_to("invoicing/generation", "Invoicing") }}

            </li>
            <li class="active">

                {{ link_to("invoicing/issued", "Issued Documents") }}

            </li>
        </ol>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                            {{ image("img/ajax.svg") }}
                            {{ "Por favor, espere..." }}

                        </div>
                        <div class="panel panel-default hide" id="panIssued">
                            <div class="panel-heading">
                                <h2>Electronic Documents Issued</h2>
                                <div class="panel-ctrls center-block"></div>
                            </div>
                            <div class="panel-body no-padding">
                            <div class="table-responsive mb0">                                
                                <table class="table table-striped table-bordered nowrap tcur-pointer" id="issued" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Contract Code</th>
                                            <th>Document Code</th>
                                            <th>Client</th>
                                            <th>Document Type</th>
                                            <th>Document Number</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            </div>                
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="preloader-pdf" class="hide">
    <div id="page-loader-pdf"></div>
</div>

{{ assets.outputJs() }}