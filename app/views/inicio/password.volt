<div class="container" id="login-form">
	
	{{ link_to("desktop", "", "class": "login-logo") }}

	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Login Form</h2>
				</div>
				<div class="panel-body">
					
					{{ form("class": "form-horizontal", "id": "forPassword", "autocomplete": "off", "data-parsley-validate": "data-parsley-validate") }}

						<div class="form-group">
	                        <div class="col-xs-12 closest">
	                        	<p>Enter your email to reset your password</p>
	                        	<div class="input-group">							
									<span class="input-group-addon">
										<i class="fa fa-envelope"></i>
									</span>

									{{ email_field("emaAddress", "class": "form-control", "placeholder": "Enter Email Address", "required": "required") }}

								</div>
	                        </div>
						</div>
						<div class="panel-footer">
							<div class="clearfix">

								{{ link_to("login", "Go Back", "class": "btn btn-default pull-left") }}
								{{ submit_button("Reset", "class": "btn btn-primary pull-right", "id": "reset") }}

								<span id="loaPassword" class="hide pull-right">

									{{ image("img/ajax.svg") }}

								</span>
							</div>
						</div>

					{{ end_form() }}

				</div>
			</div>
		</div>
	</div>
</div>

{{ assets.outputJs() }}

<script>
	window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

	  	errorsWrapper: "<span class='help-block'></span>",
	  	errorTemplate: "<span></span>"
	};
</script>