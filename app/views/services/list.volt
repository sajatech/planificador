{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
            <li>

                {{ link_to("desktop", "Desktop") }}

            </li>
            <li>

                {{ link_to("invoicing/generation", "Invoicing") }}

            </li>
            <li class="active">

                {{ link_to("services/list", "Checked Services") }}

            </li>
        </ol>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                            {{ image("img/ajax.svg") }}
                            {{ "Por favor, espere..." }}

                        </div>
                        <div class="panel panel-default hide" id="panServices">
                            <div class="panel-heading">
                                <h2 class="title c-w fz-24">Checked Services</h2>
                                <div class="panel-ctrls"></div>
                            </div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive mb0">                                
                                    <table class="table table-striped table-bordered nowrap tcur-pointer" id="services" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Client Code</th>
                                                <th>Contract Code</th>
                                                <th>End Format</th>
                                                <th>Service Code</th>
                                                <th>Document Type</th>
                                                <th>Client</th>
                                                <th>Service Date</th>
                                                <th>Service Name</th>
                                                <th>Amount</th>
                                                <th>Paid</th>
                                                <th>Balance</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>                
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ partial("partials/services") }}
{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsWrapper: "<span class='help-block'></span>", 
        errorTemplate: "<span></span>"
    };
</script>