{{ assets.outputCss() }}

<table class="table table-striped table-bordered nowrap">
    <thead>
        <tr>
            <th>Service Date</th>
            <th>Amount</th>
            <th class="text-center">Select</th>
        </tr>
    </thead>
    <tbody>

        {% for inv in arrInvoicing %}

            <tr>
                <td> {{ inv["invoicing"].con_day ~ ", " ~ funciones.cambiaf_a_normal(inv["invoicing"].ser_date) }} </td>
                <td> {{ "$ " ~ funciones.number_format(inv["invoicing"].con_pay + inv["additional"]) }} </td>
                <td class="select"> {{ check_field("select[]", "class": "icheck", "value": inv["invoicing"].id, "checked": "checked") }} </td>
            </tr>

        {% endfor %}

    </tbody>
</table>

{{ assets.outputJs() }}