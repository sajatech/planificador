<?php
if(!$this->session->get("nivel1")["user"])
	return $this->response->redirect("login");
?>

<header id="topnav" class="navbar navbar-midnightblue {{ router.getRewriteUri() != "/contracts/list" and router.getRewriteUri() != "/services/list" and router.getRewriteUri() != "/configuration/groups" and router.getRewriteUri() != "/users/list" and router.getRewriteUri() != "/reports/income" and router.getRewriteUri() != "/reports/pending" ? "navbar-fixed-top clearfix" : "" }}" role="banner">
	<span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
		<a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
			<span class="icon-bg">
				<i class="fa fa-fw fa-bars"></i>
			</span>
		</a>
	</span>
	<a class="navbar-brand" href="index.html">Planificador de Actividades</a>
	<span id="trigger-infobar" class="toolbar-trigger toolbar-icon-bg">
		
		{{ link_to(funciones.gethostname() ~ "login", "<span title='Logout' class='icon-bg'><i class='fa fa-lock'></i></span>", false) }}
		
	</span>
	<ul class="nav navbar-nav toolbar pull-right">
		<li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
			<a href="#" class="toggle-fullscreen">
				<span class="icon-bg">
					<i class="fa fa-fw fa-arrows-alt"></i>
				</span></i></a>
		</li>
		{#<li class="dropdown toolbar-icon-bg">
			<a href="#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'>
				<span class="icon-bg">
					<i class="fa fa-fw fa-bell"></i>
				</span>
				<span class="badge badge-info">5</span>
			</a>
			<div class="dropdown-menu dropdown-alternate notifications arrow">
				<div class="dd-header">
					<span>Notifications</span>
					<span>
						<a href="#">Settings</a>
					</span>
				</div>
				<div class="scrollthis scroll-pane">
					<ul class="scroll-content">
						<li class="">
							<a href="#" class="notification-info">
								<div class="notification-icon"><i class="fa fa-user fa-fw"></i></div>
								<div class="notification-content">Profile Page has been updated</div>
								<div class="notification-time">2m</div>
							</a>
						</li>
						<li class="">
							<a href="#" class="notification-success">
								<div class="notification-icon"><i class="fa fa-check fa-fw"></i></div>
								<div class="notification-content">Updates pushed successfully</div>
								<div class="notification-time">12m</div>
							</a>
						</li>
						<li class="">
							<a href="#" class="notification-primary">
								<div class="notification-icon"><i class="fa fa-users fa-fw"></i></div>
								<div class="notification-content">New users request to join</div>
								<div class="notification-time">35m</div>
							</a>
						</li>
						<li class="">
							<a href="#" class="notification-danger">
								<div class="notification-icon"><i class="fa fa-shopping-cart fa-fw"></i></div>
								<div class="notification-content">More orders are pending</div>
								<div class="notification-time">11h</div>
							</a>
						</li>
						<li class="">
							<a href="#" class="notification-primary">
								<div class="notification-icon"><i class="fa fa-arrow-up fa-fw"></i></div>
								<div class="notification-content">Pending Membership approval</div>
								<div class="notification-time">2d</div>
							</a>
						</li>
						<li class="">
							<a href="#" class="notification-info">
								<div class="notification-icon"><i class="fa fa-check fa-fw"></i></div>
								<div class="notification-content">Succesfully updated to version 1.0.1</div>
								<div class="notification-time">40m</div>
							</a>
						</li>
					</ul>
				</div>
				<div class="dd-footer">
					<a href="#">View all notifications</a>
				</div>
			</div>
		</li>#}
	</ul>
</header>
<div id="wrapper">
	<div id="layout-static">
		<div class="static-sidebar-wrapper sidebar-midnightblue">
			<div class="static-sidebar">
				<div class="sidebar">
					<div class="widget stay-on-collapse" id="widget-welcomebox">
						<div class="widget-body welcome-box tabular">
							<div class="tabular-row">
								<div class="tabular-cell welcome-avatar">
									<a href="#">
										<img src="http://placehold.it/300&text=Placeholder" class="avatar">
									</a>
								</div>
								<div class="tabular-cell welcome-options">
									<span class="welcome-text">Welcome,</span>
									<a href="#" class="name">Ignacio Gonzalez</a>
								</div>
							</div>
						</div>
					</div>
					<div class="widget stay-on-collapse" id="widget-sidebar">
						<nav role="navigation" class="widget-body">
							<ul class="acc-menu">
								<li class="nav-separator">Menu</li>
								<li {% if router.getRewriteUri() == "/desktop" %} class="active" {% endif %}>

									{{ link_to("desktop", "<i class='fa fa-calendar'></i><span>Calendar</span>") }}

								</li>
								<li {% if router.getRewriteUri() == "/contracts/registration" or router.getRewriteUri() == "/contracts/list" or router.getRewriteUri() == "/contracts/update" %} class="open active hasChild" {% endif %}>
									<a href="javascript:;">
										<i class="fa fa-briefcase"></i>
										<span>Clients</span>
									</a>
									<ul class="acc-menu" {% if router.getRewriteUri() == "/contracts/registration" or router.getRewriteUri() == "/contracts/list" or router.getRewriteUri() == "/contracts/update" %} style="display: block" {% endif %}>
										<li {% if router.getRewriteUri() == "/contracts/registration" %} class="active" {% endif %}>

											{{ link_to("contracts/registration", "New Client") }}

										</li>
										<li {% if router.getRewriteUri() == "/contracts/list" %} class="active" {% endif %}>

											{{ link_to("contracts/list", "Find Clients") }}

										</li>
									</ul>
								</li>
								<li {% if router.getRewriteUri() == "/services/list" or router.getRewriteUri() == "/invoicing/generation" or router.getRewriteUri() == "/invoicing/issued" %} class="open active hasChild" {% endif %}>
									<a href="javascript:;">
										<i class="fa fa-money"></i>
										<span>Invoicing</span>
									</a>
									<ul class="acc-menu" {% if router.getRewriteUri() == "/services/list" or router.getRewriteUri() == "/invoicing/generation" or router.getRewriteUri() == "/invoicing/issued" %} style="display: block" {% endif %}>
										<li {% if router.getRewriteUri() == "/services/list" %} class="active" {% endif %}>

											{{ link_to("services/list", "List of Checked Services") }}

										</li>
										<li {% if router.getRewriteUri() == "/invoicing/generation" %} class="active" {% endif %}>

											{{ link_to("invoicing/generation", "Generation of Documents") }}

										</li>
										<li {% if router.getRewriteUri() == "/invoicing/issued" %} class="active" {% endif %}>

											{{ link_to("invoicing/issued", "Issued Documents") }}

										</li>
									</ul>
								</li>
								<li {% if router.getRewriteUri() == "/expenses/registration" or router.getRewriteUri() == "/expenses/list" or router.getRewriteUri() == "/expenses/update" %} class="open active hasChild" {% endif %}>
									<a href="javascript:;">
										<i class="fa fa-caret-square-o-left"></i>
										<span>Expenses</span>
									</a>
									<ul class="acc-menu" {% if router.getRewriteUri() == "/expenses/registration" or router.getRewriteUri() == "/expenses/list" or router.getRewriteUri() == "/expenses/update" %} style="display: block" {% endif %}>
										<li {% if router.getRewriteUri() == "/expenses/registration" %} class="active" {% endif %}>

											{{ link_to("expenses/registration", "New Expense") }}

										</li>
										<li {% if router.getRewriteUri() == "/expenses/list" %} class="active" {% endif %}>

											{{ link_to("expenses/list", "Find Expenses") }}

										</li>
									</ul>
								</li>
								<li {% if router.getRewriteUri() == "/reports/weekly" or router.getRewriteUri() == "/reports/income" or router.getRewriteUri() == "/reports/pending" or router.getRewriteUri() == "/reports/expenses" %} class="open active hasChild" {% endif %}>
									<a href="javascript:;">
										<i class="fa fa-files-o"></i>
										<span>Reports</span>
									</a>
									<ul class="acc-menu" {% if router.getRewriteUri() == "/reports/weekly" or router.getRewriteUri() == "/reports/monthly" or router.getRewriteUri() == "/reports/income" or router.getRewriteUri() == "/reports/pending" or router.getRewriteUri() == "/reports/expenses" %} style="display: block" {% endif %}>
										<li {% if router.getRewriteUri() == "/reports/weekly" %} class="active" {% endif %}>

											{{ link_to("reports/weekly", "Weekly Report") }}

										</li>
										<li {% if router.getRewriteUri() == "/reports/monthly" %} class="active" {% endif %}>

											{{ link_to("reports/monthly", "Monthly Report") }}

										</li>
										<li {% if router.getRewriteUri() == "/reports/income" %} class="active" {% endif %}>

											{{ link_to("reports/income", "Income Summary") }}

										</li>
										<li {% if router.getRewriteUri() == "/reports/pending" %} class="active" {% endif %}>

											{{ link_to("reports/pending", "Pending Payments") }}

										</li>
										<li {% if router.getRewriteUri() == "/reports/expenses" %} class="active" {% endif %}>

											{{ link_to("reports/expenses", "Expenses Report") }}

										</li>
									</ul>
								</li>
								<li {% if router.getRewriteUri() == "/configuration/groups" or router.getRewriteUri() == "/configuration/expenses" %} class="open active hasChild" {% endif %}>
									<a href="javascript:;">
										<i class="fa fa-cogs"></i>
										<span>Configuration</span>
									</a>
									<ul class="acc-menu" {% if router.getRewriteUri() == "/configuration/groups" or router.getRewriteUri() == "/configuration/expenses" %} style="display: block" {% endif %}>
										<li {% if router.getRewriteUri() == "/configuration/groups" %} class="active" {% endif %}>

											{{ link_to("configuration/groups", "Work Groups") }}
					
										</li>										
										<li {% if router.getRewriteUri() == "/configuration/expenses" %} class="active" {% endif %}>

											{{ link_to("configuration/expenses", "Expenditure Items") }}
					
										</li>
									</ul>
								</li>
								<li {% if router.getRewriteUri() == "/users/registration" or router.getRewriteUri() == "/users/list" or router.getRewriteUri() == "/users/update" %} class="open active hasChild" {% endif %}>
									<a href="javascript:;">
										<i class="fa fa-user"></i>
										<span>Users</span>
									</a>
									<ul class="acc-menu" {% if router.getRewriteUri() == "/users/registration" or router.getRewriteUri() == "/users/list" or router.getRewriteUri() == "/users/update" %} style="display: block" {% endif %}>
										<li {% if router.getRewriteUri() == "/users/registration" %} class="active" {% endif %}>

											{{ link_to("users/registration", "New User") }}

										</li>
										<li {% if router.getRewriteUri() == "/users/list" %} class="active" {% endif %}>

											{{ link_to("users/list", "Find Users") }}

										</li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="static-content-wrapper">

			{{ content() }}
	
			<footer role="contentinfo">
				<div class="clearfix">
					<ul class="list-unstyled list-inline pull-left">
						<li>
							<h6 style="margin: 0;"> &copy; 2018 ignaciogonzalezlandscaping.com</h6>
						</li>
					</ul>
					<button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top">
						<i class="fa fa-arrow-up"></i>
					</button>
				</div>
			</footer>
		</div>
	</div>
</div>