<div class="container" id="login-form">
	<a href="" class="login-logo">{{ image("img/title.png") }}</a>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Login Form</h2>
				</div>
				<div class="panel-body">
					
					{{ form("class": "form-horizontal", "id": "forLogin", "autocomplete": "off", "data-parsley-validate": "data-parsley-validate") }}

						<div class="form-group">
	                        <div class="col-xs-12 closest">
	                        	<div class="input-group">							
									<span class="input-group-addon">
										<i class="fa fa-user"></i>
									</span>
									
									{{ text_field("username", "class": "form-control", "placeholder": "Username", "required": "required") }}

								</div>
	                        </div>
						</div>
						<div class="form-group">
	                        <div class="col-xs-12 closest">
	                        	<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-key"></i>
									</span>
									
									{{ password_field("password", "class": "form-control", "placeholder": "Password", "required": "required") }}

								</div>
	                        </div>
						</div>
						<div class="panel-footer">
							<div class="clearfix">

								{{ link_to("password", "Forgot password?", "class": "pull-left") }}
								{{ submit_button("Login", "class": "btn btn-primary pull-right", "id": "login") }}

								<span id="loaLogin" class="hide pull-right">

									{{ image("img/ajax.svg") }}

								</span>
							</div>
						</div>

					{{ end_form() }}

				</div>
			</div>
		</div>
	</div>
</div>

{{ assets.outputJs() }}

<script>
	window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

	  	errorsWrapper: "<span class='help-block'></span>",
	  	errorTemplate: "<span></span>"
	};
</script>