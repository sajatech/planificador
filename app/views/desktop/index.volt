{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
		<div class="page-heading text-center">
            <div class="row">
				<div class="col-md-6 col-xs-12 col-sm-8">
            		<h1 class="center-block">Monthly Service Planner</h1>
            	</div>
            	<div class="col-md-6 col-xs-12 col-sm-4">
					<div class="options center-block">
						<div class="btn-toolbar">
							
							{{ link_to("contracts/registration", "<i class='fa fa-plus'></i><span> New Client</span>", "class": "btn btn-primary") }}

						</div>
					</div>
            	</div>
            </div>
        </div>        
        <div class="container-fluid">
			<div data-widget-group="group1" class="pl-lg pr-lg">
				<div class="row">
					<div class="col-md-12 text-center">
						<div id="calendar"></div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

{# Modal de lista de servicios del día #}
<div class="modal fade" id="modServices" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Services of the day: <span id='serDate'></span></h2>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 pl-lg" id="services-content">
                        <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                            {{ image("img/ajax.svg") }}
                            {{ "Por favor, espere..." }}

                        </div>
                        <table class="table table-striped table-bordered nowrap" id="services" style="width:100%">
		                	<thead>
		                		<tr>
                                    <th>#</th>
                                    <th>Contract Date</th>
                                    <th>Contract Code</th>
                                    <th>Group Code</th>
                                    <th>Client Code</th>
                                    <th>Client</th>
                                    <th>Service</th>
                                    <th>Status</th>
                                    <th>Group</th>
		                			<th>Actions</th>
                                    <th>Position</th>
		                		</tr>
		                	</thead>
		                </table>
                    </div>
                </div>                    
            </div>
            <div class="modal-footer">
                <span id="loaService" class="hide">

                    {{ image("img/ajax.svg") }}

                </span>
                <button type="button" class="btn btn-inverse" id="reoList">Reorder List</button>
                <button type="button" class="btn btn-inverse hide" id="regList">Save List</button>
                {#<button type="button" class="btn btn-inverse hide" id="return">Return</button>#}
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
            </div>
        </div>
    </div>
</div>

{# ******************************** #}

{{ partial("partials/services") }}
{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>