{# Modal de pagos de contrato a procesar #}
<div class="modal fade" id="modAdditional" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Service payment: <span id="serPayment"></span></h2>
            </div>
            <div class="modal-body">
				<div class="tab-container tab-default">
					<ul class="nav nav-tabs">
						<li class="active">

							{{ link_to("#contract", "<i class='fa fa-file-o mr-sm col-009'></i> Contract", "id": "linContract", "data-toggle": "tab") }}

						</li>
						<li>
							
							{{ link_to("#additional", "<i class='fa fa-plus mr-sm col-e91'></i> Additional", "id": "linAdditional", "data-toggle": "tab") }}

						</li>
						<li>
							
							{{ link_to("#payments", "<i class='fa fa-dollar mr-sm col-8bc'></i> Payments", "id": "linPayments", "data-toggle": "tab") }}

						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="contract">
			                <table class="table table-striped mb0">
			                	<thead>
			                		<tr>
			                			<td colspan="2" class="text-center">
				                			<div class="pull-left mt0">
												<h1 class="text-left mt0">
													STATUS
													<span class="btn-group status mt0 mt6n" data-toggle="buttons">
												        <label class="btn btn-primary-alt labStatus">

															{{ radio_field("status", "id": "executed", "value": "EXECUTED", "class": "estExecuted") }} Executed

												        </label>
												        <label class="btn btn-primary-alt labStatus active">

															{{ radio_field("status", "id": "notExecuted", "value": "NOT EXECUTED", "class": "estExecuted") }} Not Executed

												        </label>
												    </span>
													<small><span id="status"></span></small>
												</h1>
					                		</div>
				                			<div class="pull-right mt0">
												<h1 class="text-right mt0" id="contract">
													CONTRACT
													<small>#<span id="conCode"></span></small>
												</h1>
											</div>
										</td>			                		
			                		</tr>
			                	</thead>
			                    <tbody>
			                        <tr>
			                            <td><strong>Client Code</strong></td>
			                            <td id="cliCode"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Client Name</strong></td>
			                            <td id="cliName"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Contract Date</strong></td>
			                            <td id="conDate"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Address</strong></td>
			                            <td id="address"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Phone</strong></td>
			                            <td id="phone"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Mobile Phone</strong></td>
			                            <td id="mobPhone"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Email</strong></td>
			                            <td id="email"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Service</strong></td>
			                            <td id="service"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Payment</strong></td>
			                            <td class="text-danger"><strong>$ <span id="payment"></span></strong></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Group</strong></td>
			                            <td id="group"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Position</strong></td>
			                            <td id="position"></td>
			                        </tr>
			                        <tr>
			                            <td><strong>Frequency</strong></td>
			                            <td id="frequency"></td>
			                        </tr>
			                    </tbody>
			                </table>
						</div>
						<div class="tab-pane" id="additional">
                            <table class="table table-striped table-bordered nowrap" id="tabServices" style="width:100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Additional Service</th>
										<th>Amount</th>
										<th>Creation Date</th>
										<th>Actions</th>
									</tr>
								</thead>
							</table>

			                {{ form("autocomplete": "off", "id": "forService", "data-parsley-validate": "data-parsley-validate", "class": "mt18") }}

			                    <div class="row">
									<div class="col-sm-9 mb-md">
										<label for="addiService" class="control-label required">
											<strong>Additional Service</strong>
										</label>
											
										{{ text_field("addiService", "class": "form-control", "placeholder": "Enter Additional Service", "required": "required") }}

									</div>
									<div class="col-sm-3 mb-md">
										<label for="inpAmount1" class="control-label required">
											<strong>Amount ($)</strong>
										</label>

										{{ text_field("inpAmount1", "class": "form-control amount", "placeholder": "0.00", "required": "required", "data-parsley-error-message": "Cost of service.", "data-parsley-amount": "data-parsley-amount") }}

									</div>
			                    </div>
			                    <div class="row">
									<div class="col-sm-12">
										
										{{ link_to("", "<i class='fa fa-plus'></i> Add Service", "class": "btn btn-inverse pull-right mb-md", "id": "addService") }}

									</div>
								</div>

			                {{ end_form() }}

						</div>
						<div class="tab-pane" id="payments">
		                    <div class="row mt9-5">
								<div class="col-sm-3 col-xs-6">
									<a class="info-tile tile-brown" href="#">
										<div class="tile-heading pb-sm">
											<div class="pull-center">
												<strong>Contracted</strong>
											</div>
										</div>
										<div class="tile-body pt0 pb-sm">
											<div class="pull-left"><i class="fa fa-check-square"></i></div>
											<div class="pull-right">
												<strong>$ <span id="contracted"></span></strong>
											</div>
										</div>
									</a>
								</div>
								<div class="col-sm-3 col-xs-6">
									<a class="info-tile tile-danger" href="#">
										<div class="tile-heading pb-sm">
											<div class="pull-center">
												<strong>Additional</strong>
											</div>
										</div>
										<div class="tile-body pt0 pb-sm">
											<div class="pull-left"><i class="fa fa-caret-square-o-up"></i></div>
											<div class="pull-right">
												<strong>$ <span id="tilAdditional"></span></strong>
											</div>
										</div>
									</a>
								</div>
								<div class="col-sm-3 col-xs-6">
									<a class="info-tile tile-purple" href="#">
										<div class="tile-heading pb-sm">
											<div class="pull-center">
												<strong>Amount</strong>
											</div>
										</div>
										<div class="tile-body pt0 pb-sm">
											<div class="pull-left"><i class="fa fa-plus-square"></i></div>
											<div class="pull-right">
												<strong>$ <span id="tilAmount"></span></strong>
											</div>
										</div>
									</a>
								</div>
								<div class="col-sm-3 col-xs-6">
									<a class="info-tile tile-inverse" href="#">
										<div class="tile-heading pb-sm">
											<div class="pull-center">
												<strong>Balance</strong>
											</div>
										</div>
										<div class="tile-body pt0 pb-sm">
											<div class="pull-left"><i class="fa fa-minus-square"></i></div>
											<div class="pull-right">
												<strong>$ <span id="balance"></span></strong>
											</div>
										</div>
									</a>
								</div>
							</div>
							<table id="tabPayments" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Payment Date</th>
										<th>Amount</th>
										<th>Payment Type</th>
										<th>Actions</th>
									</tr>
								</thead>
							</table>

			                {{ form("autocomplete": "off", "id": "forPayment", "data-parsley-validate": "data-parsley-validate", "class": "mt18") }}

			                    <div class="row">
									<div class="col-sm-9 mb-md">
										<label for="payType" class="control-label required">
											<strong>Payment Type</strong>
										</label>
										<select name="payType" id="payType" required="required">
											<option value="">-- SELECT --</option>

											{% for type in types %}

												<option value="{{ type.id }}">{{ type.typ_name }}</option>

											{% endfor %}

										</select>
									</div>
									<div class="col-sm-3 mb-md">
										<label for="inpAmount2" class="control-label required">
											<strong>Amount ($)</strong>
										</label>

										{{ text_field("inpAmount2", "class": "form-control amount", "placeholder": "0.00", "required": "required", "data-parsley-error-message": "Payment Amount.", "data-parsley-amount": "data-parsley-amount") }}

									</div>
			                    </div>
			                    <div class="row">
									<div class="col-sm-12 mb-md">
										
										{{ link_to("", "<i class='fa fa-plus'></i> Add Payment", "class": "btn btn-inverse pull-right", "id": "addPayment") }}

									</div>
								</div>

			                {{ end_form() }}

						</div>
  					</div>
				</div>
            </div>
            <div class="modal-footer">
                <span id="loaGroup" class="hide loading">

					{{ image("img/ajax.svg") }}

                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close">Close</button>
            </div>
        </div>
    </div>
</div>

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/services.js") }}