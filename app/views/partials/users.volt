{{ form("autocomplete": "off", "id": "forUser", "data-parsley-validate": "data-parsley-validate", "data-parsley-excluded": "input[name=status]") }}

	<div class="row">
		<div class="col-sm-5">
			<p>Fields marked with an asterisk (<span class="fc-dd">*</span>) are required.</p>
		</div>
		<div class="col-sm-7 text-right">
			<h1 class="lh0 mt0">
				Status
				<span class="btn-group status" data-toggle="buttons">
			        <label class="btn btn-inverse-alt {{ user is not defined or user.use_status == "ENABLED" ? "active" : "" }}" onclick="disUser('enauser', '{{ user is defined ? user.use_code : false }}')">

						{{ radio_field("status", "id": "enabled", "value": "ENABLED", "class": "estEnabled") }} <i class="fa fa-play"></i> Enabled

			        </label>
			        <label class="btn btn-inverse-alt {{ user.use_status == "DISABLED" ? "active" : "" }}" onclick="disUser('disuser', '{{ user is defined ? user.use_code : false }}')">

						{{ radio_field("status", "id": "disabled", "value": "DISABLED", "class": "estEnabled") }} <i class="fa fa-pause"></i> Disabled

			        </label>
			    </span>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 mb-md">
			<label for="useCode" class="control-label required">
				<strong>User Code</strong>
			</label>
				
			{{ text_field("useCode", "class": "form-control", "placeholder": "Enter User Code", "required": "required", "value": user is defined ? user.use_code : null) }}

		</div>
		<div class="col-sm-6 mb-md">
			<label for="name" class="control-label required">
				<strong>Name</strong>
			</label>

			{{ text_field("name", "class": "form-control", "placeholder": "Enter Name", "required": "required", "value": user is defined ? user.use_name : null) }}

		</div>
		<div class="col-sm-3 closest mb-md">
			<label for="phoNumber" class="control-label">
				<strong>Phone</strong>
			</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-phone"></i>
				</span>

				{{ text_field("phone", "class": "form-control", "placeholder": "Enter Phone", "value": user is defined ? user.use_phone : null) }}

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 closest mb-md">
			<label for="email" class="control-label required">
				<strong>Email</strong>
			</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-envelope"></i>
				</span>

				{{ email_field("email", "class": "form-control", "required": "required", "placeholder": "Enter Email", "value": user is defined ? user.use_email : null) }} 

			</div>
		</div>
		<div class="col-sm-6 closest mb-md">
			<label for="useRole" class="control-label required">
				<strong>User Role</strong>
			</label>
			<select name="useRole" id="useRole" required="required">
				<option value="">-- SELECT --</option>

				{% for rol in roles %}

					<option value="{{ rol.id }}" {% if user is defined and rol.id == user.rol_id %} selected="selected" {% endif %}>{{ rol.rol_name }}</option>

				{% endfor %}

			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 closest mb-md">
			<label for="username" class="control-label required">
				<strong>Username</strong>
			</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-user"></i>
				</span>

				{{ text_field("username", "class": "form-control {#text-lowercase#}", "placeholder": "Enter Username", "required": "required", "value": user is defined ? user.use_login : null) }}

			</div>
		</div>
		<div class="col-sm-4 closest mb-md">
			<label for="password" class="control-label required">
				<strong>Password</strong>
			</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-key"></i>
				</span>

				{{ password_field("password", "class": "form-control", "placeholder": "Enter Password", "required": "required", "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Enter password.") }}

			</div>
		</div>
		<div class="col-sm-4 closest mb-md">
			<label for="pasConfirm" class="control-label required">
				<strong>Password Confirmation</strong>
			</label>
			<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-key"></i>
				</span>

				{{ password_field("pasConfirmation", "class": "form-control", "placeholder": "Enter Password Confirmation", "required": "required", "data-parsley-equalto": "#password", "data-parsley-error-message": "<i class='fa fa-times-circle'></i> Enter password.") }}

			</div>
		</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/users.js") }}