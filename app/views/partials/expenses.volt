{{ form("autocomplete": "off", "id": "forExpense", "data-parsley-validate": "data-parsley-validate", "data-parsley-excluded": "input[name=status]") }}

	<div class="row">
		<div class="col-sm-5">
			<p>Fields marked with an asterisk (<span class="fc-dd">*</span>) are required.</p>
		</div>
		<div class="col-sm-7 text-right">
			<h1 class="lh0 mt0">
				Status
				<span class="btn-group status" data-toggle="buttons">
			        <label class="btn btn-inverse-alt {{ expense is not defined or expense.exp_status == "ENABLED" ? "active" : "" }}" onclick="disExpense('enaexpense', '{{ expense is defined ? expense.id : false }}')">

						{{ radio_field("status", "id": "enabled", "value": "ENABLED", "class": "estEnabled") }} <i class="fa fa-play"></i> Enabled

			        </label>
			        <label class="btn btn-inverse-alt {{ expense.exp_status == "DISABLED" ? "active" : "" }}" onclick="disExpense('disexpense', '{{ expense is defined ? expense.id : false }}')">

						{{ radio_field("status", "id": "disabled", "value": "DISABLED", "class": "estEnabled") }} <i class="fa fa-pause"></i> Disabled

			        </label>
			    </span>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 mb-md">
			<label for="description" class="control-label required">
				<strong>Description</strong>
			</label>

			{{ text_field("description", "class": "form-control", "placeholder": "Enter Description", "required": "required", "value": expense is defined ? expense.exp_description : null) }}

		</div>
		<div class="col-sm-2 mb-md">
			<label for="expDate" class="control-label required">
				<strong>Date</strong>
			</label>

			{{ text_field("expDate", "class": "form-control cur-pointer", "placeholder": date("m/d/Y"), "required": "required", "value": expense is defined ? funciones.cambiaf_a_normal(expense.exp_date) : date("m/d/Y")) }}

		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 mb-md closest">
			<label for="concept" class="control-label required">
				<strong>Expenditure Item</strong>
			</label>
			<select name="concept" id="concept" required="required">
				<option value="">-- SELECT --</option>

				{% for concept in concepts %}

					<option value="{{ concept.id }}" {% if expense is defined and concept.id == expense.con_id %} selected="selected" {% endif %}> {{ concept.con_name }} </option>

				{% endfor %}

			</select>
		</div>
		<div class="col-sm-2 mb-md">
			<label for="quantity" class="control-label required">
				<strong>Quantity</strong>
			</label>
				
			{{ text_field("quantity", "class": "form-control", "placeholder": "0", "required": "required", "data-parsley-min": 1, "value": expense is defined ? expense.exp_quantity : 1) }}

		</div>
		<div class="col-sm-3 mb-md">
			<label for="cost" class="control-label required">
				<strong>Cost ($)</strong>
			</label>
				
			{{ text_field("cost", "class": "form-control", "placeholder": "0,00", "required": "required", "data-parsley-costo": "data-parsley-costo", "value": expense is defined ? funciones.number_format(expense.exp_cost) : null) }}

		</div>
		<div class="col-sm-3 mb-md">
			<label for="total" class="control-label required">
				<strong>Total ($)</strong>
			</label>
				
			{{ text_field("total", "class": "form-control", "placeholder": "0,00", "readonly": "readonly", "value": expense is defined ? funciones.number_format(expense.exp_quantity * expense.exp_cost) : null) }}

		</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/expenses.js") }}