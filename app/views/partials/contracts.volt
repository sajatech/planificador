{{ form("autocomplete": "off", "id": "forContract", "data-parsley-validate": "data-parsley-validate", "data-parsley-excluded": "input[name=status], input[name=frequency], input[name=docType]") }}

	{#<div class="row">
		<div class="col-sm-6">
			<p>Fields marked with an asterisk (<span class="fc-dd">*</span>) are required.</p>
		</div>
		<div class="col-sm-6 text-right">
			<h1 style="margin-top: -12px !important; margin-bottom: 24px !important">Status
				<span class="fs-20" id="status">

					{% if contract is defined %}
						{{ funciones.ucwords(funciones.strtolower_utf8(contract.con_status)) }} 
					{% else %} 
						{{ "Enabled" }} 
					{% endif %}

				</span>
			</h1>
		</div>
	</div>#}

	<div class="row">
		<div class="col-sm-5">
			<p>Fields marked with an asterisk (<span class="fc-dd">*</span>) are required.</p>
		</div>
		<div class="col-sm-7 text-right">
			<h1 class="lh0 mt0">
				Status
				<span class="btn-group status" data-toggle="buttons">
			        <label class="btn btn-inverse-alt {{ contract is not defined or contract.con_status == "ENABLED" ? "active" : "" }}" onclick="disContract('enacontract', '{{ contract is defined ? contract.id : false }}')">

						{{ radio_field("status", "id": "enabled", "value": "ENABLED", "class": "estEnabled") }} <i class="fa fa-play"></i> Enabled

			        </label>
			        <label class="btn btn-inverse-alt {{ contract.con_status == "DISABLED" ? "active" : "" }}" onclick="disContract('discontract', '{{ contract is defined ? contract.id : false }}')">

						{{ radio_field("status", "id": "disabled", "value": "DISABLED", "class": "estEnabled") }} <i class="fa fa-pause"></i> Disabled

			        </label>
			    </span>
			</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-3 closest mb-md" id="col-cliCode">
			<label for="cliCode" class="control-label required">
				<strong>Client Code</strong>
			</label>
				
			{{ text_field("cliCode", "class": "form-control cliCode", "placeholder": "Client Code", "required": "required", "data-parsley-whitespace": "squish", "value": client is defined ? client.cli_code : null) }}

		</div>
		<div class="col-sm-6 closest mb-md" id="col-cliName">
			<label for="cliName" class="control-label required">
				<strong>Client Name</strong>
			</label>
				
			{{ text_field("cliName", "class": "form-control cliName", "placeholder": "Client Name", "required": "required", "data-parsley-whitespace": "squish", "value": client is defined ? client.cli_name : null) }}

		</div>
		<div class="col-sm-3 closest mb-md">
			<label for="conDate" class="control-label required">
				<strong>Contract Date</strong>
			</label>
			<div class="input-group date" id="datepicker-startview">
				<span class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</span>
			
				{{ text_field("conDate", "class": "form-control cur-pointer fs-12", "placeholder": "Contract Date", "required": "required", "value": contract is defined ? contract.con_day ~ ", " ~ funciones.cambiaf_a_normal(contract.con_date) : null) }}

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 mb-md">
			<label for="address" class="control-label">
				<strong>Address</strong>
			</label>
			<div class="input-icon left spinner">
				<i class="fa fa-fw fa-spin fa-spinner hide"></i>

				{{ text_area("address", "class": "form-control pl10", "placeholder": "Client Address", "rows": 5, "value": client is defined ? client.cli_address : null) }}

			</div>
		</div>
		<div class="col-sm-6">
			<div class="short-div">
				<div class="row">
					<div class="col-sm-6 mb-md">
						<label for="phone" class="control-label">
							<strong>Phone</strong>
						</label>
						<div class="input-icon left spinner">
							<i class="fa fa-fw fa-spin fa-spinner hide"></i>
							
							{{ text_field("phone", "class": "form-control pl10", "placeholder": "Client Phone", "value": client is defined ? client.cli_phone : null) }}

						</div>
					</div>
					<div class="col-sm-6 mb-md">
						<label for="mobPhone" class="control-label required">
							<strong>Mobile Phone</strong>
						</label>
						<div class="input-icon left spinner">
							<i class="fa fa-fw fa-spin fa-spinner hide"></i>

							{{ text_field("mobPhone", "class": "form-control pl10", "placeholder": "Client Mobile Phone", "required": "required", "value": client is defined ? client.cli_mob_phone : null) }}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 mb-md">
						<label for="email" class="control-label">
							<strong>Email</strong>
						</label>
						<div class="input-icon left spinner">
							<i class="fa fa-fw fa-spin fa-spinner hide"></i>

							{{ email_field("email", "class": "form-control pl10", "placeholder": "Client Email", "value": client is defined ? client.cli_email : null) }} 

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 mb-md">
			<label for="service" class="control-label required">
				<strong>Service</strong>
			</label>
				
			{{ text_field("service", "class": "form-control", "placeholder": "Service Contracted", "required": "required", "value": contract is defined ? contract.con_service : null) }}

		</div>
		<div class="col-sm-2 mb-md">
			<label for="pay" class="control-label required">
				<strong>Pay $</strong>
			</label>

			{{ text_field("pay", "class": "form-control", "placeholder": "0.00", "required": "required", "data-parsley-error-message": "Cost of service.", "data-parsley-pay": "data-parsley-pay", "readonly": contract is defined ? "readonly" : null, "value": contract is defined ? funciones.number_format(contract.con_pay) : null) }}

		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 mb-md">
			<label for="group" class="control-label required">
				<strong>Group</strong>
			</label>
			<select name="group" id="group" required="required">
				<option value="">-- SELECT --</option>

				{% for group in groups %}

					<option value="{{ group.id }}" {% if contract is defined and group.id == contract.gro_id %} selected="selected" {% endif %}>{{ group.gro_name }}</option>

				{% endfor %}

			</select>
		</div>
		<div class="col-sm-2 mb-md">
			<label for="position" class="control-label">
				<strong>Position</strong>
			</label>

			{{ text_field("position", "class": "form-control", "placeholder": "0", "readonly": "readonly", "value": contract is defined ? contract.con_position : null) }}

		</div>
		<div class="col-sm-3 mb-md">
			<label for="frequency" class="control-label">
				<strong>Frequency</strong>
			</label><br/>
            <label class="radio-inline icheck">

        		{{ radio_field("frequency", "id": "7", "class": "freFirst", "value": "7", "checked": (contract is defined and contract.con_frequency == 7) or contract is not defined ? "checked" : null) }}

        		<span>7</span>
    		</label>
            <label class="radio-inline icheck">

        		{{ radio_field("frequency", "id": "14", "value": "14", "checked": contract is defined and contract.con_frequency == 14 ? "checked" : null ) }}

        		<span>14</span>
    		</label>
            <label class="radio-inline icheck">

        		{{ radio_field("frequency", "id": "21", "value": "21", "checked": contract is defined and contract.con_frequency == 21 ? "checked" : null) }}

        		<span>21</span>
    		</label>
            <label class="radio-inline icheck">

        		{{ radio_field("frequency", "id": "28", "value": "28", "checked": contract is defined and contract.con_frequency == 28 ? "checked" : null) }}

        		<span>28</span>
    		</label>			
    	</div>
		<div class="col-sm-3 mb-md">
			<label for="docType" class="control-label">
				<strong>Document Type</strong>
			</label><br/>
            <label class="radio-inline icheck">

        		{{ radio_field("docType", "id": "invoice", "value": "INVOICE", "checked": (contract is defined and contract.con_doc_type == "INVOICE") or contract is not defined ? "checked" : null) }}

        		<span>Invoice</span>
    		</label>
            <label class="radio-inline icheck">

        		{{ radio_field("docType", "id": "receipt", "value": "RECEIPT", "checked": contract is defined and contract.con_doc_type == "RECEIPT" ? "checked" : null ) }}

        		<span>Receipt</span>
    		</label>			
    	</div>
	</div>

{{ end_form() }}

{{  javascript_include("assets/js/jquery-1.10.2.min.js") }}
{{  javascript_include("js/partials/contracts.js") }}