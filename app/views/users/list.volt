{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
            <li>

                {{ link_to("desktop", "Calendar") }}

            </li>
            <li>

                {{ link_to("users/registration", "Users") }}

            </li>
            <li class="active">

                {{ link_to("users/list", "Find Users") }}

            </li>
        </ol>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                            {{ image("img/ajax.svg") }}
                            {{ "Por favor, espere..." }}

                        </div>
                        <div class="panel panel-default hide" id="panUsers">
                            <div class="panel-heading">
                                <h2>Registered Users</h2>
                                <div class="panel-ctrls"></div>
                            </div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive mb0">                                
                                    <table class="table table-striped table-bordered nowrap tcur-pointer" id="users" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>User Code</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>User Role</th>
                                                <th>Username</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>                
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}