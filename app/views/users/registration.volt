{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
			<li>

				{{ link_to("desktop", "Calendar") }}

			</li>
			<li>

				{{ link_to("users/list", "Users") }}

			</li>
			<li class="active">

				{{ link_to("users/registration", "New User") }}

			</li>
        </ol>
        <div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Users Registration</h2>
								<div class="panel-ctrls">

            						{#{% if funciones.in_array(27, priRol) %}#}
										{{ link_to("users/list", "<i class='fa fa-search'></i><span> Find User</span>", "class": "btn btn-default ctrls pull-right mt-sm") }}
									{#{% endif %}#}

								</div>
							</div>
							<div class="panel-body">

								{{ partial("partials/users") }}

								<div class="panel-footer">
									<div class="clearfix pt20">
										<div class="pull-right">
											<span id="loaUser" class="hide">

												{{ image("img/ajax.svg") }}

											</span>

											{{ submit_button("Send", "class": "btn btn-primary", "id": "regUser") }}
											{{ submit_button("Cancel", "class": "btn btn-default", "id": "canUser") }}

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },
        
		errorsWrapper: "<span class='help-block'></span>", 
		errorTemplate: "<span></span>"
    };
</script>