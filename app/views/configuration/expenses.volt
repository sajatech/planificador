{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
            <li>

                {{ link_to("desktop", "Calendar") }}

            </li>
            <li>

                {{ link_to("configuration/expenses", "Configuration") }}

            </li>
            <li class="active">

                {{ link_to("configuration/expenses", "Expenditure Items") }}

            </li>
        </ol>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                            {{ image("img/ajax.svg") }}
                            {{ "Por favor, espere..." }}

                        </div>
                        <div class="panel panel-default hide" id="panExpenses">
                            <div class="panel-heading">
                                <h2 class="title c-w fz-24">List of Expenditure Items</h2>
                                <div class="panel-ctrls"></div>
                            </div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive mb0">                                
                                    <table class="table table-striped table-bordered nowrap tcur-pointer" id="concepts" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Item Code</th>
                                                <th>Creation Date</th>
                                                <th>Name</th>
                                                <th>Registered By</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>                
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{# Modal de conceptos de gastos #}
<div class="modal fade" id="modConcepts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bc-8bc">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title c-w fz-24"></h2>
            </div>
            <div class="modal-body bc-g">

                {{ form("autocomplete": "off", "id": "concept", "data-parsley-validate": "data-parsley-validate") }}

                    <div class="row">
                        <div class="col-sm-12 closest mb-md">
                            <label for="name" class="control-label">Name</label>

                            {{ text_field("name", "class": "form-control", "placeholder": "Enter Name", "required": "required") }}

                        </div>
                    </div>

                {{ end_form() }}

            </div>
            <div class="modal-footer">
                <span id="loaConcept" class="hide loading">

                    {{ image("img/ajax.svg") }}

                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Close</button>
                <button type="button" class="btn btn-inverse action"></button>
            </div>
        </div>
    </div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>