{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
            <li>

                {{ link_to("desktop", "Calendar") }}

            </li>
            <li>

                {{ link_to("configuration/groups", "Configuration") }}

            </li>
            <li class="active">

                {{ link_to("configuration/groups", "Groups") }}

            </li>
        </ol>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <div class="row">
                    <div class="col-md-12">
                        <div id="ajaxSpinner" class="fs-16 text-center mt-md">

                            {{ image("img/ajax.svg") }}
                            {{ "Por favor, espere..." }}

                        </div>
                        <div class="panel panel-default hide" id="panGroups">
                            <div class="panel-heading">
                                <h2 class="title c-w fz-24">Registered Work Groups</h2>
                                <div class="panel-ctrls"></div>
                            </div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive mb0">                                
                                    <table class="table table-striped table-bordered nowrap tcur-pointer" id="groups" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Group Code</th>
                                                <th>Name</th>
                                                <th>Leader</th>
                                                <th>Registered By</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>                
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{# Modal de grupos de trabajo #}
<div class="modal fade" id="modGroups" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title"></h2>
            </div>
            <div class="modal-body">

                {{ form("autocomplete": "off", "id": "group", "data-parsley-validate": "data-parsley-validate") }}

                    <div class="row">
                        <div class="col-sm-12 mb-md">
                            <label for="name" class="control-label required">Name</label>

                            {{ text_field("name", "class": "form-control", "placeholder": "Group Name", "required": "required") }}

                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-sm-12 mb-md">
                            <label for="leader" class="control-label required">Leader</label>

                            {{ text_field("leader", "class": "form-control", "placeholder": "Group Leader", "required": "required") }}

                        </div>
                    </div>

                {{ end_form() }}

            </div>
            <div class="modal-footer">
                <span id="loaGroup" class="hide loading">

					{{ image("img/ajax.svg") }}

                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
                <button type="button" class="btn btn-primary action"></button>
            </div>
        </div>
    </div>
</div>

{# ******************************** #}

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>