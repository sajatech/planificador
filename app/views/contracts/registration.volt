{{ assets.outputCss() }}

<div class="static-content">
    <div class="page-content">
        <ol class="breadcrumb">                        
            <li>

                {{ link_to("desktop", "Calendar") }}

            </li>
            <li>

                {{ link_to("contracts/list", "Clients") }}

            </li>
            <li class="active">

                {{ link_to("contracts/registration", "New Client") }}

            </li>
        </ol>
        <div class="container-fluid">
			<div data-widget-group="group1">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Client Registration</h2>
								<div class="panel-ctrls">

									{{ link_to("contracts/list", "<i class='fa fa-search'></i><span> Find Client</span>", "class": "btn btn-default ctrls pull-right mt-sm") }}

								</div>
							</div>
							<div class="panel-body">

								{{ partial("partials/contracts") }}

								<div class="panel-footer">
									<div class="clearfix pt20">
										<div class="pull-right">
											<span id="loaContract" class="hide">

												{{ image("img/ajax.svg") }}

											</span>

											{{ submit_button("Send", "class": "btn btn-primary", "id": "regContract") }}
											{{ submit_button("Cancel", "class": "btn btn-default", "id": "canContract") }}

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

{{ assets.outputJs() }}

<script>
    window.ParsleyConfig = {
        errorsContainer: function(el) {
            return el.$element.closest(".closest");
        },

  		errorsWrapper: "<span class='help-block'></span>", 
  		errorTemplate: "<span></span>"
    };
</script>