<?php

use Phalcon\Mvc\Model\Query;

class InvoicingController extends ControllerBase { 

    // Generación de documento de pago
    private function _documentbd($email, $explode) {
        $this->view->disable();

        if(!$this->request->get("generation")) {
            // Inicialización de variables
            if($email) {
                $contract = $this->request->get("contract");
                $type = $this->request->get("type");
                $number = $this->request->get("number");
            } else {
                $contract = $explode[0];
                $type = $explode[1];
                $number = $explode[2];
            }

            // Servicios adicionales
            $additional = "SELECT
                a.id,
                a.add_name,
                a.add_amount,
                s.ser_date,
                c.con_day
                FROM
                Additional AS a,
                Services AS s,
                Documents AS d,
                Contracts AS c
                WHERE
                a.ser_id = s.id
                AND
                s.id = d.ser_id
                AND
                s.con_id = c.id
                AND
                d.doc_status = 'ENABLED'
                AND 
                d.doc_number = '" . $number . "'
                ORDER BY
                s.ser_date";

            // ******************************** //

            // Tipos de pago
            $payTypes = "SELECT
                t.typ_name
                FROM
                Types AS t,
                Payments AS p,
                Services AS s,
                Documents AS d
                WHERE
                t.id = p.typ_id
                AND
                p.ser_id = s.id
                AND
                d.ser_id = s.id
                AND
                d.doc_number = '" . $number . "'";

            // ******************************** //
        } else {
            // Inicialización de variables
            $contract = $this->request->get("contract");
            $type = $this->request->get("type");
            $number = false;

            // Servicios adicionales
            $additional = "SELECT
                a.id,
                a.add_name,
                a.add_amount,
                s.ser_date,
                c.con_day
                FROM
                Additional AS a,
                Services AS s,
                Contracts AS c
                WHERE
                NOT EXISTS (SELECT 1
                    FROM
                    Documents AS d
                    WHERE
                    d.ser_id = s.id
                    AND
                    d.doc_status = 'ENABLED')
                AND
                a.ser_id = s.id
                AND
                s.con_id = c.id
                AND
                s.ser_status = 'EXECUTED'
                AND
                s.con_id = '" . $contract  . "'";

            // Tipos de pago
            $payTypes = "SELECT
                t.typ_name
                FROM
                Types AS t,
                Payments AS p,
                Services AS s,
                Contracts AS c
                WHERE
                t.id = p.typ_id
                AND
                s.con_id = c.id
                AND
                p.ser_id = s.id
                AND
                c.id = '" . $contract . "'";
        }

        if(!empty($this->request->get("select"))) {
            $query = " AND (";

            foreach($this->request->get("select") as $index => $select) {
                $query .= "s.id = '" . $select . "'";

                if(count($this->request->get("select")) - 1 > $index)
                    $query .= " OR ";
            }

            $query .= ")";
            $additional .= $query;
            $payTypes .= $query;
        }

        $additional = (new Query($additional, $this->getDI()))->execute();
        $payTypes = (new Query($payTypes, $this->getDI()))->execute();

        $arrTypes = array();
        foreach($payTypes as $payType)
            $arrTypes[] = " " . $payType->typ_name;

        // ******************************** //

        // Datos del contrato
        $contract = Contracts::findFirstById($contract);

        // Obtención de los valores adeudados y pagados
        $values = (new ServicesController())->getvalues($contract->id, $contract->con_pay, "EXECUTED", $number);

        // Generación de correlativo
        if(!$number) {
            if(count($values["services"]) > 0) {
                if($type == "receipt") {
                    $maximum = Documents::maximum(array("column" => "doc_number", "conditions" => "doc_invoice = 0"));
                    $flag = true;
                } else
                    $maximum = Documents::maximum(array("column" => "doc_number", "conditions" => "doc_invoice = 1"));

                if($maximum == 0)
                    $docNumber = isset($flag) ? 1 : 1524;
                else if(Documents::findFirst("doc_number = '" . $maximum . "' and doc_status = 'ENABLED'"))
                    $docNumber = $maximum + 1;
                else
                    $docNumber = $maximum;

                $docAmount = $contract->con_pay * count($values["services"]) + $values["additional"];

                foreach($values["services"] as $service) {
                    $item = Documents::findFirst("ser_id = '" . $service->id . "' AND doc_status = 'ENABLED'");

                    if(!$item) {
                        $document = new Documents();
                        $document->setSerId($service->id);
                        $document->setDocNumber($docNumber);
                        $document->setDocAmount($docAmount);

                        isset($flag) ? $document->setDocInvoice(0) : $document->setDocInvoice(1);

                        if(!$document->save())
                            exit("An internal error has occurred, please close the window and try again.");
                    }
                }
            }
        } else
            $docNumber = $number;

        // ******************************** //

        // Datos del cliente
        $client = Clients::findFirstById($contract->cli_id);

        $html = $this->view->getRender("invoicing", $type, array(
            "additional" => $additional,
            "contract" => $contract,
            "client" => $client,
            "docStatus" => $values["docStatus"],
            "document" => $this->funciones->str_pad($docNumber),
            "payments" => $values["payments"],
            "paid" => $values["paid"],
            "payTypes" => implode(",", $arrTypes),
            "services" => $values["services"]
        ));

        $this->mpdf->WriteHTML($html);

        if($this->request->get("generation") == 1) {
            $separador = "vWQJ%Ryv";
            $file = "Cliente - " . $client->cli_name . " - " . ucfirst($type) . " # " . $this->funciones->str_pad($docNumber) . ".pdf";
            $cadModificar = $file . $separador . "generation";
            $cadModificada = $this->funciones->encrypt($cadModificar, "fVBk3fJ5+dX*");
            $this->mpdf->Output("files/" . $file, "F");
            
            return $cadModificada;
        } else if(!$email) {
            $this->mpdf->Output();
            exit;
        } else {
            $file = "Cliente - " . $client->cli_name . " - " . ucfirst($type) . " # " . $this->funciones->str_pad($docNumber) . ".pdf";
            $this->mpdf->Output("files/" . $file, "F");
            $parametros["docNumber"] = $this->funciones->str_pad($docNumber);
            $parametros["client"] = $client->cli_name;

            return $parametros;
        }
    }

    // Vista de generación de documentos
    public function generationAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/invoicing/generation.js");
    }

    // Invocar la generación del documento
    public function documentbdAction() {
        $this->view->disable();
        return $this->_documentbd(false, false);
    }

    // Documento de servicios en formato pdf
    public function documentAction() {
        $this->view->disable();

        $cadDescodificada = $this->funciones->dencrypt($this->request->get("file"), "fVBk3fJ5+dX*");
        $explode = explode("vWQJ%Ryv", $cadDescodificada);

        if($explode[1] == "generation") {
            $this->mpdf->SetImportUse();
            $pagecount = $this->mpdf->SetSourceFile("files/" . $explode[0]);
            $tplId = $this->mpdf->ImportPage($pagecount);
            $this->mpdf->UseTemplate($tplId);
            $this->mpdf->Output();
            exit;
        } else
            return $this->_documentbd(false, $explode);
    }

    // Vista de documentos emitidos
    public function issuedAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/invoicing/issued.js");
    }

    // Encriptación de datos para reimpresión de documento
    public function encurlAction() {
        $this->view->disable();
        $separador = "vWQJ%Ryv";
        $cadModificar = $this->request->get("contract") . $separador . $this->request->get("type") . $separador . $this->request->get("number");
        $cadModificada = $this->funciones->encrypt($cadModificar, "fVBk3fJ5+dX*");
        
        return $cadModificada;
    }

    // Enviar por email factura o nota de entrega
    public function senemailAction() {
        $this->view->disable();

        //if($this->request->isPost()) {

            //$client = Clients::findFirstByCliCode($this->request->getPost("client"));

            //if($client) {
                //if($client->cli_email != null) { 
                    // Se genera el archivo pdf
                    $document = $this->_documentbd(true, false);
                    // Se envía el email
                    $send = $this->mailService->send("document", array("file" => "files/Cliente - " . $document["client"] . " - " . ucfirst($this->request->get("type")) . " # " . $document["docNumber"] . ".pdf", "client" => $document["client"], "type" => $this->request->get("type")), ["anaandrade133@gmail.com"], "Payment Support");

                    if($send !== true)
                        $errores[] = $send;
                //} else
                    //$errores[] = "Client does not have associated email.";
            //}
        //}

        if(count($errores) > 0) {
            $parametros["text"] = implode("</br>", $errores);
            $parametros["type"] = "error";
        } else {
            $parametros["text"] = "Document has been sent correctly to the client " . $document["client"] . ".";
            $parametros["type"] = "success";
        }

        echo json_encode($parametros);
    }

}

?>