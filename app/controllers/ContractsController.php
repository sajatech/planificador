<?php

class ContractsController extends ControllerBase { 

    // Vista de registro de contratos
    public function registrationAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/css/datepicker.css")
            ->addCss("assets/plugins/iCheck/skins/minimal/red.css")
            ->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/form-typeahead/typeahead.bundle.min.js")
            ->addJs("assets/plugins/iCheck/icheck.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/contracts/registration.js");

        // Lista de grupos de trabajo
        $groups = Groups::find(array("order" => "gro_name"));
        $this->view->setVar("groups", $groups);
    }

    // Determinación de la posición del contrato/servicio
    public function getnumcontractsAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $conDate = explode(", ", $this->request->getPost("conDate"));
            $contracts = Contracts::find("con_date = '" . $this->funciones->cambiaf_a_sql($conDate[1]) . "' and gro_id = '" . $this->request->getPost("group") . "'");

            echo count($contracts);
        }
    }

    // Almacenamiento del contrato en la base de datos
    public function registrationbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $client = Clients::findFirst("upper(cli_code) = '" . $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("cliCode"))) . "' or cli_name = '" . $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("cliName"))) . "'");

            if(!$client) {
                // Datos del cliente
                $client = new Clients();

                $client->setCliCode($this->funciones->squish($this->request->getPost("cliCode")));
                $client->setCliName($this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("cliName"))));
                $client->setCliPhone($this->request->getPost("phone"));
                $client->setCliMobPhone($this->request->getPost("mobPhone"));
                $client->setCliAddress($this->funciones->strtoupper_utf8($this->request->getPost("address")));
                $client->setCliEmail($this->funciones->normaliza($this->request->getPost("email")));
                $client->setUseId($this->funciones->getUsuario());

                // ******************************** //

                if(!$client->save()) {
                    foreach($client->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                } else {
                    // Datos del contrato
                    $contract = new Contracts();
                    $conDate = explode(", ", $this->request->getPost("conDate"));
                    
                    $contract->setConDay($conDate[0]);
                    $contract->setConDate($this->funciones->cambiaf_a_sql($conDate[1]));
                    $contract->setConService($this->funciones->strtoupper_utf8($this->request->getPost("service")));
                    $contract->setConPay($this->funciones->cambiam_a_numeric($this->request->getPost("pay")));
                    $contract->setConPosition($this->request->getPost("position"));
                    $contract->setConFrequency($this->request->getPost("frequency"));
                    $contract->setConDocType($this->request->getPost("docType"));
                    $contract->setConColor($this->funciones->array_rand());
                    $contract->setConStatus($this->request->get("status"));
                    $contract->setCliId($client->id);
                    $contract->setGroId($this->request->getPost("group"));
                    $contract->setUseId($this->funciones->getUsuario());

                    // ******************************** //

                    if(!$contract->save()) {
                        foreach($contract->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                }
            } else
                $errores[] = "There is already a registered client with this code or name.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Contract with code #" . $this->funciones->str_pad($contract->id) . " registered correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de contratos registrados
    public function listAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/contracts/list.js");
    }

    // Obtención de la lista de contratos
    public function getcontractsAction() {
        $this->view->disable();
        $contracts = Contracts::find(array("order" => "con_status desc, id desc"));
        $arrContracts = array();

        foreach($contracts as $clave => $contract) {
            $label = $contract->con_status == "ENABLED" ? "label-success" : "label-danger";
            
            // Datos del cliente
            $client = Clients::findFirstById($contract->cli_id);
            
            // Datos del grupo de trabajo
            $group = Groups::findFirstById($contract->gro_id);

            $arrContracts["aaData"][$clave]["conCode"] = $this->funciones->str_pad($contract->id);
            $arrContracts["aaData"][$clave]["client"] = $client->cli_name . " (" . $client->cli_code . ")";
            $arrContracts["aaData"][$clave]["conDate"] = $contract->con_day . ", " . $this->funciones->cambiaf_a_normal($contract->con_date);
            $arrContracts["aaData"][$clave]["service"] = $contract->con_service;
            $arrContracts["aaData"][$clave]["pay"] = "$ " . $this->funciones->number_format($contract->con_pay);
            $arrContracts["aaData"][$clave]["group"] = $group->gro_name;
            $arrContracts["aaData"][$clave]["status"] = "<span class='label " . $label . "'>" . $contract->con_status . "</span>";
        }

        //$arrArticulos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 1);

        return json_encode($arrContracts);
    }

    // Vista de actualización de contratos
    public function updateAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets->addCss("assets/css/datepicker.css")
                ->addCss("assets/plugins/iCheck/skins/minimal/red.css")
                ->addCss("assets/plugins/form-select2/select2.css");

            $this->assets
                ->addJs("assets/js/jquery.price_format.min.js")
                ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("assets/plugins/form-select2/select2.min.js")
                ->addJs("assets/plugins/form-typeahead/typeahead.bundle.min.js")
                ->addJs("assets/plugins/iCheck/icheck.min.js")
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("js/contracts/update.js");

            // Datos del contrato
            $contract = Contracts::findFirstById($this->request->getPost("code"));

            if($contract) {
                $this->view->setVar("contract", $contract);

                // Datos del cliente
                $client = Clients::findFirstById($contract->cli_id);            
                $this->view->setVar("client", $client);

                // Lista de grupos de trabajo
                $groups = Groups::find(array("order" => "gro_name"));
                $this->view->setVar("groups", $groups);
            } else
                $this->response->redirect("contracts/list");
        } else {
            $this->response->redirect("contracts/list");
            return false;
        }
    }

    // Actualización de contratos en la base de datos
    public function updatebdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $contract = Contracts::findFirstById($this->request->get("contract"));
            $errores = array();

            if($contract) {
                // Datos del cliente
                $client = Clients::findFirst("(upper(cli_code) = '" . $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("cliCode"))) . "' or cli_name = '" . $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("cliName"))) . "') and id != '" . $this->request->get("client") . "'");

                if(!$client) {
                    $client = Clients::findFirstById($this->request->get("client"));

                    if($client) {
                        $client->cli_code = $this->request->getPost("cliCode");
                        $client->cli_name = $this->funciones->strtoupper_utf8($this->request->getPost("cliName"));
                        $client->cli_phone = $this->request->getPost("phone");
                        $client->cli_mob_phone = $this->request->getPost("mobPhone");
                        $client->cli_address = $this->funciones->strtoupper_utf8($this->request->getPost("address"));
                        $client->cli_email = $this->funciones->normaliza($this->request->getPost("email"));
                        $client->use_id = $this->funciones->getUsuario();

                        if(!$client->save()) {
                            foreach($client->getMessages() as $mensaje)
                                $errores[] = $mensaje;
                        } else {
                            $conDate = explode(", ", $this->request->getPost("conDate"));

                            $contract->con_day = $conDate[0];
                            $contract->con_date = $this->funciones->cambiaf_a_sql($conDate[1]);
                            $contract->con_service = $this->funciones->strtoupper_utf8($this->request->getPost("service"));
                            $contract->con_pay = $this->funciones->cambiam_a_numeric($this->request->getPost("pay"));
                            $contract->con_position = $this->request->getPost("position");
                            $contract->con_frequency = $this->request->getPost("frequency");
                            $contract->con_doc_type = $this->request->getPost("docType");
                            $contract->cli_id = $client->id;
                            $contract->gro_id = $this->request->getPost("group");
                            $contract->use_id = $this->funciones->getUsuario();

                            // ******************************** //

                            if(!$contract->update()) {
                                foreach($contract->getMessages() as $mensaje)
                                    $errores[] = $mensaje;
                            } else {
                                $services = Services::find("con_id = '" . $contract->id . "' and ser_status = 'NOT EXECUTED'");

                                if(!$services->delete()) {
                                    foreach($services->getMessages() as $mensaje)
                                        $errores[] = $mensaje;
                                }
                            }

                        }
                    }
                } else
                    $errores[] = "There is already a registered client with this code or name.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Contract with code #" . $this->funciones->str_pad($contract->id) . " updated correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de contratos
    public function discontractAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $contract = Contracts::findFirstById($this->request->getPost("code"));

            if($contract) {
                $contract->con_status = "DISABLED";

                if(!$contract->update()) {
                    foreach($contract->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no contract.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Contract with code # " . $this->funciones->str_pad($contract->id) . " disabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de contratos
    public function enacontractAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $contract = Contracts::findFirstById($this->request->getPost("code"));

            if($contract) {
                $contract->con_status = "ENABLED";

                if(!$contract->update()) {
                    foreach($contract->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no contract.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Contract with code # " . $this->funciones->str_pad($contract->id) . " enabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Obtención del detalle del contrato
    public function getcontractAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $contract = Contracts::findFirstById($this->request->getPost("contract"));

            if($contract) {
                // Datos del grupo de trabajo
                $group = Groups::findFirstById($contract->gro_id);
                
                // Datos del servicio
                $service = Services::findFirst("ser_date = '" . $this->request->getPost("serDate") . "' and con_id = '" . $contract->id . "'");

                // Obtención de los valores adeudados y pagados
                $values = (new ServicesController())->getvalues($service ? $service->id : false, $contract->con_pay, "ALL", false);

                $parametro["contract"] = $contract;
                $parametro["group"] = $group->gro_name;
                $parametro["service"] = $service ? (int) $service->id : "false";
                $parametro["payments"] = $values["payments"];
                $parametro["additional"] = $values["additional"];
                $parametro["amount"] = $values["amount"];
                $parametro["balance"] = $values["balance"];
                $parametro["status"] = $service ? $service->ser_status : "NOT EXECUTED";
                $parametro["paid"] = $values["paid"];
                
                return json_encode($parametro);
            }
            else {
                $parametro["false"] = true;
                return json_encode($parametro);
            }
        }
    }

    public function updpositionsAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();

            foreach($this->request->getPost("serList") as $index => $item) {
                $service = Services::findFirst(array("ser_date = '" . $item["date"] . "' and con_id = '" . $item["contract"] . "'"));
                $date = $item["date"];

                if(!$service) {
                    // Datos del servicio principal
                    $service = new Services();

                    $service->setSerDate($date);
                    $service->setSerPosition($index + 1);
                    $service->setConId($item["contract"]);
                    $service->setUseId($this->funciones->getUsuario());

                    if(!$service->save()) {
                        foreach($service->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                } else {
                    $service->ser_position = $index + 1;

                    if(!$service->update()) {
                        foreach($service->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "List of services for date " . $this->funciones->cambiaf_a_normal($date) . " has been saved correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}