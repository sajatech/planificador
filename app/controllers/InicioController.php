    <?php

use Phalcon\Mvc\Model\Query;

class InicioController extends ControllerBase {

    private function _registerSession($user, $rol) {
        //$request = new Request();
        //$acceso = new Accesos();

        //$acceso->setAccIp($request->getClientAddress());
        //$acceso->setUsuId($usuario[0]->id);

        //if($acceso->save()) { echo $usuario[0]->rol_nombre;
            //$nombre = explode(" ", $usuario[0]->usu_razonsocial);

            // Se crea la sesión para el usuario
            $this->session->set(strtolower($rol->rol_name), array(
                "user" => $user->id,
                "name" => $user->use_name,
                "rol" => $rol->id 
                //"nombre" => ucwords($funciones->strtolower_utf8($nombre[0]))
            ));
        //}
    }

    // Formulario de acceso
    public function indexAction() {
        $this->getassets();
        
        $this->assets
        	->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
        	->addJs("js/inicio/index.js?v=2");
    }

    // Validación de credenciales de usuario
    public function loginAction() {
    	$this->view->disable();

        if($this->request->isPost()) {
            $user = Users::findFirst("use_login = '" . $this->request->getPost("username") . "' and use_password = '" . md5($this->request->getPost("password")) . "'");

            if($user) {
                $rol = Roles::findFirstById($user->rol_id); 

                if($rol) {
                    $this->_registerSession($user, $rol);
                    echo 0;
                } else
                    echo "User does not have access privileges.";
            } else
                echo "Username or password incorrect.";
		}
    }

    // Cierre de sesión
    public function logoutAction() {
        // Se eliminan las sesiones abiertas
        $this->session->destroy();

        return $this->dispatcher->forward(array(
            "action" => "index"
        ));
    }

    // Recuperación de contraseña
    public function passwordAction() {
        $this->getassets();
        
        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/inicio/password.js");
    }

    public function resetAction() {

    }

}