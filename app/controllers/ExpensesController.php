<?php

class ExpensesController extends ControllerBase {

    // Obtención de la lista de conceptos de gastos
    public function getconceptsAction() {
        $this->view->disable();
        $concepts = ExpConcepts::find(array("order" => "id desc"));
        $arrConcepts = array();

        foreach($concepts as $clave => $concept) {
            $user = Users::findFirstById($concept->use_id);
            $arrConcepts["aaData"][$clave]["conCode"] = $this->funciones->str_pad($concept->id);
            $arrConcepts["aaData"][$clave]["creDate"] = $this->funciones->cambiaf_a_normal($this->funciones->getdate($concept->con_cre_date));
            $arrConcepts["aaData"][$clave]["name"] = $concept->con_name;
            $arrConcepts["aaData"][$clave]["regBy"] = $user->use_name . " (" . $user->use_code . ")";
            $arrConcepts["aaData"][$clave]["status"] = "<span class='label " . ($concept->con_status == "ENABLED" ? "label-success" : "label-danger") . "'>" . $concept->con_status . "</span>";
        }

        //$arrConcepts["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 14);

        return json_encode($arrConcepts);
    }

    // Vista de registro de gastos
    public function registrationAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/css/datepicker.css")
            ->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("js/expenses/registration.js");

        // Lista de conceptos
        $concepts = ExpConcepts::find(array("con_status = 'ENABLED'", "order" => "con_name"));
        $this->view->setVar("concepts", $concepts);
    }

    // Almacenamiento del gasto en la base de datos
    public function registrationbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $expense = new Expenses();

            // Datos del gasto
            $expense->setExpDescription($this->funciones->strtoupper_utf8($this->request->getPost("description")));
            $expense->setExpDate($this->funciones->cambiaf_a_sql($this->request->getPost("expDate")));
            $expense->setExpQuantity($this->request->getPost("quantity"));
            $expense->setExpCost($this->funciones->cambiam_a_numeric($this->request->getPost("cost")));
            $expense->setExpStatus($this->request->get("status"));
            $expense->setConId($this->request->getPost("concept"));
            $expense->setUseId($this->funciones->getUsuario());

            // ******************************** //

            if(!$expense->save()) {
                foreach($expense->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense for item " . ExpConcepts::findFirstById($expense->getConId())->con_name . " registered correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de gastos registrados
    public function listAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/expenses/list.js");
    }

    // Obtención de la lista de gastos
    public function getexpensesAction() {
        $this->view->disable();
        $expenses = Expenses::find(array("order" => "id desc"));
        $arrExpenses = array();

        foreach($expenses as $clave => $expense) {
            $arrExpenses["aaData"][$clave]["expCode"] = $this->funciones->str_pad($expense->id);
            $arrExpenses["aaData"][$clave]["description"] = $expense->exp_description;
            $arrExpenses["aaData"][$clave]["expDate"] = $this->funciones->cambiaf_a_normal($expense->exp_date);
            $arrExpenses["aaData"][$clave]["total"] = "$ " . $this->funciones->number_format($expense->exp_quantity * $expense->exp_cost);
            $arrExpenses["aaData"][$clave]["expItem"] = ExpConcepts::findFirstById($expense->con_id)->con_name;
            $arrExpenses["aaData"][$clave]["status"] = "<span class='label " . ($expense->exp_status == "ENABLED" ? "label-success" : "label-danger") . "'>" . $expense->exp_status . "</span>";
        }

        //$arrExpenses["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 15);

        return json_encode($arrExpenses);
    }

    // Vista de actualización de gastos
    public function updateAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets->addCss("assets/css/datepicker.css")
                ->addCss("assets/plugins/form-select2/select2.css");

            $this->assets
                ->addJs("assets/js/jquery.price_format.min.js")
                ->addJs("assets/js/number.format.js")
                ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("assets/plugins/form-select2/select2.min.js")
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("js/expenses/update.js");

            // Datos del gasto
            $expense = Expenses::findFirstById($this->request->getPost("code"));
            $expense ? $this->view->setVar("expense", $expense) : $this->response->redirect("expenses/list");

            // Valor del estatus del gasto
            $this->tag->setDefault("status", $expense->exp_status);

            // Lista de partidas o conceptos
            $concepts = ExpConcepts::find(array("order" => "con_name"));
            $this->view->setVar("concepts", $concepts);
        } else {
            $this->response->redirect("expenses/list");
            return false;
        }
    }

    // Actualización de gastos en la base de datos
    public function updatebdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $expense = Expenses::findFirstById($this->request->get("id"));
            $errores = array();

            if($expense) {
                // Datos del gasto
                $expense->exp_description = $this->funciones->strtoupper_utf8($this->request->getPost("description"));
                $expense->exp_date = $this->funciones->cambiaf_a_sql($this->request->getPost("expDate"));
                $expense->exp_quantity = $this->request->getPost("quantity");
                $expense->exp_cost = $this->funciones->cambiam_a_numeric($this->request->getPost("cost"));
                $expense->con_id = $this->request->getPost("concept");

                // ******************************** //

                if(!$expense->update()) {
                    foreach($expense->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
        }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense for item " . ExpConcepts::findFirstById($expense->id)->con_name . " updated correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de gastos
    public function disexpenseAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $expense = Expenses::findFirstById($this->request->getPost("code"));

            if($expense) {
                $expense->exp_status = "DISABLED";

                if(!$expense->update()) {
                    foreach($expense->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no expense.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense for item " . ExpConcepts::findFirstById($expense->id)->con_name . " disabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de gastos
    public function enaexpenseAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $expense = Expenses::findFirstById($this->request->getPost("code"));

            if($expense) {
                $expense->exp_status = "ENABLED";

                if(!$expense->update()) {
                    foreach($user->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no expense.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense for item " . ExpConcepts::findFirstById($expense->id)->con_name . " enabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}