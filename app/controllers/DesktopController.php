<?php

class DesktopController extends ControllerBase {

    public function indexAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();
        
        $this->assets->addCss("assets/plugins/fullcalendar/fullcalendar.min.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css")
            ->addCss("assets/plugins/form-nestable/jquery.nestable.css")
            ->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/jstree/dist/themes/avenger/style.min.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/fullcalendar/moment.min.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/form-nestable/jquery.nestable.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/fullcalendar/fullcalendar.min.js")
            ->addJs("assets/plugins/jstree/dist/jstree.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/desktop/index.js?v=7");

		// Lista de tipos de pago
        $types = Types::find(array("order" => "typ_name"));
        $this->view->setVar("types", $types);
    }

}