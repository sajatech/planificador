<?php

class ClientsController extends ControllerBase { 

    public function getclientsAction() {
        $this->view->disable();
        $clients = Clients::find(array("order" => "cli_name"));
        $arrClients = array();

        foreach($clients as $index => $client) {
           $arrClients[$index]["code"] = $client->cli_code;
           $arrClients[$index]["name"] = $client->cli_name;
        }

        echo json_encode($arrClients);
    }

    // Obtención del detalle del cliente
    public function getclientAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $client = $this->request->getPost("type") == "code" ? Clients::findFirstByCliCode($this->request->getPost("client")) : Clients::findFirstByCliName($this->request->getPost("client"));

            if($client) 
                return json_encode($client);
            else {
                $parametro["false"] = true;
                return json_encode($parametro);
            }
        }
    }

}