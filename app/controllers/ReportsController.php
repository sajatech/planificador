<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo\Mysql;

class ReportsController extends ControllerBase {

    // ******** REPORTE DE RESUMEN DE INGRESOS (INICIO) ************************ //

    // Query para resumen de ingresos
    private function _getincomebd($since, $until, $client, $payment) {
        // Lista de ingresos
        $query = "SELECT
            p.pay_date,
            p.pay_amount,
            c.cli_name,
            c.cli_code,
            t.typ_name,
            o.con_service
            FROM
            Payments AS p,
            Clients AS c,
            Types AS t,
            Contracts AS o,
            Services AS s
            WHERE
            p.typ_id = t.id
            AND
            o.cli_id = c.id
            AND 
            p.ser_id = s.id 
            AND 
            s.con_id = o.id
            AND
            s.ser_status = 'EXECUTED'
            AND
            p.pay_date
            BETWEEN
            '" . $this->funciones->cambiaf_a_sql($since) . "'
            AND
            '" . $this->funciones->cambiaf_a_sql($until) . "'";

        if(!empty($client)) {
            $query .= " AND
            c.id = '" . $client . "'";
        }

        if(!empty($payment)) {
            $query .= " AND
            t.id = '" . $payment . "'";
        }

        $query .= " ORDER BY
            p.id";

        $income = (new Query($query, $this->getDI()))->execute();

        // ******************************** //

        return $income;   
    }

    // Reporte de resumen de ingresos
    public function incomeAction() {
        $this->view->setTemplateAfter("main");
        $this->view->pick("reports/income/consult");        
        $this->getassets();

        $this->assets->addCss("assets/css/datepicker.css")
            ->addCss("assets/plugins/form-select2/select2.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/reports/income.js");

        // Lista de clientes
        $clients = Clients::find(array("order" => "cli_name, cli_code"));
        $this->view->setVar("clients", $clients);

        // Lista de tipos de pago
        $types = Types::find(array("order" => "typ_name"));
        $this->view->setVar("types", $types);
    }

    // Obtención de la lista de ingresos
    public function getincomeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->pick("reports/income/getincome");        

        if($this->request->isPost()) {
            $income = $this->_getincomebd($this->request->getPost("since"), $this->request->getPost("until"), $this->request->getPost("client"), $this->request->getPost("payment"));
            count($income) > 0 ? $this->view->setVar("income", $income) : $this->view->disable();
        }
    }

    // Reporte de ingresos en formato pdf
    public function income_summaryAction() {
        $this->view->disable();
        $this->view->pick("reports/income/income_summary");
        $income = $this->_getincomebd($this->request->get("since"), $this->request->get("until"), $this->request->get("client"), $this->request->get("payment"));

        $html = $this->view->getRender("reports", "income_summary", array(
            "income" => $income
        )); 

        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // ******** REPORTE DE RESUMEN DE INGRESOS (FIN) ************************ //


    // ******** REPORTE SEMANAL DE SERVICIOS (INICIO) ************************ //

    // Reporte semanal de servicios
    public function weeklyAction() {
        $this->view->setTemplateAfter("main");
        $this->view->pick("reports/weekly/consult");        
        $this->getassets();

        $this->assets->addCss("assets/css/datepicker.css")
            ->addCss("assets/plugins/bootstrap-datepicker/weekpicker.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css");

        $this->assets
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")  
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/reports/weekly.js");
    }

    // Obtención de la lista de servicios de la semana
    public function getweeklyAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->pick("reports/weekly/getweekly");           

        if($this->request->isPost()) {
            $weeYear = explode(" to ", $this->request->getPost("week"));
            $date1 = explode("/", $weeYear[0]);
            $day = $date1[1];
            $lasDay = 28;
            $weekday = 0; // Domingo se representa con el número 6
            
            while(checkdate($date1[0], $lasDay + 1, $date1[2]))
                $lasDay++;

            while($weekday <= 6) {
                $html = ""; 
                $day = strlen($day) == 1 ? "0" . $day : $day;

                switch($weekday) {
                    case 1:
                        $weeDay = 0;
                        break;
                    case 2:
                        $weeDay = 1;
                        break;
                    case 3:
                        $weeDay = 2;
                        break;
                    case 4:
                        $weeDay = 3;
                        break;
                    case 5:
                        $weeDay = 4;
                        break;
                    case 6:
                        $weeDay = 5;
                        break;
                    default:
                        $weeDay = 6;
                }

                $query = "SELECT
                    c.cli_name, 
                    WEEKDAY(o.con_date) AS weekday,
                    o.id,
                    o.con_date,
                    o.con_frequency
                    FROM
                    Clients AS c,
                    Contracts AS o,
                    Groups AS g
                    WHERE
                    o.cli_id = c.id
                    AND 
                    o.con_status = 'ENABLED'
                    AND
                    o.gro_id = g.id
                    AND
                    WEEKDAY(o.con_date) = '" . $weeDay . "' 
                    ORDER BY 
                    o.gro_id, 
                    o.con_date, 
                    c.id";

                $weekly = (new Query($query, $this->getDI()))->execute();

                if(count($weekly) > 0) {
                    $html .= "<ol class='calendar'>";
                    $arrServices = [];
                    $t = 0;

                    foreach($weekly as $index => $wee) {
                        $service = Services::findFirst("ser_date = '" . $date1[2] . "-". $date1[0] ."-". $day . "' and con_id = '" . $wee->id . "'");
                        $status = $service ? $service->ser_status : "NOT EXECUTED";
                        $date2 = explode("-", $wee->con_date);
                        $compare = $this->funciones->compararFechas($day . "/" . $date1[0] . "/" . $date1[2], $date2[2] . "/" . $date2[1] . "/" . $date2[0]);
                        $print = $compare >= 0 && $compare % $wee->con_frequency == 0 ? 1 : 0;

                        if($print == 1 && $status == "NOT EXECUTED") {
                            $arrServices[$t]["client"] = $wee->cli_name;
                            $arrServices[$t]["position"] = $service && $service->ser_position != null ? (int) $service->ser_position : $t;
                            $t++;
                        }
                    }

                    $array = $this->funciones->burbuja($arrServices, "position");

                    foreach($array as $index => $service)
                        $html .= "<li>" . $service["client"] . "</li>";    

                    $html .= "</ol>";
                }

                $parametro[$weeDay] = "<p>" . $date1[0] . "/" . $day . "/" . $date1[2] . "</p>" . $html;

                if($day < $lasDay)
                    $day++;
                else {
                    $day = 1;
                    $date1[0]++;
                    $date1[0] = strlen($date1[0]) == 1 ? "0" . $date1[0] : $date1[0];
                }

                $weekday++;
            }

            $this->view->setVar("weekly", $parametro);
        }
    }

    // Reporte de servicios semanales en formato pdf
    public function weekly_reportAction() {
        $this->view->disable();
        $this->view->pick("reports/weekly/weekly_report");

        $weeYear = explode(" to ", $this->request->get("week"));
        $date1 = explode("/", $weeYear[0]);
        $day = $date1[1];
        $lasDay = 28;
        $weekday = 0; // Domingo se representa con el número 6   

        while(checkdate($date1[0], $lasDay + 1, $date1[2]))
            $lasDay++;

        while($weekday <= 6) {
            $html = ""; 
            $day = strlen($day) == 1 ? "0" . $day : $day;

            switch($weekday) {
                case 1:
                    $weeDay = 0;
                    break;
                case 2:
                    $weeDay = 1;
                    break;
                case 3:
                    $weeDay = 2;
                    break;
                case 4:
                    $weeDay = 3;
                    break;
                case 5:
                    $weeDay = 4;
                    break;
                case 6:
                    $weeDay = 5;
                    break;
                default:
                    $weeDay = 6;
            }

            $query = "SELECT
                c.cli_name, 
                WEEKDAY(o.con_date) AS weekday,
                o.id,
                o.con_date,
                o.con_frequency
                FROM
                Clients AS c,
                Contracts AS o,
                Groups AS g
                WHERE
                o.cli_id = c.id
                AND 
                o.con_status = 'ENABLED'
                AND
                o.gro_id = g.id
                AND
                WEEKDAY(o.con_date) = '" . $weeDay . "' 
                ORDER BY 
                o.gro_id, 
                o.con_date, 
                c.id";

            $weekly = (new Query($query, $this->getDI()))->execute();

            if(count($weekly) > 0) {
                $arrServices = [];
                $t = 0;

                foreach($weekly as $index => $wee) {
                    $service = Services::findFirst("ser_date = '" . $date1[2] . "-". $date1[0] ."-". $day . "' and con_id = '" . $wee->id . "'");
                    $status = $service ? $service->ser_status : "NOT EXECUTED";
                    $date2 = explode("-", $wee->con_date);
                    $compare = $this->funciones->compararFechas($day . "/" . $date1[0] . "/" . $date1[2], $date2[2] . "/" . $date2[1] . "/" . $date2[0]);
                    $print = $compare >= 0 && $compare % $wee->con_frequency == 0 ? 1 : 0;

                    if($print == 1 && $status == "NOT EXECUTED") {
                        $arrServices[$t]["client"] = $wee->cli_name;
                        $arrServices[$t]["position"] = $service && $service->ser_position != null ? (int) $service->ser_position : $t;
                        $t++;
                    }
                }

                $array = $this->funciones->burbuja($arrServices, "position");
                
                foreach($array as $index => $service)
                    $html .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . ($index + 1) . ". " . $service["client"] . "<br/>";
            }

            $parametro[$weeDay] = $date1[0] . "/" . $day . "/" . $date1[2] . "<br/><br/>" . $html . "</br>&nbsp;";

            if($day < $lasDay)
                $day++;
            else {
                $day = 1;
                $date1[0]++;
                $date1[0] = strlen($date1[0]) == 1 ? "0" . $date1[0] : $date1[0];
            }

            $weekday++;
        }

        $html2 = $this->view->getRender("reports", "weekly_report", array(
            "weekly" => $parametro,
            "week" => $this->request->get("week")
        )); 

        $this->mpdf->WriteHTML($html2);
        $this->mpdf->Output();
        
        exit;
    }

    // ******** REPORTE SEMANAL DE SERVICIOS (FIN) ************************ //


    // ******** REPORTE MENSUAL DE SERVICIOS (INICIO) ************************ //

    // Reporte mensual de servicios
    public function monthlyAction() {
        $this->view->setTemplateAfter("main");
        $this->view->pick("reports/monthly/consult");        
        $this->getassets();

        $this->assets->addCss("assets/css/datepicker.css")
            ->addCss("assets/plugins/bootstrap-datepicker/weekpicker.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.css");

        $this->assets
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/reports/monthly.js");
    }

    // Obtención de la lista de servicios del mes
    public function getmonthlyAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->pick("reports/monthly/getmonthly");  
   
        if($this->request->isPost()) {
            $dia = 1; // Contador de días
            $diames = 1; // Día del mes
            $html = ""; // Variable para almacenar resultados
            $j = 1; // Contador de semanas
            $month = explode("/", $this->request->getPost("month"));
            $numero_primer_dia = date("w", mktime(0, 0, 0, $month[0], $dia, $month[1]));
            $ultimo_dia = 28;
            $flag = false;

            while(checkdate($month[0], $ultimo_dia + 1, $month[1]))
                $ultimo_dia++;

            $total_dias = $numero_primer_dia + $ultimo_dia;

            while($j < $total_dias) {
                $html .= "<tr> \n";
                $i = 0;
                $weekday = 6; // Domingo se representa con el número 6

                while($i < 7) {
                    if($weekday == 5) 
                        $weekday = 0; // Lunes se representa con el número 0

                    if($j <= $numero_primer_dia) {
                        $html .= " <td></td> \n";
                        $weekday == 6 ? $weekday-- : $weekday++;
                    } elseif($diames > $ultimo_dia) {
                        $html .= " <td></td> \n";
                        $weekday == 6 ? $weekday-- : $weekday++;
                    } else { 
                        $diames_con_cero = $diames < 10 ? "0" . $diames : $diames;
                        $html .= " <td><div class='text-right' style='font-size: 14px !important'>" . $diames . "</div>";

                        $query = "SELECT
                            c.cli_name,
                            WEEKDAY(o.con_date) AS weekday,
                            o.id,
                            o.con_date,
                            o.con_frequency
                            FROM
                            Clients AS c,
                            Contracts AS o
                            WHERE
                            o.cli_id = c.id
                            AND 
                            o.con_status = 'ENABLED'
                            AND
                            WEEKDAY(o.con_date) = '" . $weekday . "'
                            ORDER BY
                            o.gro_id,
                            o.con_date,
                            c.id";

                        $weekly = (new Query($query, $this->getDI()))->execute();

                        if(count($weekly) > 0) {
                            $html .= "<ol class='calendar'>";
                            $arrServices = [];
                            
                            $t = 0;
                            foreach($weekly as $index => $wee) {
                                $service = Services::findFirst("ser_date = '" . $month[1] . "-". $month[0] ."-". $diames_con_cero . "' and con_id = '" . $wee->id . "'");
                                $estatus = $service ? $service->ser_status : "NOT EXECUTED";
                                $date = explode("-", $wee->con_date);
                                $compare = $this->funciones->compararFechas($diames_con_cero . "/" . $month[0] . "/" . $month[1], $date[2] . "/" . $date[1] . "/" . $date[0]);
                                $print = $compare >= 0 && $compare % $wee->con_frequency == 0 ? 1 : 0;

                                if($print == 1 && $estatus == "NOT EXECUTED") {
                                    $arrServices[$t]["client"] = $wee->cli_name;
                                    $arrServices[$t]["position"] = $service && $service->ser_position != null ? $service->ser_position : $t;
                                    $flag = true;
                                    $t++;
                                }
                            }

                            $array = $this->funciones->burbuja($arrServices, "position");

                            foreach($array as $index => $service)
                                $html .= "<li>" . $service["client"] . "</li>";

                            $html .= "</ol>";
                        }

                        $diames++;
                        $weekday == 6 ? $weekday-- : $weekday++;
                        $html .= "</td> \n";
                    }

                    $i++;
                    $j++;
                }

                $html .= "</tr> \n";
            }

            $flag ? $this->view->setVar("weekly", $html) : $this->view->disable();
        }
    }

    // Reporte se servicios mensuales en formato pdf
    public function monthly_reportAction() {
        $this->view->disable();
        $this->view->pick("reports/monthly/monthly_report");

        $dia = 1; // Contador de días
        $diames = 1; // Día del mes
        $html = ""; // Variable para almacenar resultados
        $j = 1; // Contador de semanas
        $month = explode("/", $this->request->get("month"));
        $numero_primer_dia = date("w", mktime(0, 0, 0, $month[0], $dia, $month[1]));
        $ultimo_dia = 28;
        $flag = false;

        while(checkdate($month[0], $ultimo_dia + 1, $month[1]))
            $ultimo_dia++;

        $total_dias = $numero_primer_dia + $ultimo_dia;

        while($j < $total_dias) {
            $html .= "<tr> \n";
            $i = 0;
            $weekday = 6; // Domingo se representa con el número 6

            while($i < 7) {
                if($weekday == 5) 
                    $weekday = 0; // Lunes se representa con el número 0

                if($j <= $numero_primer_dia) {
                    $html .= " <td></td> \n";
                    $weekday == 6 ? $weekday-- : $weekday++;
                } elseif($diames > $ultimo_dia) {
                    $html .= " <td></td> \n";
                    $weekday == 6 ? $weekday-- : $weekday++;
                } else { 
                    $diames_con_cero = $diames < 10 ? "0" . $diames : $diames;
                    $html .= " <td class='services'><span style='font-size: 9pt !important'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;" . $diames . "</span></div>";

                    $query = "SELECT
                        c.cli_name, 
                        WEEKDAY(o.con_date) AS weekday,
                        o.id,
                        o.con_date,
                        o.con_frequency
                        FROM
                        Clients AS c,
                        Contracts AS o
                        WHERE
                        o.cli_id = c.id
                        AND 
                        o.con_status = 'ENABLED'
                        AND
                        WEEKDAY(o.con_date) = '" . $weekday . "'
                        ORDER BY
                        o.gro_id,
                        o.con_date,
                        c.id";

                    $weekly = (new Query($query, $this->getDI()))->execute();

                    if(count($weekly) > 0) {
                        $html .= "<ol>";
                        $arrServices = [];
                        $t = 0;

                        foreach($weekly as $index => $wee) {
                            $service = Services::findFirst("ser_date = '" . $month[1] . "-". $month[0] ."-". $diames_con_cero . "' and con_id = '" . $wee->id . "'");
                            $estatus = $service ? $service->ser_status : "NOT EXECUTED";
                            $date = explode("-", $wee->con_date);
                            $compare = $this->funciones->compararFechas($diames_con_cero . "/" . $month[0] . "/" . $month[1], $date[2] . "/" . $date[1] . "/" . $date[0]);
                            $print = $compare >= 0 && $compare % $wee->con_frequency == 0 ? 1 : 0;

                            if($print == 1 && $estatus == "NOT EXECUTED") {
                                $arrServices[$t]["client"] = $wee->cli_name;
                                $arrServices[$t]["position"] = $service && $service->ser_position != null ? $service->ser_position : $t;
                                $flag = true;
                                $t++;
                            }
                        }

                        $array = $this->funciones->burbuja($arrServices, "position");

                        foreach($array as $index => $service) {
                            $html .= "<li>" . $service["client"] . "</li>";

                        }

                        $html .= "</ol>";
                    }

                    $diames++;
                    $weekday == 6 ? $weekday-- : $weekday++;
                    $html .= "</td> \n";
                }

                $i++;
                $j++;
            }

            $html .= "</tr> \n";
        }

        $html2 = $this->view->getRender("reports", "monthly_report", array(
            "monthly" => $html,
            "month" => $month[0],
            "year" => $month[1]
        )); 

        $this->mpdf->WriteHTML($html2);
        $this->mpdf->Output();
        
        exit;
    }

    // ******** REPORTE MENSUAL DE SERVICIOS (fin) ************************ //


    // ******** REPORTE DE CLIENTES CON PAGOS PENDIENTES (INICIO) ************************ //

    // Query para clientes con pagos pendientes
    private function _getpendingbd($since, $until) {
        $arrBalance = array();
        $connection = new Mysql($this->config->database->toArray());

        // Lista de pagos pendientes
        $query = "SELECT
            c.id AS client,
            c.cli_code,
            c.cli_name,
            c.cli_mob_phone,
            o.id AS contract,
            o.con_service,
            o.con_pay,
            CASE WHEN
                c.id
                NOT IN (SELECT
                c.id
                FROM
                pla_clients AS c,
                pla_contracts AS o,
                pla_payments AS pa,
                pla_services AS s
                WHERE
                pa.ser_id = s.id
                AND
                s.con_id = o.id
                AND
                o.cli_id = c.id)
                THEN 0
                ELSE
                    (SELECT
                    max(p.pay_date)
                    FROM
                    pla_clients AS c,
                    pla_contracts AS o,
                    pla_payments AS p,
                    pla_services AS s
                WHERE
                o.cli_id = c.id
                AND
                o.id = s.con_id
                AND 
                s.id = p.ser_id 
                AND
                c.id = client)
            END AS pay_date
            FROM
            pla_clients AS c,
            pla_contracts AS o,
            pla_services AS s
            WHERE
            c.id = o.cli_id
            AND
            s.con_id = o.id
            AND
            s.ser_status = 'EXECUTED'
            AND
            s.ser_date
            BETWEEN
            '" . $this->funciones->cambiaf_a_sql($since) . "'
            AND
            '" . $this->funciones->cambiaf_a_sql($until) . "'
            GROUP BY
            c.id";

        $pending = $connection->fetchAll($query, Phalcon\Db::FETCH_ASSOC);

        // ******************************** //

        foreach($pending as $pen) {
            $services = Services::find("con_id = '" . $pen["contract"] . "' and ser_status = 'EXECUTED'");
            $balance = 0;

            foreach($services as $service) {
                $values = (new ServicesController())->getvalues($service->id, $pen["con_pay"], "ALL", false); //$i++;
                $balance = $balance + $values["balance"];
            }
            
            $arrBalance[] = $balance;
        }

        $parametro["pending"] = $pending;
        $parametro["arrBalance"] = $arrBalance;

        return $parametro;
    }

    // Reporte de clientes con pagos pendientes
    public function pendingAction() {
        $this->view->setTemplateAfter("main");
        $this->view->pick("reports/pending/consult");        
        $this->getassets();

        $this->assets->addCss("assets/css/datepicker.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/reports/pending.js");
    }

    // Obtención de la lista de pagos pendientes
    public function getpendingAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->pick("reports/pending/getpending");

        if($this->request->isPost()) {
            $pending = $this->_getpendingbd($this->request->getPost("since"), $this->request->getPost("until"));

            count($pending["pending"]) > 0 ? $this->view->setVar("pending", $pending["pending"]) : $this->view->disable();
            
            $this->view->setVar("arrBalance", $pending["arrBalance"]);
        }
    }

    // Reporte de pagos pendientes en formato pdf
    public function pending_paymentsAction() {
        $this->view->disable();
        $this->view->pick("reports/pending/pending_payments");        
        $pending = $this->_getpendingbd($this->request->get("since"), $this->request->get("until"));

        $html = $this->view->getRender("reports", "pending_payments", array(
            "pending" => $pending["pending"],
            "arrBalance" => $pending["arrBalance"]
        )); 

        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

    // ******** REPORTE DE CLIENTES CON PAGOS PENDIENTES (FIN) ************************ //


    // ******** REPORTE DE GASTOS   (INICIO) ************************ //

    // Query para gastos
    private function _getexpensesbd($since, $until, $concept) {
        // Lista de gastos emitidos
        $query = "SELECT
            e.exp_description,
            e.exp_date,
            (e.exp_quantity * e.exp_cost) AS total,
            c.con_name
            FROM
            Expenses AS e,
            ExpConcepts AS c
            WHERE
            e.con_id = c.id
            AND
            e.exp_status = 'ENABLED'
            AND
            e.exp_date
            BETWEEN
            '" . $this->funciones->cambiaf_a_sql($since) . "'
            AND
            '" . $this->funciones->cambiaf_a_sql($until) . "'";

        if(!empty($concept)) {
            $query .= " AND
            c.id = '" . $concept . "'";
        }

        $expenses = (new Query($query, $this->getDI()))->execute();

        // ******************************** //

        return $expenses;
    }

    // Reporte de gastos
    public function expensesAction() {
        $this->view->setTemplateAfter("main");
        $this->view->pick("reports/expenses/consult");        
        $this->getassets();

        $this->assets->addCss("assets/css/datepicker.css")
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css")
            ->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/reports/expenses.js");

        // Lista de partidas o conceptos de gastos
        $concepts = ExpConcepts::find(array("order" => "con_name"));
        $this->view->setVar("concepts", $concepts);
    }

    // Obtención de la lista de gastos emitidos
    public function getexpensesAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->pick("reports/expenses/getexpenses");

        if($this->request->isPost()) {
            $expenses = $this->_getexpensesbd($this->request->getPost("since"), $this->request->getPost("until"), $this->request->getPost("concept"));
            count($expenses) > 0 ? $this->view->setVar("expenses", $expenses) : $this->view->disable();            
        }
    }

    // Reporte de gastos emitidos en formato pdf
    public function issued_expensesAction() {
        $this->view->disable();
        $this->view->pick("reports/expenses/issued_expenses");        
        $expenses = $this->_getexpensesbd($this->request->get("since"), $this->request->get("until"), $this->request->get("concept"));

        $html = $this->view->getRender("reports", "issued_expenses", array(
            "expenses" => $expenses
        )); 

        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
        
        exit;
    }

}