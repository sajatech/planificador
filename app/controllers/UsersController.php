<?php

use Phalcon\Mvc\Model\Query;

class UsersController extends ControllerBase { 

    // Vista de registro de usuarios
    public function registrationAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/users/registration.js");

        // Lista de roles de usuario
        $roles = Roles::find(array("order" => "rol_name"));
        $this->view->setVar("roles", $roles);
    }

    // Registro de usuarios
    public function registrationbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $code = Users::findFirst("upper(use_code) = '" . $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("useCode"))) . "'");
            $login = Users::findFirstByUseLogin($this->request->getPost("username"));

            if(!$code && !$login) {
                // Datos del usuario
                $user = new Users();

                $user->setUseCode($this->funciones->squish($this->request->getPost("useCode")));
                $user->setUseName($this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("name"))));
                $user->setUsePhone($this->request->getPost("phone"));
                $user->setUseEmail($this->funciones->normaliza($this->request->getPost("email")));
                $user->setUseLogin($this->funciones->normaliza($this->request->getPost("username")));
                $user->setUsePassword(md5($this->request->getPost("password")));
                $user->setUseStatus($this->request->get("status"));
                $user->setRolId($this->request->getPost("useRole"));

                if(!$user->save()) {
                    foreach($user->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else {
                if($code)
                    $errores[] = "Code already exists.";
                else if($login)
                    $errores[] = "Username already exists.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "User " . $user->getUseName() . " registered correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de usuarios registrados
    public function listAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/users/list.js");
    }

    // Obtención de la lista de usuarios
    public function getusersAction() {
        $this->view->disable();

        $users = new Query("SELECT 
            u.id,
            u.use_code,
            u.use_name,
            u.use_email,
            u.use_login,
            u.use_status,
            r.rol_name
            FROM
            Users u,
            Roles r
            WHERE
            u.rol_id = r.id
            ORDER BY
            u.id 
            DESC", $this->getDI());

        $users = $users->execute();
        $arrUsers = array();

        foreach($users as $clave => $user) {
            $label = $user->use_status == "ENABLED" ? "label-success" : "label-danger";
            $arrUsers["aaData"][$clave]["useCode"] = $user->use_code;
            $arrUsers["aaData"][$clave]["name"] = $user->use_name;
            $arrUsers["aaData"][$clave]["email"] = $user->use_email;
            $arrUsers["aaData"][$clave]["useRole"] = $user->rol_name;
            $arrUsers["aaData"][$clave]["username"] = $user->use_login;
            $arrUsers["aaData"][$clave]["status"] = "<span class='label " . $label . "'>" . $user->use_status . "</span>";
        }

        //$arrUsuarios["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 6);

        return json_encode($arrUsers);
    }

    // Vista de actualización de usuarios
    public function updateAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        if($this->request->isPost()) {
            $this->assets->addCss("assets/plugins/form-select2/select2.css");
            
            $this->assets
                ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
                ->addJs("assets/plugins/form-select2/select2.min.js")
                ->addJs("assets/plugins/form-parsley/parsley.js")
                ->addJs("js/users/update.js");

            // Datos del usuario
            $user = Users::findFirstByUseCode($this->request->getPost("code"));

            if($user) {
                $this->view->setVar("user", $user);
                
                // Lista de roles de usuario
                $roles = Roles::find(array("order" => "rol_name"));
                $this->view->setVar("roles", $roles);
            } else
                $this->response->redirect("users/list");
        } else {
            $this->response->redirect("users/list");
            return false;
        }
    }

    // Actualización de usuarios en la base de datos
    public function updatebdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $user = Users::findFirstById($this->request->get("id"));
            $errores = array();

            if($user) {
                $code = Users::findFirst("upper(use_code) = '" . $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("useCode"))) . "' and id != '" . $this->request->get("id") . "'");
                $username = Users::findFirst("use_login = '" . $this->funciones->normaliza($this->request->getPost("username")) . "' and id != '" . $this->request->get("id") . "'");

                if(!$code && !$username) {
                    // Datos del usuario
                    $user->use_code = $this->request->getPost("useCode");
                    $user->use_name = $this->funciones->strtoupper_utf8($this->request->getPost("name"));
                    $user->use_email = $this->funciones->normaliza($this->request->getPost("email"));
                    $user->use_phone = $this->request->getPost("phone");
                    $user->use_login = $this->funciones->normaliza($this->request->getPost("username"));
                    $user->rol_id = $this->request->getPost("useRole");

                    if(!empty($this->request->getPost("password")))
                        $user->use_password = md5($this->request->getPost("password"));

                    // ******************************** //

                    if(!$user->update()) {
                        foreach($user->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                } else {
                    if($code)
                        $errores[] = "Code already exists.";
                    else if($username)
                        $errores[] = "Username already exists.";
                }
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "User " . $user->use_name . " updated correctly.";
                $parametros["type"] = "success";
                $parametros["code"] = $user->use_code;
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de usuarios
    public function disuserAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $user = Users::findFirstByUseCode($this->request->getPost("code"));

            if($user) {
                $user->use_status = "DISABLED";

                if(!$user->update()) {
                    foreach($user->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no user.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "User with code " . $user->use_code . " disabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de usuarios
    public function enauserAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $user = Users::findFirstByUseCode($this->request->getPost("code"));

            if($user) {
                $user->use_status = "ENABLED";

                if(!$user->update()) {
                    foreach($user->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no user.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "User with code " . $user->use_code . " enabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}