<?php

class ConfigurationController extends ControllerBase { 

    // Vista de grupos de trabajo registrados
    public function groupsAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/configuration/groups.js");
    }

    // Almacenamiento del grupo en la base de datos
    public function reggroupAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $name = Groups::findFirstByGroName($this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("name"))));

            if(!$name) {
                // Datos del grupo de trabajo
                $group = new Groups();

                $group->setGroName($this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("name"))));
                $group->setGroLeader($this->funciones->strtoupper_utf8($this->request->getPost("leader")));
                $group->setUseId($this->funciones->getUsuario());

                if(!$group->save()) {
                    foreach($group->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            } else
                $errores[] = "There is already a work group with this name.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Work group registered correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Actualización de grupos de trabajo en la base de datos
    public function updgroupAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $group = Groups::findFirstById($this->request->get("code"));
            $errores = array();

            if($group) {
                $name = Groups::findFirst("gro_name = '" . $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("name"))) . "' and id != '" . $group->id . "'");
                
                if(!$name) {
                    // Datos del grupo
                    $group->gro_name = $this->funciones->strtoupper_utf8($this->funciones->squish($this->request->getPost("name")));
                    $group->gro_leader = $this->funciones->strtoupper_utf8($this->request->getPost("leader"));

                    if(!$group->update()) {
                        foreach($group->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                } else
                    $errores[] = "There is already a work group with this name.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Work group updated correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de grupos
    public function disgroupAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $group = Groups::findFirstById($this->request->getPost("code"));

            if($group) {
                $group->gro_status = "DISABLED";

                if(!$group->update()) {
                    foreach($group->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no work group.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Work group " . $group->gro_name . " disabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de grupos
    public function enagroupAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $group = Groups::findFirstById($this->request->getPost("code"));

            if($group) {
                $group->gro_status = "ENABLED";

                if(!$group->update()) {
                    foreach($group->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no work group.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Work group " . $group->gro_name . " enabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de conceptos de gastos registrados
    public function expensesAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css");

        $this->assets
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("js/configuration/expenses.js");
    }

    // Almacenamiento del concepto en la base de datos
    public function regexpenseAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $expConcepts = ExpConcepts::findFirst("upper(con_name) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("name"))) . "'");

            if(!$expConcepts) {
                // Datos del concepto de gastos
                $expConcepts = new ExpConcepts();
                $expConcepts->setConName($this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("name"))));
                $expConcepts->setUseId($this->funciones->getUsuario());

                if(!$expConcepts->save()) {
                    foreach($expConcepts->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            } else
                $errores[] = "There is already a expense concept with this name.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense concept " . $expConcepts->getConName() .  " registered correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Actualización de conceptos de gastos en la base de datos
    public function updexpenseAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $expConcepts = ExpConcepts::findFirstById($this->request->get("code"));
            $errores = array();

            if($expConcepts) {
                $name = ExpConcepts::findFirst("upper(con_name) = '" . $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("name"))) . "' and id != '" . $this->request->get("code") . "'");

                if(!$name) {
                    // Datos del concepto de gastos
                    $expConcepts->con_name = $this->funciones->strtoupper_utf8($this->funciones->fun_eliminarDobleEspacios($this->request->getPost("name")));
                    $expConcepts->use_id = $this->funciones->getUsuario();

                    if(!$expConcepts->update()) {
                        foreach($expConcepts->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }

                    // ******************************** //
                } else
                    $errores[] = "There is already a expense concept with this name.";
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense concept " . $expConcepts->con_name .  " updated correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Desactivación de conceptos de gastos
    public function disexpenseAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $expConcept = ExpConcepts::findFirstById($this->request->getPost("code"));

            if($expConcept) {
                $expConcept->con_status = "DISABLED";

                if(!$expConcept->update()) {
                    foreach($expConcept->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no expense concept.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense concep " . $expConcept->con_name . " disabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de conceptos de gastos
    public function enaexpenseAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $expConcept = ExpConcepts::findFirstById($this->request->getPost("code"));

            if($expConcept) {
                $expConcept->con_status = "ENABLED";

                if(!$expConcept->update()) {
                    foreach($expConcept->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no expense concept.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Expense concept " . $expConcept->con_name . " enabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

}