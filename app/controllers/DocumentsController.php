<?php

use Phalcon\Mvc\Model\Query;

class DocumentsController extends ControllerBase { 

    // Obtención de la lista de documentos
    public function getdocumentsAction() {
        $this->view->disable();
        $documents = Documents::find(array("order" => "id desc", "group" => "doc_number, doc_status"));
        $arrDocuments = array();

        foreach($documents as $clave => $document) {            
            // Datos del documento
            $query = "SELECT
                l.cli_name,
                l.cli_code,
                c.id,
                d.id AS doc_id,
                d.doc_number,
                d.doc_invoice,
                d.doc_amount,
                d.doc_status
                FROM
                Documents AS d,
                Services AS s,
                Contracts AS c,
                Clients AS l
                WHERE
                d.ser_id = s.id
                AND
                s.con_id = c.id
                AND
                c.cli_id = l.id
                AND
                d.id = '" . $document->id . "'";

            $invoice = (new Query($query, $this->getDI()))->execute();

            // ******************************** //

            $arrDocuments["aaData"][$clave]["amount"] = "$ " . $this->funciones->number_format($invoice[0]->doc_amount);
            $arrDocuments["aaData"][$clave]["client"] = $invoice[0]->cli_name . " (" . $invoice[0]->cli_code . ")";
            $arrDocuments["aaData"][$clave]["conCode"] = $invoice[0]->id;
            $arrDocuments["aaData"][$clave]["docId"] = $invoice[0]->doc_id;
            $arrDocuments["aaData"][$clave]["docNumber"] = $this->funciones->str_pad($invoice[0]->doc_number);
            $arrDocuments["aaData"][$clave]["docType"] = $invoice[0]->doc_invoice == 1 ? "INVOICE" : "RECEIPT";
            $arrDocuments["aaData"][$clave]["status"] = "<span class='label " . ($invoice[0]->doc_status == "ENABLED" ? "label-success" : "label-danger") . "'>" . $invoice[0]->doc_status . "</span>";
        }

        //$arrArticulos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 1);

        return json_encode($arrDocuments);
    }

    // Desactivación de documentos
    public function disdocumentAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $documents = Documents::find("doc_number = '" . $this->request->getPost("code") . "'");

            if(count($documents) > 0) {
                if(!$documents->update(array("doc_status" => "DISABLED"))) {
                    foreach($documents->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no document.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Document with number # " . $this->request->getPost("code") . " disabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Activación de documentos
    /*public function enadocumentAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $document = Documents::findFirstById($this->request->getPost("code"));

            if($document) {
                $document->doc_status = "ENABLED";

                if(!$document->update()) {
                    foreach($document->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no document.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Document with code # " . $this->request->getPost("code") . " enabled correctly.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }*/

}