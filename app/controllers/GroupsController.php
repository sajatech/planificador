<?php

class GroupsController extends ControllerBase { 

    // Obtención de la lista de grupos de trabajo
    public function getgroupsAction() {
        $this->view->disable();
        $groups = Groups::find(array("order" => "id desc"));
        $arrGroups = array();

        foreach($groups as $clave => $group) {
            $label = $group->gro_status == "ENABLED" ? "label-success" : "label-danger";
            $user = Users::findFirstById($group->use_id);
            $arrGroups["aaData"][$clave]["groCode"] = $this->funciones->str_pad($group->id);
            $arrGroups["aaData"][$clave]["name"] = $group->gro_name;
            $arrGroups["aaData"][$clave]["leader"] = $group->gro_leader;
            $arrGroups["aaData"][$clave]["regBy"] = $user->use_name . " (" . $user->use_code . ")";
            $arrGroups["aaData"][$clave]["status"] = "<span class='label " . $label . "'>" . $group->gro_status . "</span>";
        }

        //$arrArticulos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 1);

        return json_encode($arrGroups);
    }

}