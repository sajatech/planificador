<?php

class AdditionalController extends ControllerBase {

    // Almacenamiento del servicio adicional en la base de datos
    public function registrationbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $service = Services::findFirst(array("ser_date = '" . $this->request->get("serDate") . "' and con_id = '" . $this->request->get("contract") . "'"));

            if(!$service) {
                // Datos del servicio principal
                $service = new Services();

                $service->setSerDate($this->request->get("serDate"));
                $service->setConId($this->request->get("contract"));
                $service->setUseId($this->funciones->getUsuario());

                if(!$service->save()) {
                    foreach($service->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            }

            // Datos del servicio adicional
            $additional = new Additional();

            $additional->setAddName($this->funciones->strtoupper_utf8($this->request->getPost("addiService")));
            $additional->setAddAmount($this->funciones->cambiam_a_numeric($this->request->getPost("inpAmount1")));
            $additional->setSerId($service->getId());
            $additional->setUseId($this->funciones->getUsuario());

            if(!$additional->save()) {
                foreach($additional->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            // ******************************** //

            // Datos del contrato
            $contract = Contracts::findFirstById($this->request->get("contract"));

            // Obtención de los valores adeudados y pagados
            $values = (new ServicesController())->getvalues($service->getId(), $contract->con_pay, "ALL", false);

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Additional service to " . $contract->con_service . " registered correctly.";
                $parametros["type"] = "success";
                $parametros["totAdditional"] = $this->funciones->number_format($values["additional"]);
                $parametros["amount"] = $this->funciones->number_format($values["amount"]);
                $parametros["balance"] = $this->funciones->number_format($values["balance"]);
                $parametros["service"] = $service->getId();
            }

            echo json_encode($parametros);
        }
    }

    // Obtención de la lista de servicios adicionales
    public function getadditionalAction() {
        $this->view->disable();
        $service = Services::findFirstById($this->request->get("service"));
        $arrAdditional = array();

        if($service) {
            $additional = Additional::find(array("ser_id = '" . $service->id . "'", "order" => "add_cre_date desc"));

            foreach($additional as $clave => $add) {
                $arrAdditional["aaData"][$clave]["index"] = $clave + 1;
                $arrAdditional["aaData"][$clave]["addService"] = $add->add_name;
                $arrAdditional["aaData"][$clave]["amount"] = "$ " . $this->funciones->number_format($add->add_amount);
                $arrAdditional["aaData"][$clave]["creDate"] = $this->funciones->cambiaf_a_normal($this->funciones->getdate($add->add_cre_date));
                $arrAdditional["aaData"][$clave]["actions"] = "<span><a class='btn btn-md btn-inverse btn-label tooltips pr8' onclick='delAdditional($add->id)' data-trigger='hover' data-original-title='Delete'><i class='fa fa-trash btn-label-custom'></i></a></span>";
            }
        }

        //$arrArticulos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 1);

        return json_encode($arrAdditional);
    }

    // Eliminación de servicios adicionales en la base de datos
    public function deladditionalAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $additional = Additional::findFirstById($this->request->getPost("codigo"));

            if($additional) {
                if(!$additional->delete()) {
                    foreach($additional->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no such additional service.";

            // Datos del servicio
            $service = Services::findFirstById($additional->ser_id);

            // Datos del contrato
            $contract = Contracts::findFirstById($service->con_id);

            // Obtención de los valores adeudados y pagados
            $values = (new ServicesController())->getvalues($service->id, $contract->con_pay, "ALL", false);

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = "Additional service to " . $contract->con_service . " deleted correctly.";
                $parametros["type"] = "success";
                $parametros["service"] = $additional->ser_id;
                $parametros["totAdditional"] = $this->funciones->number_format($values["additional"]);
                $parametros["amount"] = $this->funciones->number_format($values["amount"]);
                $parametros["balance"] = $this->funciones->number_format($values["balance"]);
            }

            echo json_encode($parametros);
        }
    }

}