<?php

class PaymentsController extends ControllerBase {

    // Almacenamiento del pago de servicio en la base de datos
    public function registrationbdAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $service = Services::findFirst(array("ser_date = '" . $this->request->get("serDate") . "' and con_id = '" . $this->request->get("contract") . "'"));

            if(!$service) {
                // Datos del servicio principal
                $service = new Services();

                $service->setSerDate($this->request->get("serDate"));
                $service->setConId($this->request->get("contract"));
                $service->setUseId($this->funciones->getUsuario());

                if(!$service->save()) {
                    foreach($service->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            }

            // Datos del pago
            $payment = new Payments();

            $payment->setPayAmount($this->funciones->cambiam_a_numeric($this->request->getPost("inpAmount2")));
            $payment->setSerId($service->id);
            $payment->setTypId($this->request->getPost("payType"));
            $payment->setUseId($this->funciones->getUsuario());

            // ******************************** //

            if(!$payment->save()) {
                foreach($payment->getMessages() as $mensaje)
                    $errores[] = $mensaje;
            }

            // Datos del contrato
            $contract = Contracts::findFirstById($this->request->get("contract"));

            // Obtención de los valores adeudados y pagados
            $values = (new ServicesController())->getvalues($service->getId(), $contract->con_pay, "ALL", false);
    
            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = $contract->con_service . " service payment registered correctly.";
                $parametros["type"] = "success";
                $parametros["balance"] = $this->funciones->number_format($values["balance"]);
                $parametros["totPayments"] = $this->funciones->number_format($values["payments"]);
                $parametros["status"] = round($values["payments"], 2) >= $values["amount"] ? "true" : "false";
                $parametros["service"] = $service->getId();                
            }

            echo json_encode($parametros);
        }
    }

    // Obtención de la lista de pagos
    public function getpaymentsAction() {
        $this->view->disable();
        $service = Services::findFirstById($this->request->get("service"));
        $arrPayments = array();
        
        if($service) {
            $payments = Payments::find(array("ser_id = '" . $service->id . "'", "order" => "pay_date desc"));

            foreach($payments as $clave => $payment) {
                // Tipo de pago
                $payType = Types::findFirstById($payment->typ_id);
                
                $arrPayments["aaData"][$clave]["index"] = $clave + 1;
                $arrPayments["aaData"][$clave]["payDate"] = $this->funciones->cambiaf_a_normal($this->funciones->getdate($payment->pay_date));
                $arrPayments["aaData"][$clave]["amount"] = "$ " . $this->funciones->number_format($payment->pay_amount);
                $arrPayments["aaData"][$clave]["payType"] = $payType->typ_name;
                $arrPayments["aaData"][$clave]["actions"] = "<span><a class='btn btn-md btn-inverse btn-label tooltips pr8' onclick='delPayment($payment->id)' data-trigger='hover' data-original-title='Delete'><i class='fa fa-trash btn-label-custom'></i></a></span>";
            }
        }

        //$arrArticulos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 1);

        return json_encode($arrPayments);
    }

    // Eliminación de pagos en la base de datos
    public function delpaymentAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $payment = Payments::findFirstById($this->request->getPost("codigo"));

            if($payment) {
                if(!$payment->delete()) {
                    foreach($payment->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else
                $errores[] = "There is no such payment.";

            // Datos del servicio
            $service = Services::findFirstById($payment->ser_id);

            // Datos del contrato
            $contract = Contracts::findFirstById($service->con_id);

            // Obtención de los valores adeudados y pagados
            $values = (new ServicesController())->getvalues($service->getId(), $contract->con_pay, "ALL", false);

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = $contract->con_service . " service payment deleted correctly.";
                $parametros["type"] = "success";
                $parametros["service"] = $payment->ser_id;
                $parametros["balance"] = $this->funciones->number_format($values["balance"]);
                $parametros["totPayments"] = $this->funciones->number_format($values["payments"]);
                $parametros["status"] = $values["paid"];
            }

            echo json_encode($parametros);
        }
    }

}