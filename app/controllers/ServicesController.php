<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;

class ServicesController extends ControllerBase { 

    // Llamada a la función de cálculo de valores
    public function getvalues($service, $pay, $status, $number) {
        return $this->_getvaluesbd($service, $pay, $status, $number);
    }

    // Cálculo de los valores adeudados y pagados
    private function _getvaluesbd($service, $pay, $status, $number) {
        $parametro["docStatus"] = "ENABLED";
        $conditions = "id = '" . $service . "'";

        if($status == "EXECUTED") {
            $conditions = "con_id = '" . $service . "' and ser_status = 'EXECUTED'";

            if($this->request->get("generation") == 1) {
                $services = $this->_getinvoicing($service, $this->request->get("select"));
                $flag = true;
            } else if($number) {
                $query = "SELECT
                    d.ser_id,
                    d.doc_status,
                    s.id,
                    s.ser_date,
                    c.con_day
                    FROM
                    Documents AS d,
                    Services AS s,
                    Contracts AS c
                    WHERE
                    d.ser_id = s.id
                    AND
                    s.con_id = c.id
                    AND
                    d.doc_status = 'ENABLED'
                    AND
                    d.doc_number = '" . $number . "'";

                $services = (new Query($query, $this->getDI()))->execute();
                $flag = true;
                $serCode = true;
                $parametro["docStatus"] = $services[0]->doc_status;
            }
        }

        if(!isset($flag))
            $services = Services::find($conditions);
        
        $totPayments = 0;
        $totAdditional = 0;

        if(count($services) > 0) {
            $pay = $pay * count($services);

            foreach($services as $service) {
                $serId = !isset($serCode) ? $service->id : $service->ser_id;

                // Total de los pagos recibidos
                $payments = Payments::sum(array("column" => "pay_amount", "conditions" => "ser_id= '" . $serId . "'"));
                $totPayments = $totPayments + $payments;

                // Total de los montos de servicios adicionales
                $additional = Additional::sum(array("column" => "add_amount", "conditions" => "ser_id= '" . $serId . "'"));
                $totAdditional = $totAdditional + $additional;
            }
        }

        // Total general
        $amount = $pay + $totAdditional;

        // Total del monto restante
        $balance = $amount - $totPayments;

        $parametro["payments"] = $totPayments;
        $parametro["additional"] = $totAdditional;
        $parametro["amount"] = $amount;
        $parametro["balance"] = $balance;
        $parametro["paid"] = round($balance, 2) <= 0 ? "true" : "false";
        $parametro["services"] = $services;

        return $parametro;
    }

    // Servicios por facturar
    private function _getinvoicing($service, $select) {
        $query = "SELECT
            s.id,
            s.ser_date,
            c.con_day,
            c.con_pay
            FROM
            Services AS s,
            Contracts As c
            WHERE 
            NOT EXISTS (SELECT 1
                FROM
                Documents AS d
                WHERE
                d.ser_id = s.id
                AND
                d.doc_status = 'ENABLED')
            AND
            s.con_id = c.id
            AND
            s.ser_status = 'EXECUTED'
            AND
            s.con_id = '" . $service . "'";

        if(!empty($select)) {
            $query .= " and (";

            foreach($select as $index => $sel) {
                $query .= "s.id = '" . $sel . "'";

                if(count($select) - 1 > $index)
                    $query .= " or ";
            }

            $query .= ")";
        }

        $query .= " ORDER BY
            s.ser_date";

        $services = (new Query($query, $this->getDI()))->execute();

        return $services;
    }

    public function getinvoicingAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->assets
            ->addCss("assets/plugins/iCheck/skins/minimal/red.css");

        $this->assets
            ->addJs("assets/plugins/iCheck/icheck.min.js");



        $invoicing = $this->_getinvoicing($this->request->get("contract"));

        foreach($invoicing as $index => $inv) {
            $values = $this->getvalues($inv->id, $inv->con_pay, "ALL", false);
            $arrInvoicing[$index]["invoicing"] = $inv;
            $arrInvoicing[$index]["additional"] = $values["additional"];
        }

        $this->view->setVar("arrInvoicing", $arrInvoicing);
        $this->view->setVar("contract", Contracts::findFirstById($this->request->get("contract")));
    }

    // Obtención de los servicios a ser mostrados en calendario
    public function geteventsAction() {
        $this->view->disable();
        $contracts = Contracts::find(array("order" => "gro_id, con_date, cli_id"));
        $arrContracts = array();
        $arrRanges = array();
        
        foreach($contracts as $index => $contract) {
            $client = Clients::findFirstById($contract->cli_id);
            $group = Groups::findFirstById($contract->gro_id);
            $maxService = Services::maximum(array("column" => "ser_date", "conditions" => "ser_status= 'EXECUTED' and con_id = '" . $contract->id . "'"));

            switch($contract->con_day) {
                case "Sunday":
                    $dow = 0;
                    break;
                case "Monday":
                    $dow = 1;
                    break;
                case "Tuesday":
                    $dow = 2;
                    break;
                case "Wednesday":
                    $dow = 3;
                    break;
                case "Thursday":
                    $dow = 4;
                    break;
                case "Friday":
                    $dow = 5;
                    break;
                case "Saturday":
                    $dow = 6;
                    break;
                default:
                    $dow = 0;
            }

            $arrRanges[0]["start"] = $contract->con_date;
            $arrRanges[0]["end"] = $contract->con_status == "DISABLED" ? $maxService : "9999-99-99"; 
            $arrContracts[$index]["cliCode"] = $client->cli_code;
            $arrContracts[$index]["conCode"] = (int) $contract->id;
            $arrContracts[$index]["client"] = $client->cli_name . " (" . $client->cli_code . ")";
            $arrContracts[$index]["service"] = $contract->con_service;
            $arrContracts[$index]["group"] = $group->gro_name;
            $arrContracts[$index]["day"] = $contract->con_day;
            $arrContracts[$index]["start"] = "08:00";
            $arrContracts[$index]["end"] = "17:00";
            $arrContracts[$index]["dow"] = [$dow];
            $arrContracts[$index]["backgroundColor"] = $contract->con_color;
            $arrContracts[$index]["frequency"] = (int) $contract->con_frequency;
            $arrContracts[$index]["ranges"] = $arrRanges;
            $arrContracts[$index]["conDate"] = $contract->con_date;
            $arrContracts[$index]["groCode"] = $group->id;
        }

        return json_encode($arrContracts);
    }

    // Obtención del estatus del servicio al día
    public function getstatusAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $service = Services::findFirst("ser_date = '" . $this->request->getPost("date") . "' and con_id = '" . $this->request->getPost("contract") . "'");

            if($service && $service->ser_position != null) {
                $parametros["status"] = $service->ser_status;
                $parametros["position"] = $service->ser_position;
            } else if($service) {
                $parametros["status"] = $service->ser_status;
            } else {
                $parametros["status"] = "NOT EXECUTED";
            }

            return json_encode($parametros);
        }
    }

    // Ejecución de servicios
    public function exeserviceAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $service = Services::findFirst(array("ser_date = '" . $this->request->getPost("serDate") . "' and con_id = '" . $this->request->getPost("contract") . "'"));

            if($service) {
                $service->ser_status = "EXECUTED";

                if(!$service->update()) {
                    foreach($service->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }
            } else {
                // Datos del servicio principal
                $service = new Services();

                $service->setSerDate($this->request->getPost("serDate"));
                $service->setSerStatus("EXECUTED");
                $service->setConId($this->request->getPost("contract"));
                $service->setUseId($this->funciones->getUsuario());

                if(!$service->save()) {
                    foreach($service->getMessages() as $mensaje)
                        $errores[] = $mensaje;
                }

                // ******************************** //
            }

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = Contracts::findFirstById($service->con_id)->con_service . " service has been executed.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // No ejecución de servicios
    public function noeserviceAction() {
        $this->view->disable();

        if($this->request->isPost()) {
            $errores = array();
            $service = Services::findFirst("ser_date = '" . $this->request->getPost("serDate") . "' and con_id = '" . $this->request->getPost("contract") . "'");

            if($service) {
                $document = Documents::findFirst("ser_id = '" . $service->id . "' and doc_status = 'ENABLED'");

                if(!$document) {
                    $service->ser_status = "NOT EXECUTED";

                    if(!$service->update()) {
                        foreach($service->getMessages() as $mensaje)
                            $errores[] = $mensaje;
                    }
                } else
                    $errores[] = "Service has associated documents.";
            } else
                $errores[] = "There is no service.";

            if(count($errores) > 0) {
                $parametros["text"] = implode("</br>", $errores);
                $parametros["type"] = "error";
            } else {
                $parametros["text"] = Contracts::findFirstById($service->con_id)->con_service . " service has been unexecuted.";
                $parametros["type"] = "success";
            }

            echo json_encode($parametros);
        }
    }

    // Vista de servicios chequeados
    public function listAction() {
        $this->view->setTemplateAfter("main");
        $this->getassets();

        $this->assets
            ->addCss("assets/plugins/datatables/dataTables.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/fixedHeader.bootstrap.min.css")
            ->addCss("assets/plugins/datatables/responsive.bootstrap.min.css")
            ->addCss("assets/plugins/form-select2/select2.css");

        $this->assets
            ->addJs("assets/js/jquery.price_format.min.js")
            ->addJs("assets/js/jquery.redirect.js")
            ->addJs("assets/js/number.format.js")
            ->addJs("assets/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("assets/plugins/datatables/TableTools.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.js")
            ->addJs("assets/plugins/datatables/dataTables.editor.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.bootstrap.js")
            ->addJs("assets/plugins/datatables/dataTables.fixedHeader.min.js")        
            ->addJs("assets/plugins/datatables/dataTables.responsive.min.js")
            ->addJs("assets/plugins/datatables/responsive.bootstrap.min.js")
            ->addJs("assets/plugins/form-parsley/parsley.js")
            ->addJs("assets/plugins/fullcalendar/moment.min.js")
            ->addJs("assets/plugins/notifIt/js/notifIt.min.js")
            ->addJs("assets/plugins/form-select2/select2.min.js")
            ->addJs("js/services/list.js");

        // Lista de tipos de pago
        $types = Types::find(array("order" => "typ_name"));
        $this->view->setVar("types", $types);
    }

    // Obtención de la lista de servicios
    public function getservicesAction() {
        $this->view->disable();

        if($this->request->get("status")) {
            $conditions[] = "ser_status = 'EXECUTED'";
            $conditions["group"] = "con_id";
            $flag = true;
        }

        $conditions["order"] = $this->request->get("order");
                        
        $services = Services::find($conditions);
        $arrServices = array();

        foreach($services as $clave => $service) {
            $label = $service->ser_status == "EXECUTED" ? "label-success" : "label-danger";

            // Datos del contrato
            $contract = Contracts::findFirstById($service->con_id);

            // Obtención de los valores adeudados y pagados
            $getvalues = isset($flag) ? $service->con_id : $service->id;
            $values = $this->getvalues($getvalues, $contract->con_pay, $this->request->get("status") ? "EXECUTED" : "ALL", false);

            // Documentos generados
            $query = "SELECT
                d.doc_number,
                d.ser_id
                FROM
                Documents AS d,
                Services AS s
                WHERE
                d.ser_id = s.id
                AND
                d.doc_status = 'ENABLED'
                AND
                s.con_id = '" . $service->con_id . "'
                GROUP by
                d.doc_number";

            $contracts = (new Query($query, $this->getDI()))->execute();

            // ******************************** //

            // Servicios por facturar
            $query = "SELECT COUNT(*) AS count
                FROM
                Services AS s
                WHERE 
                NOT EXISTS (SELECT 1
                    FROM
                    Documents AS d
                    WHERE
                    d.ser_id = s.id 
                    AND 
                    d.doc_status = 'ENABLED')
                AND
                s.con_id = '" . $service->con_id . "'
                AND
                s.ser_status = 'EXECUTED'";

            $serInvoicing = (new Query($query, $this->getDI()))->execute();

            $arrServices["aaData"][$clave]["amount"] = "$ " . $this->funciones->number_format($values["amount"]);
            $arrServices["aaData"][$clave]["balance"] = "$ " . $this->funciones->number_format($values["balance"]);
            $arrServices["aaData"][$clave]["cliCode"] = Clients::findFirstById($contract->cli_id)->cli_code;
            $arrServices["aaData"][$clave]["client"] = Clients::findFirstById($contract->cli_id)->cli_name;
            $arrServices["aaData"][$clave]["conCode"] = $contract->id;
            $arrServices["aaData"][$clave]["conDate"] = $contract->con_day . ", " . $this->funciones->cambiaf_a_normal($contract->con_date);
            $arrServices["aaData"][$clave]["docType"] = $contract->con_doc_type;
            $arrServices["aaData"][$clave]["endFormat"] = $service->ser_date;
            $arrServices["aaData"][$clave]["genDocuments"] = count($contracts);
            $arrServices["aaData"][$clave]["paid"] = "$ " . $this->funciones->number_format($values["payments"]);
            $arrServices["aaData"][$clave]["serCode"] = $service->id;
            $arrServices["aaData"][$clave]["serDate"] = $this->funciones->saber_dia($service->ser_date) . ", " . $this->funciones->cambiaf_a_normal($service->ser_date);
            $arrServices["aaData"][$clave]["serInvoicing"] = $serInvoicing[0]->count;
            $arrServices["aaData"][$clave]["serName"] = $contract->con_service;
            $arrServices["aaData"][$clave]["status"] = "<span class='label " . $label . "'>" . $service->ser_status . "</span>";
        }

        //$arrArticulos["privilegios"] = $this->getprivilegios($this->funciones->getRolUsuario(), 1);

        return json_encode($arrServices);
    }

}