<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller {

    public function initialize() {
        // ControllerBase
    }

    // Obtención de los assets principales
    public function getassets() {
        return $this->assets
            ->addJs("assets/js/jquery-1.10.2.min.js")
            ->addJs("assets/js/jqueryui-1.9.2.min.js")
            ->addJs("assets/js/bootstrap.min.js")
            ->addJs("assets/js/enquire.min.js")
            ->addJs("assets/js/application.js")
            ->addJs("assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js");
    }
   
}