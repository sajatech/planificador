<?php

class Groups extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $gro_name;

    /**
     *
     * @var string
     */
    protected $gro_leader;

    /**
     *
     * @var string
     */
    protected $gro_status;

    /**
     *
     * @var integer
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field gro_name
     *
     * @param string $gro_name
     * @return $this
     */
    public function setGroName($gro_name)
    {
        $this->gro_name = $gro_name;

        return $this;
    }

    /**
     * Method to set the value of field gro_leader
     *
     * @param string $gro_leader
     * @return $this
     */
    public function setGroLeader($gro_leader)
    {
        $this->gro_leader = $gro_leader;

        return $this;
    }

    /**
     * Method to set the value of field gro_status
     *
     * @param string $gro_status
     * @return $this
     */
    public function setGroStatus($gro_status)
    {
        $this->gro_status = $gro_status;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field gro_name
     *
     * @return string
     */
    public function getGroName()
    {
        return $this->gro_name;
    }

    /**
     * Returns the value of field gro_leader
     *
     * @return string
     */
    public function getGroLeader()
    {
        return $this->gro_leader;
    }

    /**
     * Returns the value of field gro_status
     *
     * @return string
     */
    public function getGroStatus()
    {
        return $this->gro_status;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Pla_contracts', 'gro_id', array('alias' => 'Pla_contracts'));
        $this->belongsTo('use_id', 'Pla_users', 'id', array('alias' => 'Pla_users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_groups';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'gro_name' => 'gro_name', 
            'gro_leader' => 'gro_leader', 
            'gro_status' => 'gro_status', 
            'use_id' => 'use_id'
        );
    }

}
