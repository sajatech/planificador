<?php

class Contracts extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $con_day;

    /**
     *
     * @var string
     */
    protected $con_date;

    /**
     *
     * @var string
     */
    protected $con_service;

    /**
     *
     * @var double
     */
    protected $con_pay;

    /**
     *
     * @var integer
     */
    protected $con_position;

    /**
     *
     * @var integer
     */
    protected $con_frequency;

    /**
     *
     * @var string
     */
    protected $con_doc_type;

    /**
     *
     * @var string
     */
    protected $con_color;

    /**
     *
     * @var string
     */
    protected $con_status;

    /**
     *
     * @var string
     */
    protected $con_cre_date;

    /**
     *
     * @var integer
     */
    protected $cli_id;

    /**
     *
     * @var integer
     */
    protected $gro_id;

    /**
     *
     * @var integer
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field con_day
     *
     * @param string $con_day
     * @return $this
     */
    public function setConDay($con_day)
    {
        $this->con_day = $con_day;

        return $this;
    }

    /**
     * Method to set the value of field con_date
     *
     * @param string $con_date
     * @return $this
     */
    public function setConDate($con_date)
    {
        $this->con_date = $con_date;

        return $this;
    }

    /**
     * Method to set the value of field con_service
     *
     * @param string $con_service
     * @return $this
     */
    public function setConService($con_service)
    {
        $this->con_service = $con_service;

        return $this;
    }

    /**
     * Method to set the value of field con_pay
     *
     * @param double $con_pay
     * @return $this
     */
    public function setConPay($con_pay)
    {
        $this->con_pay = $con_pay;

        return $this;
    }

    /**
     * Method to set the value of field con_position
     *
     * @param integer $con_position
     * @return $this
     */
    public function setConPosition($con_position)
    {
        $this->con_position = $con_position;

        return $this;
    }

    /**
     * Method to set the value of field con_frequency
     *
     * @param integer $con_frequency
     * @return $this
     */
    public function setConFrequency($con_frequency)
    {
        $this->con_frequency = $con_frequency;

        return $this;
    }

    /**
     * Method to set the value of field con_doc_type
     *
     * @param string $con_doc_type
     * @return $this
     */
    public function setConDocType($con_doc_type)
    {
        $this->con_doc_type = $con_doc_type;

        return $this;
    }

    /**
     * Method to set the value of field con_color
     *
     * @param string $con_color
     * @return $this
     */
    public function setConColor($con_color)
    {
        $this->con_color = $con_color;

        return $this;
    }

    /**
     * Method to set the value of field con_status
     *
     * @param string $con_status
     * @return $this
     */
    public function setConStatus($con_status)
    {
        $this->con_status = $con_status;

        return $this;
    }

    /**
     * Method to set the value of field con_cre_date
     *
     * @param string $con_cre_date
     * @return $this
     */
    public function setConCreDate($con_cre_date)
    {
        $this->con_cre_date = $con_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field cli_id
     *
     * @param integer $cli_id
     * @return $this
     */
    public function setCliId($cli_id)
    {
        $this->cli_id = $cli_id;

        return $this;
    }

    /**
     * Method to set the value of field gro_id
     *
     * @param integer $gro_id
     * @return $this
     */
    public function setGroId($gro_id)
    {
        $this->gro_id = $gro_id;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field con_day
     *
     * @return string
     */
    public function getConDay()
    {
        return $this->con_day;
    }

    /**
     * Returns the value of field con_date
     *
     * @return string
     */
    public function getConDate()
    {
        return $this->con_date;
    }

    /**
     * Returns the value of field con_service
     *
     * @return string
     */
    public function getConService()
    {
        return $this->con_service;
    }

    /**
     * Returns the value of field con_pay
     *
     * @return double
     */
    public function getConPay()
    {
        return $this->con_pay;
    }

    /**
     * Returns the value of field con_position
     *
     * @return integer
     */
    public function getConPosition()
    {
        return $this->con_position;
    }

    /**
     * Returns the value of field con_frequency
     *
     * @return integer
     */
    public function getConFrequency()
    {
        return $this->con_frequency;
    }

    /**
     * Returns the value of field con_doc_type
     *
     * @return string
     */
    public function getConDocType()
    {
        return $this->con_doc_type;
    }

    /**
     * Returns the value of field con_color
     *
     * @return string
     */
    public function getConColor()
    {
        return $this->con_color;
    }

    /**
     * Returns the value of field con_status
     *
     * @return string
     */
    public function getConStatus()
    {
        return $this->con_status;
    }

    /**
     * Returns the value of field con_cre_date
     *
     * @return string
     */
    public function getConCreDate()
    {
        return $this->con_cre_date;
    }

    /**
     * Returns the value of field cli_id
     *
     * @return integer
     */
    public function getCliId()
    {
        return $this->cli_id;
    }

    /**
     * Returns the value of field gro_id
     *
     * @return integer
     */
    public function getGroId()
    {
        return $this->gro_id;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Pla_services', 'con_id', array('alias' => 'Pla_services'));
        $this->belongsTo('cli_id', 'Pla_clients', 'id', array('alias' => 'Pla_clients'));
        $this->belongsTo('gro_id', 'Pla_groups', 'id', array('alias' => 'Pla_groups'));
        $this->belongsTo('use_id', 'Pla_users', 'id', array('alias' => 'Pla_users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_contracts';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'con_day' => 'con_day', 
            'con_date' => 'con_date', 
            'con_service' => 'con_service', 
            'con_pay' => 'con_pay', 
            'con_position' => 'con_position', 
            'con_frequency' => 'con_frequency', 
            'con_doc_type' => 'con_doc_type', 
            'con_color' => 'con_color', 
            'con_status' => 'con_status', 
            'con_cre_date' => 'con_cre_date', 
            'cli_id' => 'cli_id', 
            'gro_id' => 'gro_id', 
            'use_id' => 'use_id'
        );
    }

}
