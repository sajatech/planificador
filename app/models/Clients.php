<?php

class Clients extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $cli_code;

    /**
     *
     * @var string
     */
    protected $cli_name;

    /**
     *
     * @var string
     */
    protected $cli_address;

    /**
     *
     * @var string
     */
    protected $cli_phone;

    /**
     *
     * @var string
     */
    protected $cli_mob_phone;

    /**
     *
     * @var string
     */
    protected $cli_email;

    /**
     *
     * @var string
     */
    protected $cli_status;

    /**
     *
     * @var string
     */
    protected $cli_cre_date;

    /**
     *
     * @var integer
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field cli_code
     *
     * @param string $cli_code
     * @return $this
     */
    public function setCliCode($cli_code)
    {
        $this->cli_code = $cli_code;

        return $this;
    }

    /**
     * Method to set the value of field cli_name
     *
     * @param string $cli_name
     * @return $this
     */
    public function setCliName($cli_name)
    {
        $this->cli_name = $cli_name;

        return $this;
    }

    /**
     * Method to set the value of field cli_address
     *
     * @param string $cli_address
     * @return $this
     */
    public function setCliAddress($cli_address)
    {
        $this->cli_address = $cli_address;

        return $this;
    }

    /**
     * Method to set the value of field cli_phone
     *
     * @param string $cli_phone
     * @return $this
     */
    public function setCliPhone($cli_phone)
    {
        $this->cli_phone = $cli_phone;

        return $this;
    }

    /**
     * Method to set the value of field cli_mob_phone
     *
     * @param string $cli_mob_phone
     * @return $this
     */
    public function setCliMobPhone($cli_mob_phone)
    {
        $this->cli_mob_phone = $cli_mob_phone;

        return $this;
    }

    /**
     * Method to set the value of field cli_email
     *
     * @param string $cli_email
     * @return $this
     */
    public function setCliEmail($cli_email)
    {
        $this->cli_email = $cli_email;

        return $this;
    }

    /**
     * Method to set the value of field cli_status
     *
     * @param string $cli_status
     * @return $this
     */
    public function setCliStatus($cli_status)
    {
        $this->cli_status = $cli_status;

        return $this;
    }

    /**
     * Method to set the value of field cli_cre_date
     *
     * @param string $cli_cre_date
     * @return $this
     */
    public function setCliCreDate($cli_cre_date)
    {
        $this->cli_cre_date = $cli_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field cli_code
     *
     * @return string
     */
    public function getCliCode()
    {
        return $this->cli_code;
    }

    /**
     * Returns the value of field cli_name
     *
     * @return string
     */
    public function getCliName()
    {
        return $this->cli_name;
    }

    /**
     * Returns the value of field cli_address
     *
     * @return string
     */
    public function getCliAddress()
    {
        return $this->cli_address;
    }

    /**
     * Returns the value of field cli_phone
     *
     * @return string
     */
    public function getCliPhone()
    {
        return $this->cli_phone;
    }

    /**
     * Returns the value of field cli_mob_phone
     *
     * @return string
     */
    public function getCliMobPhone()
    {
        return $this->cli_mob_phone;
    }

    /**
     * Returns the value of field cli_email
     *
     * @return string
     */
    public function getCliEmail()
    {
        return $this->cli_email;
    }

    /**
     * Returns the value of field cli_status
     *
     * @return string
     */
    public function getCliStatus()
    {
        return $this->cli_status;
    }

    /**
     * Returns the value of field cli_cre_date
     *
     * @return string
     */
    public function getCliCreDate()
    {
        return $this->cli_cre_date;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Pla_contracts', 'cli_id', array('alias' => 'Pla_contracts'));
        $this->belongsTo('use_id', 'Pla_users', 'id', array('alias' => 'Pla_users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_clients';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'cli_code' => 'cli_code', 
            'cli_name' => 'cli_name', 
            'cli_address' => 'cli_address', 
            'cli_phone' => 'cli_phone', 
            'cli_mob_phone' => 'cli_mob_phone', 
            'cli_email' => 'cli_email', 
            'cli_status' => 'cli_status', 
            'cli_cre_date' => 'cli_cre_date', 
            'use_id' => 'use_id'
        );
    }

}
