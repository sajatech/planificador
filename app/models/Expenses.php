<?php

class Expenses extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=20, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="exp_description", type="string", length=255, nullable=false)
     */
    protected $exp_description;

    /**
     *
     * @var string
     * @Column(column="exp_date", type="string", nullable=false)
     */
    protected $exp_date;

    /**
     *
     * @var integer
     * @Column(column="exp_quantity", type="integer", length=11, nullable=false)
     */
    protected $exp_quantity;

    /**
     *
     * @var double
     * @Column(column="exp_cost", type="double", nullable=false)
     */
    protected $exp_cost;

    /**
     *
     * @var string
     * @Column(column="exp_status", type="string", length=255, nullable=false)
     */
    protected $exp_status;

    /**
     *
     * @var string
     * @Column(column="exp_cre_date", type="string", nullable=false)
     */
    protected $exp_cre_date;

    /**
     *
     * @var integer
     * @Column(column="con_id", type="integer", length=11, nullable=false)
     */
    protected $con_id;

    /**
     *
     * @var integer
     * @Column(column="use_id", type="integer", length=11, nullable=false)
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field exp_description
     *
     * @param string $exp_description
     * @return $this
     */
    public function setExpDescription($exp_description)
    {
        $this->exp_description = $exp_description;

        return $this;
    }

    /**
     * Method to set the value of field exp_date
     *
     * @param string $exp_date
     * @return $this
     */
    public function setExpDate($exp_date)
    {
        $this->exp_date = $exp_date;

        return $this;
    }

    /**
     * Method to set the value of field exp_quantity
     *
     * @param integer $exp_quantity
     * @return $this
     */
    public function setExpQuantity($exp_quantity)
    {
        $this->exp_quantity = $exp_quantity;

        return $this;
    }

    /**
     * Method to set the value of field exp_cost
     *
     * @param double $exp_cost
     * @return $this
     */
    public function setExpCost($exp_cost)
    {
        $this->exp_cost = $exp_cost;

        return $this;
    }

    /**
     * Method to set the value of field exp_status
     *
     * @param string $exp_status
     * @return $this
     */
    public function setExpStatus($exp_status)
    {
        $this->exp_status = $exp_status;

        return $this;
    }

    /**
     * Method to set the value of field exp_cre_date
     *
     * @param string $exp_cre_date
     * @return $this
     */
    public function setExpCreDate($exp_cre_date)
    {
        $this->exp_cre_date = $exp_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field con_id
     *
     * @param integer $con_id
     * @return $this
     */
    public function setConId($con_id)
    {
        $this->con_id = $con_id;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field exp_description
     *
     * @return string
     */
    public function getExpDescription()
    {
        return $this->exp_description;
    }

    /**
     * Returns the value of field exp_date
     *
     * @return string
     */
    public function getExpDate()
    {
        return $this->exp_date;
    }

    /**
     * Returns the value of field exp_quantity
     *
     * @return integer
     */
    public function getExpQuantity()
    {
        return $this->exp_quantity;
    }

    /**
     * Returns the value of field exp_cost
     *
     * @return double
     */
    public function getExpCost()
    {
        return $this->exp_cost;
    }

    /**
     * Returns the value of field exp_status
     *
     * @return string
     */
    public function getExpStatus()
    {
        return $this->exp_status;
    }

    /**
     * Returns the value of field exp_cre_date
     *
     * @return string
     */
    public function getExpCreDate()
    {
        return $this->exp_cre_date;
    }

    /**
     * Returns the value of field con_id
     *
     * @return integer
     */
    public function getConId()
    {
        return $this->con_id;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("pla_expenses");
        $this->belongsTo('use_id', '\PlaUsers', 'id', ['alias' => 'PlaUsers']);
        $this->belongsTo('con_id', '\PlaExpConcepts', 'id', ['alias' => 'PlaExpConcepts']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_expenses';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlaExpenses[]|PlaExpenses|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlaExpenses|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
