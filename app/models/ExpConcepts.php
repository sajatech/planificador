<?php

class ExpConcepts extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=20, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="con_name", type="string", length=255, nullable=false)
     */
    protected $con_name;

    /**
     *
     * @var string
     * @Column(column="con_status", type="string", length=255, nullable=false)
     */
    protected $con_status;

    /**
     *
     * @var string
     * @Column(column="con_cre_date", type="string", nullable=false)
     */
    protected $con_cre_date;

    /**
     *
     * @var integer
     * @Column(column="use_id", type="integer", length=11, nullable=false)
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field con_name
     *
     * @param string $con_name
     * @return $this
     */
    public function setConName($con_name)
    {
        $this->con_name = $con_name;

        return $this;
    }

    /**
     * Method to set the value of field con_status
     *
     * @param string $con_status
     * @return $this
     */
    public function setConStatus($con_status)
    {
        $this->con_status = $con_status;

        return $this;
    }

    /**
     * Method to set the value of field con_cre_date
     *
     * @param string $con_cre_date
     * @return $this
     */
    public function setConCreDate($con_cre_date)
    {
        $this->con_cre_date = $con_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field con_name
     *
     * @return string
     */
    public function getConName()
    {
        return $this->con_name;
    }

    /**
     * Returns the value of field con_status
     *
     * @return string
     */
    public function getConStatus()
    {
        return $this->con_status;
    }

    /**
     * Returns the value of field con_cre_date
     *
     * @return string
     */
    public function getConCreDate()
    {
        return $this->con_cre_date;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("pla_exp_concepts");
        $this->hasMany('id', 'PlaExpenses', 'con_id', ['alias' => 'PlaExpenses']);
        $this->belongsTo('use_id', '\PlaUsers', 'id', ['alias' => 'PlaUsers']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_exp_concepts';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlaExpConcepts[]|PlaExpConcepts|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlaExpConcepts|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
