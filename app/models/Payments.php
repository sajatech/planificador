<?php

class Payments extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var double
     */
    protected $pay_amount;

    /**
     *
     * @var string
     */
    protected $pay_date;

    /**
     *
     * @var integer
     */
    protected $ser_id;

    /**
     *
     * @var integer
     */
    protected $typ_id;

    /**
     *
     * @var integer
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pay_amount
     *
     * @param double $pay_amount
     * @return $this
     */
    public function setPayAmount($pay_amount)
    {
        $this->pay_amount = $pay_amount;

        return $this;
    }

    /**
     * Method to set the value of field pay_date
     *
     * @param string $pay_date
     * @return $this
     */
    public function setPayDate($pay_date)
    {
        $this->pay_date = $pay_date;

        return $this;
    }

    /**
     * Method to set the value of field ser_id
     *
     * @param integer $ser_id
     * @return $this
     */
    public function setSerId($ser_id)
    {
        $this->ser_id = $ser_id;

        return $this;
    }

    /**
     * Method to set the value of field typ_id
     *
     * @param integer $typ_id
     * @return $this
     */
    public function setTypId($typ_id)
    {
        $this->typ_id = $typ_id;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pay_amount
     *
     * @return double
     */
    public function getPayAmount()
    {
        return $this->pay_amount;
    }

    /**
     * Returns the value of field pay_date
     *
     * @return string
     */
    public function getPayDate()
    {
        return $this->pay_date;
    }

    /**
     * Returns the value of field ser_id
     *
     * @return integer
     */
    public function getSerId()
    {
        return $this->ser_id;
    }

    /**
     * Returns the value of field typ_id
     *
     * @return integer
     */
    public function getTypId()
    {
        return $this->typ_id;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ser_id', 'Pla_services', 'id', array('alias' => 'Pla_services'));
        $this->belongsTo('typ_id', 'Pla_types', 'id', array('alias' => 'Pla_types'));
        $this->belongsTo('use_id', 'Pla_users', 'id', array('alias' => 'Pla_users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_payments';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'pay_amount' => 'pay_amount', 
            'pay_date' => 'pay_date', 
            'ser_id' => 'ser_id', 
            'typ_id' => 'typ_id', 
            'use_id' => 'use_id'
        );
    }

}
