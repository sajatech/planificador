<?php

class Additional extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $add_name;

    /**
     *
     * @var double
     */
    protected $add_amount;

    /**
     *
     * @var string
     */
    protected $add_cre_date;

    /**
     *
     * @var integer
     */
    protected $ser_id;

    /**
     *
     * @var integer
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field add_name
     *
     * @param string $add_name
     * @return $this
     */
    public function setAddName($add_name)
    {
        $this->add_name = $add_name;

        return $this;
    }

    /**
     * Method to set the value of field add_amount
     *
     * @param double $add_amount
     * @return $this
     */
    public function setAddAmount($add_amount)
    {
        $this->add_amount = $add_amount;

        return $this;
    }

    /**
     * Method to set the value of field add_cre_date
     *
     * @param string $add_cre_date
     * @return $this
     */
    public function setAddCreDate($add_cre_date)
    {
        $this->add_cre_date = $add_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field ser_id
     *
     * @param integer $ser_id
     * @return $this
     */
    public function setSerId($ser_id)
    {
        $this->ser_id = $ser_id;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field add_name
     *
     * @return string
     */
    public function getAddName()
    {
        return $this->add_name;
    }

    /**
     * Returns the value of field add_amount
     *
     * @return double
     */
    public function getAddAmount()
    {
        return $this->add_amount;
    }

    /**
     * Returns the value of field add_cre_date
     *
     * @return string
     */
    public function getAddCreDate()
    {
        return $this->add_cre_date;
    }

    /**
     * Returns the value of field ser_id
     *
     * @return integer
     */
    public function getSerId()
    {
        return $this->ser_id;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ser_id', 'Pla_services', 'id', array('alias' => 'Pla_services'));
        $this->belongsTo('use_id', 'Pla_users', 'id', array('alias' => 'Pla_users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_additional';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'add_name' => 'add_name', 
            'add_amount' => 'add_amount', 
            'add_cre_date' => 'add_cre_date', 
            'ser_id' => 'ser_id', 
            'use_id' => 'use_id'
        );
    }

}
