<?php

class Documents extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $doc_number;

    /**
     *
     * @var integer
     */
    protected $doc_invoice;

    /**
     *
     * @var double
     */
    protected $doc_amount;

    /**
     *
     * @var string
     */
    protected $doc_status;

    /**
     *
     * @var string
     */
    protected $doc_cre_date;

    /**
     *
     * @var integer
     */
    protected $ser_id;

    /**
     * Method to set the value of field id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field doc_number
     *
     * @param integer $doc_number
     * @return $this
     */
    public function setDocNumber($doc_number)
    {
        $this->doc_number = $doc_number;

        return $this;
    }

    /**
     * Method to set the value of field doc_invoice
     *
     * @param integer $doc_invoice
     * @return $this
     */
    public function setDocInvoice($doc_invoice)
    {
        $this->doc_invoice = $doc_invoice;

        return $this;
    }

    /**
     * Method to set the value of field doc_amount
     *
     * @param double $doc_amount
     * @return $this
     */
    public function setDocAmount($doc_amount)
    {
        $this->doc_amount = $doc_amount;

        return $this;
    }

    /**
     * Method to set the value of field doc_status
     *
     * @param string $doc_status
     * @return $this
     */
    public function setDocStatus($doc_status)
    {
        $this->doc_status = $doc_status;

        return $this;
    }

    /**
     * Method to set the value of field doc_cre_date
     *
     * @param string $doc_cre_date
     * @return $this
     */
    public function setDocCreDate($doc_cre_date)
    {
        $this->doc_cre_date = $doc_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field ser_id
     *
     * @param integer $ser_id
     * @return $this
     */
    public function setSerId($ser_id)
    {
        $this->ser_id = $ser_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field doc_number
     *
     * @return integer
     */
    public function getDocNumber()
    {
        return $this->doc_number;
    }

    /**
     * Returns the value of field doc_invoice
     *
     * @return integer
     */
    public function getDocInvoice()
    {
        return $this->doc_invoice;
    }

    /**
     * Returns the value of field doc_amount
     *
     * @return double
     */
    public function getDocAmount()
    {
        return $this->doc_amount;
    }

    /**
     * Returns the value of field doc_status
     *
     * @return string
     */
    public function getDocStatus()
    {
        return $this->doc_status;
    }

    /**
     * Returns the value of field doc_cre_date
     *
     * @return string
     */
    public function getDocCreDate()
    {
        return $this->doc_cre_date;
    }

    /**
     * Returns the value of field ser_id
     *
     * @return integer
     */
    public function getSerId()
    {
        return $this->ser_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ser_id', 'Pla_services', 'id', array('alias' => 'Pla_services'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_documents';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'doc_number' => 'doc_number', 
            'doc_invoice' => 'doc_invoice', 
            'doc_amount' => 'doc_amount', 
            'doc_status' => 'doc_status', 
            'doc_cre_date' => 'doc_cre_date', 
            'ser_id' => 'ser_id'
        );
    }

}
