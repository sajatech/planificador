<?php

class Roles extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $rol_name;

    /**
     *
     * @var string
     */
    protected $rol_status;

    /**
     *
     * @var integer
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field rol_name
     *
     * @param string $rol_name
     * @return $this
     */
    public function setRolName($rol_name)
    {
        $this->rol_name = $rol_name;

        return $this;
    }

    /**
     * Method to set the value of field rol_status
     *
     * @param string $rol_status
     * @return $this
     */
    public function setRolStatus($rol_status)
    {
        $this->rol_status = $rol_status;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field rol_name
     *
     * @return string
     */
    public function getRolName()
    {
        return $this->rol_name;
    }

    /**
     * Returns the value of field rol_status
     *
     * @return string
     */
    public function getRolStatus()
    {
        return $this->rol_status;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Pla_users', 'rol_id', array('alias' => 'Pla_users'));
        $this->belongsTo('usu_id', 'Pla_usuarios', 'id', array('alias' => 'Pla_usuarios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_roles';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'rol_name' => 'rol_name', 
            'rol_status' => 'rol_status', 
            'use_id' => 'use_id'
        );
    }

}
