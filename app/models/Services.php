<?php

class Services extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $ser_date;

    /**
     *
     * @var string
     */
    protected $ser_status;

    /**
     *
     * @var string
     */
    protected $ser_position;

    /**
     *
     * @var string
     */
    protected $ser_cre_date;

    /**
     *
     * @var integer
     */
    protected $con_id;

    /**
     *
     * @var integer
     */
    protected $use_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field ser_date
     *
     * @param string $ser_date
     * @return $this
     */
    public function setSerDate($ser_date)
    {
        $this->ser_date = $ser_date;

        return $this;
    }

    /**
     * Method to set the value of field ser_status
     *
     * @param string $ser_status
     * @return $this
     */
    public function setSerStatus($ser_status)
    {
        $this->ser_status = $ser_status;

        return $this;
    }

    /**
     * Method to set the value of field ser_position
     *
     * @param string $ser_position
     * @return $this
     */
    public function setSerPosition($ser_position)
    {
        $this->ser_position = $ser_position;

        return $this;
    }

    /**
     * Method to set the value of field ser_cre_date
     *
     * @param string $ser_cre_date
     * @return $this
     */
    public function setSerCreDate($ser_cre_date)
    {
        $this->ser_cre_date = $ser_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field con_id
     *
     * @param integer $con_id
     * @return $this
     */
    public function setConId($con_id)
    {
        $this->con_id = $con_id;

        return $this;
    }

    /**
     * Method to set the value of field use_id
     *
     * @param integer $use_id
     * @return $this
     */
    public function setUseId($use_id)
    {
        $this->use_id = $use_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field ser_date
     *
     * @return string
     */
    public function getSerDate()
    {
        return $this->ser_date;
    }

    /**
     * Returns the value of field ser_status
     *
     * @return string
     */
    public function getSerStatus()
    {
        return $this->ser_status;
    }

    /**
     * Returns the value of field ser_position
     *
     * @return string
     */
    public function getSerPosition()
    {
        return $this->ser_position;
    }

    /**
     * Returns the value of field ser_cre_date
     *
     * @return string
     */
    public function getSerCreDate()
    {
        return $this->ser_cre_date;
    }

    /**
     * Returns the value of field con_id
     *
     * @return integer
     */
    public function getConId()
    {
        return $this->con_id;
    }

    /**
     * Returns the value of field use_id
     *
     * @return integer
     */
    public function getUseId()
    {
        return $this->use_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Pla_additional', 'ser_id', array('alias' => 'Pla_additional'));
        $this->hasMany('id', 'Pla_documents', 'ser_id', array('alias' => 'Pla_documents'));
        $this->hasMany('id', 'Pla_payments', 'ser_id', array('alias' => 'Pla_payments'));
        $this->belongsTo('con_id', 'Pla_contracts', 'id', array('alias' => 'Pla_contracts'));
        $this->belongsTo('use_id', 'Pla_users', 'id', array('alias' => 'Pla_users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_services';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'ser_date' => 'ser_date', 
            'ser_status' => 'ser_status', 
            'ser_position' => 'ser_position', 
            'ser_cre_date' => 'ser_cre_date', 
            'con_id' => 'con_id', 
            'use_id' => 'use_id'
        );
    }

}
