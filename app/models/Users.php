<?php

class Users extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=20, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="use_code", type="string", length=255, nullable=false)
     */
    protected $use_code;

    /**
     *
     * @var string
     * @Column(column="use_name", type="string", length=255, nullable=false)
     */
    protected $use_name;

    /**
     *
     * @var string
     * @Column(column="use_email", type="string", length=255, nullable=false)
     */
    protected $use_email;

    /**
     *
     * @var string
     * @Column(column="use_phone", type="string", length=255, nullable=true)
     */
    protected $use_phone;

    /**
     *
     * @var string
     * @Column(column="use_login", type="string", length=255, nullable=false)
     */
    protected $use_login;

    /**
     *
     * @var string
     * @Column(column="use_password", type="string", length=255, nullable=false)
     */
    protected $use_password;

    /**
     *
     * @var string
     * @Column(column="use_status", type="string", length=255, nullable=false)
     */
    protected $use_status;

    /**
     *
     * @var string
     * @Column(column="use_cre_date", type="string", nullable=false)
     */
    protected $use_cre_date;

    /**
     *
     * @var integer
     * @Column(column="rol_id", type="integer", length=11, nullable=false)
     */
    protected $rol_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field use_code
     *
     * @param string $use_code
     * @return $this
     */
    public function setUseCode($use_code)
    {
        $this->use_code = $use_code;

        return $this;
    }

    /**
     * Method to set the value of field use_name
     *
     * @param string $use_name
     * @return $this
     */
    public function setUseName($use_name)
    {
        $this->use_name = $use_name;

        return $this;
    }

    /**
     * Method to set the value of field use_email
     *
     * @param string $use_email
     * @return $this
     */
    public function setUseEmail($use_email)
    {
        $this->use_email = $use_email;

        return $this;
    }

    /**
     * Method to set the value of field use_phone
     *
     * @param string $use_phone
     * @return $this
     */
    public function setUsePhone($use_phone)
    {
        $this->use_phone = $use_phone;

        return $this;
    }

    /**
     * Method to set the value of field use_login
     *
     * @param string $use_login
     * @return $this
     */
    public function setUseLogin($use_login)
    {
        $this->use_login = $use_login;

        return $this;
    }

    /**
     * Method to set the value of field use_password
     *
     * @param string $use_password
     * @return $this
     */
    public function setUsePassword($use_password)
    {
        $this->use_password = $use_password;

        return $this;
    }

    /**
     * Method to set the value of field use_status
     *
     * @param string $use_status
     * @return $this
     */
    public function setUseStatus($use_status)
    {
        $this->use_status = $use_status;

        return $this;
    }

    /**
     * Method to set the value of field use_cre_date
     *
     * @param string $use_cre_date
     * @return $this
     */
    public function setUseCreDate($use_cre_date)
    {
        $this->use_cre_date = $use_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field rol_id
     *
     * @param integer $rol_id
     * @return $this
     */
    public function setRolId($rol_id)
    {
        $this->rol_id = $rol_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field use_code
     *
     * @return string
     */
    public function getUseCode()
    {
        return $this->use_code;
    }

    /**
     * Returns the value of field use_name
     *
     * @return string
     */
    public function getUseName()
    {
        return $this->use_name;
    }

    /**
     * Returns the value of field use_email
     *
     * @return string
     */
    public function getUseEmail()
    {
        return $this->use_email;
    }

    /**
     * Returns the value of field use_phone
     *
     * @return string
     */
    public function getUsePhone()
    {
        return $this->use_phone;
    }

    /**
     * Returns the value of field use_login
     *
     * @return string
     */
    public function getUseLogin()
    {
        return $this->use_login;
    }

    /**
     * Returns the value of field use_password
     *
     * @return string
     */
    public function getUsePassword()
    {
        return $this->use_password;
    }

    /**
     * Returns the value of field use_status
     *
     * @return string
     */
    public function getUseStatus()
    {
        return $this->use_status;
    }

    /**
     * Returns the value of field use_cre_date
     *
     * @return string
     */
    public function getUseCreDate()
    {
        return $this->use_cre_date;
    }

    /**
     * Returns the value of field rol_id
     *
     * @return integer
     */
    public function getRolId()
    {
        return $this->rol_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("pla_users");
        $this->hasMany('id', 'PlaAdditional', 'use_id', ['alias' => 'PlaAdditional']);
        $this->hasMany('id', 'PlaClients', 'use_id', ['alias' => 'PlaClients']);
        $this->hasMany('id', 'PlaContracts', 'use_id', ['alias' => 'PlaContracts']);
        $this->hasMany('id', 'PlaExpConcepts', 'use_id', ['alias' => 'PlaExpConcepts']);
        $this->hasMany('id', 'PlaExpenses', 'use_id', ['alias' => 'PlaExpenses']);
        $this->hasMany('id', 'PlaGroups', 'use_id', ['alias' => 'PlaGroups']);
        $this->hasMany('id', 'PlaPayments', 'use_id', ['alias' => 'PlaPayments']);
        $this->hasMany('id', 'PlaRoles', 'use_id', ['alias' => 'PlaRoles']);
        $this->hasMany('id', 'PlaServices', 'use_id', ['alias' => 'PlaServices']);
        $this->hasMany('id', 'PlaTypes', 'usu_id', ['alias' => 'PlaTypes']);
        $this->belongsTo('rol_id', '\PlaRoles', 'id', ['alias' => 'PlaRoles']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_users';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlaUsers[]|PlaUsers|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PlaUsers|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
