<?php

class Types extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $typ_name;

    /**
     *
     * @var string
     */
    protected $typ_status;

    /**
     *
     * @var string
     */
    protected $typ_cre_date;

    /**
     *
     * @var integer
     */
    protected $usu_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field typ_name
     *
     * @param string $typ_name
     * @return $this
     */
    public function setTypName($typ_name)
    {
        $this->typ_name = $typ_name;

        return $this;
    }

    /**
     * Method to set the value of field typ_status
     *
     * @param string $typ_status
     * @return $this
     */
    public function setTypStatus($typ_status)
    {
        $this->typ_status = $typ_status;

        return $this;
    }

    /**
     * Method to set the value of field typ_cre_date
     *
     * @param string $typ_cre_date
     * @return $this
     */
    public function setTypCreDate($typ_cre_date)
    {
        $this->typ_cre_date = $typ_cre_date;

        return $this;
    }

    /**
     * Method to set the value of field usu_id
     *
     * @param integer $usu_id
     * @return $this
     */
    public function setUsuId($usu_id)
    {
        $this->usu_id = $usu_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field typ_name
     *
     * @return string
     */
    public function getTypName()
    {
        return $this->typ_name;
    }

    /**
     * Returns the value of field typ_status
     *
     * @return string
     */
    public function getTypStatus()
    {
        return $this->typ_status;
    }

    /**
     * Returns the value of field typ_cre_date
     *
     * @return string
     */
    public function getTypCreDate()
    {
        return $this->typ_cre_date;
    }

    /**
     * Returns the value of field usu_id
     *
     * @return integer
     */
    public function getUsuId()
    {
        return $this->usu_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Pla_payments', 'typ_id', array('alias' => 'Pla_payments'));
        $this->belongsTo('usu_id', 'Pla_users', 'id', array('alias' => 'Pla_users'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pla_types';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'typ_name' => 'typ_name', 
            'typ_status' => 'typ_status', 
            'typ_cre_date' => 'typ_cre_date', 
            'usu_id' => 'usu_id'
        );
    }

}
