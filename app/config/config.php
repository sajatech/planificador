<?php

/**
 * Composer autoload
 */
include __DIR__ . '/../../vendor/autoload.php';

/**
 * Environment variables
 */
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../../');
$dotenv->load();

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'  => 'Mysql',
        'host'     => getenv('DATABASE_HOST'),
        'username' => getenv('DATABASE_USER'),
        'password' => getenv('DATABASE_PASS'),
        'dbname'   => getenv('DATABASE_NAME')
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => getenv('BASE_URI'),
    ),
    'email' => array(
        'templatesDir' =>   __DIR__ . '/../../app/views/email/',
        'from'         =>   'notification@ignaciogonzalezlandscaping.com',
        'fromName'     =>   'Ignacio González',
        'host'         =>   'a2plcpnl0874.prod.iad2.secureserver.net',
        'SMTPAuth'     =>   true,
        'username'     =>   'notification@ignaciogonzalezlandscaping.com',
        'password'     =>   '2oHAjQDiJsM',
        'SMTPSecure'   =>   'ssl',
        'port'         =>   465
    )
));